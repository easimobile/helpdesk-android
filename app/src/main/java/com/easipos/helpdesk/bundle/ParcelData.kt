package com.easipos.helpdesk.bundle

object ParcelData {

    const val CLEAR_DB = "clear_db"
    const val URL = "url"
    const val SELECTION_TITLE = "SELECTION_TITLE"
    const val SELECTIONS = "SELECTION"
    const val CASE = "CASE"
    const val CASE_ID = "CASE_ID"
    const val CASE_DETAIL = "CASE_DETAIL"
    const val IS_EDITABLE = "IS_EDITABLE"
    const val CREATE_NEW_CASE_DETAIL = "CREATE_NEW_CASE_DETAIL"
    const val OPERATION_EMPLOYEES = "OPERATION_EMPLOYEES"
    const val DUPLICATE_CASE = "DUPLICATE_CASE"
    const val GET_CLIENT_URL = "GET_CLIENT_URL"
}