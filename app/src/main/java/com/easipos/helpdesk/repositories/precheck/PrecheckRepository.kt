package com.easipos.helpdesk.repositories.precheck

import com.easipos.helpdesk.api.requests.precheck.CheckVersionRequestModel
import com.easipos.helpdesk.api.requests.precheck.GetClientUrlRequestModel
import com.easipos.helpdesk.models.Result

interface PrecheckRepository {

    suspend fun getClientUrl(model: GetClientUrlRequestModel): Result<String>

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}
