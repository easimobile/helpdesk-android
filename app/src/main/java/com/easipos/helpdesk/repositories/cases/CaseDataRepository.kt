package com.easipos.helpdesk.repositories.cases

import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.datasource.DataFactory
import com.easipos.helpdesk.mapper.cases.*
import com.easipos.helpdesk.models.*

class CaseDataRepository(private val dataFactory: DataFactory) : CaseRepository {

    private val caseMapper by lazy { CaseMapper() }
    private val caseInfoMapper by lazy { CaseInfoMapper() }
    private val caseDetailMapper by lazy { CaseDetailMapper() }
    private val projectMapper by lazy { ProjectMapper() }
    private val customerOutletMapper by lazy { CustomerOutletMapper() }
    private val outletPosMapper by lazy { OutletPosMapper() }
    private val projectSlaMapper by lazy { ProjectSlaMapper() }
    private val operationEmployeeMapper by lazy { OperationEmployeeMapper() }
    private val serviceTypeMapper by lazy { ServiceTypeMapper() }
    private val caseTypeMapper by lazy { CaseTypeMapper() }
    private val caseSubTypeMapper by lazy { CaseSubTypeMapper() }
    private val caseRatingMapper by lazy { CaseRatingMapper() }

    override suspend fun getCases(model: GetCasesRequestModel): Result<DataSourceListModel<Case>> =
        dataFactory.createCaseDataSource()
            .getCases(model, caseMapper)

    override suspend fun getCaseInfo(model: GetCaseInfoRequestModel): Result<CaseInfo> =
        dataFactory.createCaseDataSource()
            .getCaseInfo(model, caseInfoMapper)

    override suspend fun getCaseDetails(model: GetCaseDetailsRequestModel): Result<List<CaseDetail>> =
        dataFactory.createCaseDataSource()
            .getCaseDetails(model, caseDetailMapper)

    override suspend fun getProjects(): Result<List<Project>> =
        dataFactory.createCaseDataSource()
            .getProjects(projectMapper)

    override suspend fun getCustomerOutlets(model: GetCustomerOutletsRequestModel): Result<List<CustomerOutlet>> =
        dataFactory.createCaseDataSource()
            .getCustomerOutlets(model, customerOutletMapper)

    override suspend fun getOutletPosList(model: GetOutletPosListRequestModel): Result<List<OutletPos>> =
        dataFactory.createCaseDataSource()
            .getOutletPosList(model, outletPosMapper)

    override suspend fun getProjectSla(model: GetProjectSlaRequestModel): Result<ProjectSla> =
        dataFactory.createCaseDataSource()
            .getProjectSla(model, projectSlaMapper)

    override suspend fun getOperationEmployees(): Result<List<OperationEmployee>> =
        dataFactory.createCaseDataSource()
            .getOperationEmployees(operationEmployeeMapper)

    override suspend fun getServiceTypes(): Result<List<ServiceType>> =
        dataFactory.createCaseDataSource()
            .getServiceTypes(serviceTypeMapper)

    override suspend fun getCaseTypes(): Result<List<CaseType>> =
        dataFactory.createCaseDataSource()
            .getCaseTypes(caseTypeMapper)

    override suspend fun getCaseSubTypes1(model: GetCaseSubTypesRequestModel): Result<List<CaseSubType>> =
        dataFactory.createCaseDataSource()
            .getCaseSubTypes1(model, caseSubTypeMapper)

    override suspend fun getCaseSubTypes2(model: GetCaseSubTypesRequestModel): Result<List<CaseSubType>> =
        dataFactory.createCaseDataSource()
            .getCaseSubTypes2(model, caseSubTypeMapper)

    override suspend fun getCaseSubTypes3(model: GetCaseSubTypesRequestModel): Result<List<CaseSubType>> =
        dataFactory.createCaseDataSource()
            .getCaseSubTypes3(model, caseSubTypeMapper)

    override suspend fun createCase(model: CreateCaseRequestModel): Result<String> =
        dataFactory.createCaseDataSource()
            .createCase(model)

    override suspend fun createCaseDetail(model: CreateCaseDetailRequestModel): Result<Nothing> =
        dataFactory.createCaseDataSource()
            .createCaseDetail(model)

    override suspend fun editCase(model: EditCaseRequestModel): Result<Nothing> =
        dataFactory.createCaseDataSource()
            .editCase(model)

    override suspend fun editCaseDetail(model: EditCaseDetailRequestModel): Result<Nothing> =
        dataFactory.createCaseDataSource()
            .editCaseDetail(model)

    override suspend fun changeCaseAssignee(model: ChangeCaseAssigneeRequestModel): Result<Nothing> =
        dataFactory.createCaseDataSource()
            .changeCaseAssignee(model)

    override suspend fun changeCaseDetailStatus(model: ChangeCaseDetailStatusRequestModel): Result<Nothing> =
        dataFactory.createCaseDataSource()
            .changeCaseDetailStatus(model)

    override suspend fun getCaseRatings(model: GetCaseRatingsRequestModel): Result<DataSourceListModel<CaseRating>> =
        dataFactory.createCaseDataSource()
            .getCaseRatings(model, caseRatingMapper)
}
