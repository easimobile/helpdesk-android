package com.easipos.helpdesk.repositories.report

import com.easipos.helpdesk.api.requests.report.GetReportSummaryRequestModel
import com.easipos.helpdesk.datasource.DataFactory
import com.easipos.helpdesk.mapper.report.ReportSummaryMapper
import com.easipos.helpdesk.models.ReportSummary
import com.easipos.helpdesk.models.Result

class ReportDataRepository(private val dataFactory: DataFactory) : ReportRepository {

    private val reportSummaryMapper by lazy { ReportSummaryMapper() }

    override suspend fun getReportSummary(model: GetReportSummaryRequestModel): Result<ReportSummary> =
        dataFactory.createReportDataSource()
            .getReportSummary(model, reportSummaryMapper)
}
