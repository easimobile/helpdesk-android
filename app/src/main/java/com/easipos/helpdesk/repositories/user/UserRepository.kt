package com.easipos.helpdesk.repositories.user

import com.easipos.helpdesk.api.requests.user.ChangePasswordRequestModel
import com.easipos.helpdesk.api.requests.user.ForgotPasswordRequestModel
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.models.UserInfo

interface UserRepository {

    suspend fun forgotPassword(model: ForgotPasswordRequestModel): Result<Nothing>

    suspend fun changePassword(model: ChangePasswordRequestModel): Result<Nothing>

    suspend fun getUserInfo(): Result<UserInfo>
}
