package com.easipos.helpdesk.repositories.cases

import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.models.*

interface CaseRepository {

    suspend fun getCases(model: GetCasesRequestModel): Result<DataSourceListModel<Case>>

    suspend fun getCaseInfo(model: GetCaseInfoRequestModel): Result<CaseInfo>

    suspend fun getCaseDetails(model: GetCaseDetailsRequestModel): Result<List<CaseDetail>>

    suspend fun getProjects(): Result<List<Project>>

    suspend fun getCustomerOutlets(model: GetCustomerOutletsRequestModel): Result<List<CustomerOutlet>>

    suspend fun getOutletPosList(model: GetOutletPosListRequestModel): Result<List<OutletPos>>

    suspend fun getProjectSla(model: GetProjectSlaRequestModel): Result<ProjectSla>

    suspend fun getOperationEmployees(): Result<List<OperationEmployee>>

    suspend fun getServiceTypes(): Result<List<ServiceType>>

    suspend fun getCaseTypes(): Result<List<CaseType>>

    suspend fun getCaseSubTypes1(model: GetCaseSubTypesRequestModel): Result<List<CaseSubType>>

    suspend fun getCaseSubTypes2(model: GetCaseSubTypesRequestModel): Result<List<CaseSubType>>

    suspend fun getCaseSubTypes3(model: GetCaseSubTypesRequestModel): Result<List<CaseSubType>>

    suspend fun createCase(model: CreateCaseRequestModel): Result<String>

    suspend fun createCaseDetail(model: CreateCaseDetailRequestModel): Result<Nothing>

    suspend fun editCase(model: EditCaseRequestModel): Result<Nothing>

    suspend fun editCaseDetail(model: EditCaseDetailRequestModel): Result<Nothing>

    suspend fun changeCaseAssignee(model: ChangeCaseAssigneeRequestModel): Result<Nothing>

    suspend fun changeCaseDetailStatus(model: ChangeCaseDetailStatusRequestModel): Result<Nothing>

    suspend fun getCaseRatings(model: GetCaseRatingsRequestModel): Result<DataSourceListModel<CaseRating>>
}
