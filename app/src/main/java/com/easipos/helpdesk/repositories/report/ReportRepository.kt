package com.easipos.helpdesk.repositories.report

import com.easipos.helpdesk.api.requests.report.GetReportSummaryRequestModel
import com.easipos.helpdesk.models.ReportSummary
import com.easipos.helpdesk.models.Result

interface ReportRepository {

    suspend fun getReportSummary(model: GetReportSummaryRequestModel): Result<ReportSummary>
}
