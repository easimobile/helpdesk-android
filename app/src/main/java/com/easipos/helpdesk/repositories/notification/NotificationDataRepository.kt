package com.easipos.helpdesk.repositories.notification

import com.easipos.helpdesk.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.helpdesk.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.helpdesk.datasource.DataFactory
import com.easipos.helpdesk.models.Result

class NotificationDataRepository(private val dataFactory: DataFactory) : NotificationRepository {

    override suspend fun registerFcmToken(model: RegisterFcmTokenRequestModel): Result<Nothing> =
        dataFactory.createNotificationDataSource()
            .registerFcmToken(model)

    override suspend fun removeFcmToken(model: RemoveFcmTokenRequestModel): Result<Nothing> =
        dataFactory.createNotificationDataSource()
            .removeFcmToken(model)
}