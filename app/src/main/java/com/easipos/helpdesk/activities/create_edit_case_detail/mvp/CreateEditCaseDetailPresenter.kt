package com.easipos.helpdesk.activities.create_edit_case_detail.mvp

import android.app.Application
import com.easipos.helpdesk.R
import com.easipos.helpdesk.api.requests.cases.CreateCaseDetailRequestModel
import com.easipos.helpdesk.api.requests.cases.EditCaseDetailRequestModel
import com.easipos.helpdesk.api.requests.cases.GetCaseInfoRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.event_bus.RefreshCase
import com.easipos.helpdesk.event_bus.RefreshCaseListing
import com.easipos.helpdesk.models.CaseInfo
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.models.ServiceType
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.kodein.di.generic.instance

class CreateEditCaseDetailPresenter(application: Application)
    : Presenter<CreateEditCaseDetailView>(application) {

    private val caseRepository by instance<CaseRepository>()

    fun doGetServiceTypes() {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getServiceTypes()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<ServiceType>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showServiceTypeSelections(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doCreateCaseDetail(model: CreateCaseDetailRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.createCaseDetail(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(application.getString(R.string.prompt_case_detail_create_successful)) {
                            view?.finishScreen()
                        }

                        EventBus.getDefault().post(RefreshCase())
                        EventBus.getDefault().post(RefreshCaseListing())
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doEditCaseDetail(model: EditCaseDetailRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.editCaseDetail(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(application.getString(R.string.prompt_case_detail_update_successful)) {
                            view?.finishScreen()
                        }

                        EventBus.getDefault().post(RefreshCase())
                        EventBus.getDefault().post(RefreshCaseListing())
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doVerifyCase(model: GetCaseInfoRequestModel, callback: (CaseInfo) -> Unit) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCaseInfo(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<CaseInfo> -> {
                        view?.setLoadingIndicator(false)
                        callback(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
