package com.easipos.helpdesk.activities.create_edit_case.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.create_edit_case.CreateEditCaseActivity
import com.easipos.helpdesk.activities.view_case_details.ViewCaseDetailsActivity

class CreateEditCaseNavigationImpl : CreateEditCaseNavigation {

    override fun navigateToViewCaseDetails(activity: Activity, caseId: String, isEditable: Boolean) {
        activity.startActivity(ViewCaseDetailsActivity.newIntent(activity, caseId, isEditable, false))
    }

    override fun navigateToEditCase(activity: Activity, caseId: String) {
        activity.startActivity(CreateEditCaseActivity.newIntent(activity, caseId, null))
    }
}