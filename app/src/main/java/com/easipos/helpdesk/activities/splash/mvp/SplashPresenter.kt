package com.easipos.helpdesk.activities.splash.mvp

import android.app.Application
import com.easipos.helpdesk.R
import com.easipos.helpdesk.api.misc.HostSelectionInterceptor
import com.easipos.helpdesk.api.requests.precheck.CheckVersionRequestModel
import com.easipos.helpdesk.api.requests.precheck.GetClientUrlRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.managers.UserManager
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.precheck.PrecheckRepository
import com.easipos.helpdesk.tools.Preference
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class SplashPresenter(application: Application)
    : Presenter<SplashView>(application) {

    private val precheckRepository by instance<PrecheckRepository>()
    private val hostSelectionInterceptor by instance<HostSelectionInterceptor>()

    private val urlRegex = "https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)"

    fun getClientUrl() {
        launch {
            val result = precheckRepository.getClientUrl(GetClientUrlRequestModel())
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<String> -> {
                        val currentClientUrl = Preference.prefClientUrl
                        var newClientUrl = result.data.trim()
                        if (currentClientUrl != newClientUrl) {
                            if (newClientUrl.endsWith("/").not()) {
                                newClientUrl += "/"
                            }
                            Preference.prefClientUrl = newClientUrl
                            hostSelectionInterceptor.setHost(newClientUrl)
                            checkIsAuthenticated()
                        } else {
                            if (Preference.prefClientUrl.matches(urlRegex.toRegex())) {
                                checkIsAuthenticated()
                            } else {
                                view?.showErrorAlertDialog(application.getString(R.string.error_invalid_url)) {
                                    view?.exitApplication()
                                }
                            }
                        }
                    }

                    is Result.Error -> {
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception)) {
                            Preference.prefClientName = ""
                            view?.showClientNameDialog()
                        }
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun checkVersion() {
        launch {
            val result = precheckRepository.checkVersion(CheckVersionRequestModel())
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<Boolean> -> {
                        if (result.data) {
                            view?.showUpdateAppDialog()
                        } else {
                            checkIsAuthenticated()
                        }
                    }

                    is Result.Error -> {
                        checkIsAuthenticated()
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun checkIsAuthenticated() {
        if (Preference.prefIsLoggedIn && UserManager.token != null) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}
