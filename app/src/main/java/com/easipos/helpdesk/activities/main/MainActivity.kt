package com.easipos.helpdesk.activities.main

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.main.mvp.MainPresenter
import com.easipos.helpdesk.activities.main.mvp.MainView
import com.easipos.helpdesk.activities.main.navigation.MainNavigation
import com.easipos.helpdesk.base.CustomBaseAppCompatActivity
import com.easipos.helpdesk.constant.NotificationCategory
import com.easipos.helpdesk.fragments.account.AccountFragment
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.fragments.cases.CaseFragment
import com.easipos.helpdesk.fragments.dashboard.DashboardFragment
import com.easipos.helpdesk.fragments.rating.CaseRatingFragment
import com.easipos.helpdesk.managers.FcmManager
import com.easipos.helpdesk.managers.PushNotificationManager
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.CaseDuplication
import com.easipos.helpdesk.tools.ModulePermissionParser
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.onesignal.OneSignal
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.findColor
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.then
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.textColor
import org.kodein.di.generic.instance

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        const val HOME_TAB = 0 // Aka Case
        const val DASHBOARD_TAB = 1
        val RATING_TAB = ModulePermissionParser.isCasesAllowPost() then 2 ?: 1
        val ACCOUNT_TAB = ModulePermissionParser.isCasesAllowPost() then 3 ?: 2

        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    enum class MainNavTab {
        HOME, // Aka Case
        DASHBOARD,
        RATING,
        ACCOUNT
    }

    //region Variables
    private val navigation by instance<MainNavigation>()
    private val fcmManager by instance<FcmManager>()
    private val pushNotificationManager by instance<PushNotificationManager>()

    private val presenter by lazy { MainPresenter(application) }

    private val caseFragment by lazy { CaseFragment.newInstance() }
    private val dashboardFragment by lazy { DashboardFragment.newInstance() }
    private val caseRatingFragment by lazy { CaseRatingFragment.newInstance() }
    private val accountFragment by lazy { AccountFragment.newInstance() }

    private var selectedTab = MainNavTab.HOME
    private val tabFragments = arrayListOf<Fragment>()
    //endregion

    //region Lifecycle
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleDynamicLink(intent)
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        observeOneSignalSubscription()
        registerPushTokenIfPossible()
        processPayload()
        handleDynamicLink(intent)

        setupViews()
        setupListeners()

        selectTab()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun navigateToSplash() {
        navigation.navigateToSplash(this)
    }
    //endregion

    //region Action Methods
    fun processPayload() {
        Logger.d(pushNotificationManager.payload)
        pushNotificationManager.payload?.let { payload ->
            payload.category?.let { category ->
                NotificationCategory.parse(category)?.let { notificationCategory ->
                    when (notificationCategory) {
                        NotificationCategory.CASE_ASSIGNED_TO,
                        NotificationCategory.CASE_CREATED_BY_CUSTOMER,
                        NotificationCategory.CASE_EXCEED_SLA_RESPONSE_TIME,
                        NotificationCategory.CASE_EXCEED_SLA_RESOLUTION_TIME -> {
                            payload.arg?.let { caseId ->
                                navigation.navigateToEditCase(this@MainActivity, caseId)
                            }
                        }
                    }
                }
            }
            pushNotificationManager.removePayload()
        }
    }

    fun doLogout() {
        presenter.doLogout()
    }

    fun refreshCases() {
        caseFragment.getCases()
    }

    fun navigateToChangePassword() {
        navigation.navigateToChangePassword(this)
    }

    fun navigateToCreateCase(caseDuplication: CaseDuplication) {
        navigation.navigateToCreateCase(this, caseDuplication)
    }

    fun navigateToEditCase(caseId: String) {
        navigation.navigateToEditCase(this, caseId)
    }

    fun navigateToViewCaseDetails(case: Case) {
        navigation.navigateToViewCaseDetails(this, case.caseId, case.editable)
    }

    private fun observeOneSignalSubscription() {
        OneSignal.addSubscriptionObserver { state ->
            Logger.d(state.toString())
            fcmManager.service.saveFcmToken(state.to.userId)
        }
    }

    private fun registerPushTokenIfPossible() {
        fcmManager.service.registerFcmToken()
    }

    private fun handleDynamicLink(intent: Intent?) {
        intent?.let {
            FirebaseDynamicLinks
                .getInstance()
                .getDynamicLink(it)
                .addOnSuccessListener(this) { pendingDynamicLinkData ->
                    pendingDynamicLinkData?.link?.let { deepLink ->
                        Logger.d(deepLink.path)
                        val pathSegments = deepLink.pathSegments
                        if (pathSegments.contains("hds")) {
                            if (pathSegments.contains("case")) {
                                val id = pathSegments[pathSegments.size - 1]

                                navigateToEditCase(id)
                            }
                        }
                    }
                }
                .addOnFailureListener(this) { e ->
                    Logger.d(e.localizedMessage ?: "")
                }
        }
    }

    private fun setupViews() {
        if (ModulePermissionParser.isCasesAllowPost().not()) {
            text_view_nav_dashboard.gone()
        }

        tabFragments.apply {
            this.add(caseFragment)
            if (ModulePermissionParser.isCasesAllowPost()) {
                this.add(dashboardFragment)
            }
            this.add(caseRatingFragment)
            this.add(accountFragment)
        }

        view_pager.apply {
            this.adapter = MainViewPagerAdapter(this@MainActivity)
            this.offscreenPageLimit = 10
            this.isUserInputEnabled = false
        }
    }

    private fun setupListeners() {
        fab.click {
            navigation.navigateToCreateCase(this)
        }

        text_view_nav_home.click {
            if (selectedTab != MainNavTab.HOME) {
                selectedTab = MainNavTab.HOME

                selectTab()
            }
        }

        text_view_nav_dashboard.click {
            if (selectedTab != MainNavTab.DASHBOARD) {
                selectedTab = MainNavTab.DASHBOARD

                selectTab()
            }
        }

        text_view_nav_rating.click {
            if (selectedTab != MainNavTab.RATING) {
                selectedTab = MainNavTab.RATING

                selectTab()
            }
        }

        text_view_nav_account.click {
            if (selectedTab != MainNavTab.ACCOUNT) {
                selectedTab = MainNavTab.ACCOUNT

                selectTab()
            }
        }
    }

    private fun selectTab() {
        when (selectedTab) {
            MainNavTab.HOME -> {
                setActiveTab(text_view_nav_home)

                setInactiveTab(text_view_nav_dashboard)
                setInactiveTab(text_view_nav_rating)
                setInactiveTab(text_view_nav_account)

                view_pager.setCurrentItem(HOME_TAB, false)
            }

            MainNavTab.DASHBOARD -> {
                setActiveTab(text_view_nav_dashboard)

                setInactiveTab(text_view_nav_home)
                setInactiveTab(text_view_nav_rating)
                setInactiveTab(text_view_nav_account)

                view_pager.setCurrentItem(DASHBOARD_TAB, false)
            }

            MainNavTab.RATING -> {
                setActiveTab(text_view_nav_rating)

                setInactiveTab(text_view_nav_home)
                setInactiveTab(text_view_nav_dashboard)
                setInactiveTab(text_view_nav_account)

                view_pager.setCurrentItem(RATING_TAB, false)
            }

            MainNavTab.ACCOUNT -> {
                setActiveTab(text_view_nav_account)

                setInactiveTab(text_view_nav_home)
                setInactiveTab(text_view_nav_dashboard)
                setInactiveTab(text_view_nav_rating)

                view_pager.setCurrentItem(ACCOUNT_TAB, false)
            }
        }
    }

    private fun setActiveTab(textView: MaterialTextView) {
        val tabActiveColor = findColor(R.color.colorTabActive)

        textView.textColor = tabActiveColor
        textView.compoundDrawables.getOrNull(1)?.mutate()?.let { drawable ->
            drawable.colorFilter = PorterDuffColorFilter(tabActiveColor, PorterDuff.Mode.SRC_IN)
        }
    }

    private fun setInactiveTab(textView: MaterialTextView) {
        val tabInactiveColor = findColor(R.color.colorTabInactive)

        textView.textColor = tabInactiveColor
        textView.compoundDrawables.getOrNull(1)?.mutate()?.let { drawable ->
            drawable.colorFilter = PorterDuffColorFilter(tabInactiveColor, PorterDuff.Mode.SRC_IN)
        }
    }
    //endregion

    inner class MainViewPagerAdapter(fragmentActivity: FragmentActivity)
        : FragmentStateAdapter(fragmentActivity) {

        override fun getItemCount(): Int = tabFragments.size

        override fun createFragment(position: Int): Fragment {
            return tabFragments[position]
        }
    }
}
