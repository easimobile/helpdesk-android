package com.easipos.helpdesk.activities.create_edit_case.viewmodel

import android.app.Activity
import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.easipos.helpdesk.api.requests.cases.GetCaseInfoRequestModel
import com.easipos.helpdesk.base.CustomBaseAndroidViewModel
import com.easipos.helpdesk.models.CaseInfo
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import io.github.anderscheow.library.viewModel.util.AlertDialogData
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class CreateEditCaseViewModel(application: Application) : CustomBaseAndroidViewModel<Void>(application) {

    //region Observable Field
    val caseInfo = ObservableField<CaseInfo>()
    //endregion

    //region LiveData
    val caseInfoLD = MutableLiveData<CaseInfo>()
    //endregion

    private val caseRepository by instance<CaseRepository>()

    override fun start(args: Void?) {
    }

    fun getCaseInfo(activity: Activity, model: GetCaseInfoRequestModel) {
        showProgressDialog()
        viewModelScope.launch {
            when (val result = caseRepository.getCaseInfo(model)) {
                is Result.Success<CaseInfo> -> {
                    dismissProgressDialog()
                    setCaseInfo(result.data)
                }

                is Result.Error -> {
                    dismissProgressDialog()
                    showAlertDialog(AlertDialogData(
                        message = ErrorUtil.parseException(result.exception),
                        action = {
                            activity.finish()
                        }
                    ))
                }

                else -> {
                }
            }
        }
    }

    private fun setCaseInfo(caseInfo: CaseInfo) {
        this.caseInfo.set(caseInfo)
        this.caseInfo.notifyChange()

        caseInfoLD.postValue(caseInfo)
    }
}