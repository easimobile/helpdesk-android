package com.easipos.helpdesk.activities.view_case_details.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.create_edit_case_detail.CreateEditCaseDetailActivity
import com.easipos.helpdesk.models.CaseDetail

class ViewCaseDetailsNavigationImpl : ViewCaseDetailsNavigation {

    override fun navigateToCreateCaseDetail(activity: Activity, caseId: String) {
        activity.startActivity(CreateEditCaseDetailActivity.newIntent(activity, caseId, null))
    }

    override fun navigateToEditCaseDetail(activity: Activity, caseId: String, caseDetail: CaseDetail) {
        activity.startActivity(CreateEditCaseDetailActivity.newIntent(activity, caseId, caseDetail))
    }
}