package com.easipos.helpdesk.activities.view_case_details.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.easipos.helpdesk.api.requests.cases.GetCaseDetailsRequestModel
import com.easipos.helpdesk.base.CustomBaseAndroidViewModel
import com.easipos.helpdesk.models.CaseDetail
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class ViewCaseDetailsViewModel(application: Application) : CustomBaseAndroidViewModel<Void>(application) {

    //region Observable Field
    val caseDetails = ObservableField<List<CaseDetail>>()
    //endregion

    //region LiveData
    //endregion

    private var model: GetCaseDetailsRequestModel? = null

    private val caseRepository by instance<CaseRepository>()

    override fun start(args: Void?) {
    }

    override fun onRefresh() {
        model?.let { model ->
            getCaseDetails(model)
        }
    }

    fun getCaseDetails(model: GetCaseDetailsRequestModel) {
        this.model = model

        setIsLoading(true)
        viewModelScope.launch {
            when (val result = caseRepository.getCaseDetails(model)) {
                is Result.Success<List<CaseDetail>> -> {
                    finishLoading(result.data.size.toLong())
                    setCaseDetails(result.data)
                }

                is Result.Error -> {
                    setIsLoading(false)
                    showToast(ErrorUtil.parseException(result.exception))
                }

                else -> {
                }
            }
        }
    }

    private fun setCaseDetails(caseDetails: List<CaseDetail>) {
        this.caseDetails.set(caseDetails)
        this.caseDetails.notifyChange()
    }
}