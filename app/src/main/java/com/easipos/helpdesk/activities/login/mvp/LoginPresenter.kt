package com.easipos.helpdesk.activities.login.mvp

import android.app.Application
import com.easipos.helpdesk.R
import com.easipos.helpdesk.api.requests.auth.LoginRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.managers.UserManager
import com.easipos.helpdesk.models.Auth
import com.easipos.helpdesk.models.LoginInfo
import com.easipos.helpdesk.models.ResponseWrapper
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.auth.AuthRepository
import com.easipos.helpdesk.tools.Preference
import com.easipos.helpdesk.util.ApiErrorException
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class LoginPresenter(application: Application)
    : Presenter<LoginView>(application) {

    private val authRepository by instance<AuthRepository>()

    fun doLogin(model: LoginRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = authRepository.login(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<ResponseWrapper<LoginInfo>> -> {
                        view?.setLoadingIndicator(false)
                        when (result.data.code) {
                            200 -> {
                                Preference.prefUserCode = model.id
                                Preference.prefModulePermission = result.data.data.modules
                                UserManager.token = Auth(result.data.data.apiKey)

                                view?.navigateToMain()
                            }

                            302 -> {
                                Preference.prefUserCode = model.id
                                UserManager.token = Auth(result.data.data.apiKey)

                                view?.showErrorAlertDialog(application.getString(R.string.prompt_password_almost_expired)) {
                                    view?.navigateToMain()
                                }
                            }
                        }
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        if (result.exception is ApiErrorException) {
                            if (result.exception.code == 202) {
                                view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception)) {
                                    view?.navigateToChangePassword()
                                }
                                return@withContext
                            } else if (result.exception.code == 401) {
                                view?.showErrorAlertDialog(application.getString(R.string.prompt_password_expired)) {
                                    view?.navigateToChangePassword()
                                }
                                return@withContext
                            }
                        }
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
