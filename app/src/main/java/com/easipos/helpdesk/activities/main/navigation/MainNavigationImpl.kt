package com.easipos.helpdesk.activities.main.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.change_password.ChangePasswordActivity
import com.easipos.helpdesk.activities.create_edit_case.CreateEditCaseActivity
import com.easipos.helpdesk.activities.splash.SplashActivity
import com.easipos.helpdesk.activities.view_case_details.ViewCaseDetailsActivity
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.CaseDuplication

class MainNavigationImpl : MainNavigation {

    override fun navigateToSplash(activity: Activity) {
        activity.startActivity(SplashActivity.newIntent(activity))
        activity.finish()
    }

    override fun navigateToCreateCase(activity: Activity) {
        activity.startActivity(CreateEditCaseActivity.newIntent(activity, null, null))
    }

    override fun navigateToCreateCase(activity: Activity, caseDuplication: CaseDuplication) {
        activity.startActivity(CreateEditCaseActivity.newIntent(activity, null, caseDuplication))
    }

    override fun navigateToEditCase(activity: Activity, caseId: String) {
        activity.startActivity(CreateEditCaseActivity.newIntent(activity, caseId, null))
    }

    override fun navigateToChangePassword(activity: Activity) {
        activity.startActivity(ChangePasswordActivity.newIntent(activity))
    }

    override fun navigateToViewCaseDetails(activity: Activity, caseId: String, isEditable: Boolean) {
        activity.startActivity(ViewCaseDetailsActivity.newIntent(activity, caseId, isEditable, true))
    }
}