package com.easipos.helpdesk.activities.create_edit_case_detail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.util.Base64
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.create_edit_case_detail.mvp.CreateEditCaseDetailPresenter
import com.easipos.helpdesk.activities.create_edit_case_detail.mvp.CreateEditCaseDetailView
import com.easipos.helpdesk.activities.create_edit_case_detail.navigation.CreateEditCaseDetailNavigation
import com.easipos.helpdesk.activities.create_edit_case_detail.viewmodel.CreateEditCaseDetailViewModel
import com.easipos.helpdesk.activities.create_edit_case_detail.viewmodel.CreateEditCaseDetailViewModelFactory
import com.easipos.helpdesk.adapters.SelectionAdapter
import com.easipos.helpdesk.api.requests.cases.CreateCaseDetailRequestModel
import com.easipos.helpdesk.api.requests.cases.EditCaseDetailRequestModel
import com.easipos.helpdesk.api.requests.cases.GetCaseInfoRequestModel
import com.easipos.helpdesk.base.CustomLifecycleActivity
import com.easipos.helpdesk.bundle.ParcelData
import com.easipos.helpdesk.constant.CaseStatus
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.models.CaseDetail
import com.easipos.helpdesk.models.Selectable
import com.easipos.helpdesk.models.ServiceType
import com.easipos.helpdesk.util.showBottomSheet
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.stfalcon.imageviewer.StfalconImageViewer
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.activity_create_edit_case_detail.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import java.io.ByteArrayOutputStream

class CreateEditCaseDetailActivity : CustomLifecycleActivity<CreateEditCaseDetailViewModel>(),
    CreateEditCaseDetailView, SelectionAdapter.OnGestureDetectedListener {

    companion object {
        fun newIntent(context: Context, caseId: String, caseDetail: CaseDetail?): Intent {
            return Intent(context, CreateEditCaseDetailActivity::class.java).apply {
                this.putExtra(ParcelData.CASE_ID, caseId)
                this.putExtra(ParcelData.CASE_DETAIL, caseDetail)
            }
        }
    }

    //region Variables
    private val presenter by lazy { CreateEditCaseDetailPresenter(application) }
    private val factory by lazy { CreateEditCaseDetailViewModelFactory(application) }

    private val navigation by instance<CreateEditCaseDetailNavigation>()

    private val caseId by argument<String>(ParcelData.CASE_ID)
    private val caseDetail by argument<CaseDetail>(ParcelData.CASE_DETAIL)

    private var selectedStatus: CaseStatus? = null
    private var selectedServiceType: ServiceType? = null
    private var selectedScreenshot: Bitmap? = null
    private var isUserNegligenceChecked: Boolean = false

    private var selectionDialog: BottomSheetDialog? = null
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            try {
                ImagePicker.getFilePath(data)?.let { filePath ->
                    selectedScreenshot = BitmapFactory.decodeFile(filePath)
                    setImageToScreenshot()
                }
            } catch (ex: Exception) {
                toastMessage(ex.localizedMessage?.toString() ?: "")
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            toastMessage(ImagePicker.getError(data))
        }
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_create_edit_case_detail

    override fun setupViewModel(): CreateEditCaseDetailViewModel {
        return ViewModelProvider(this, factory)
            .get(CreateEditCaseDetailViewModel::class.java)
    }

    override fun setupViewModelObserver() {
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()

        loadCasDetail()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun showServiceTypeSelections(serviceTypes: List<ServiceType>) {
        val selectableList = serviceTypes.map { Selectable(it as Any) }
        selectedServiceType?.let { serviceType ->
            val selectable = selectableList.find {
                (it.data as ServiceType).serviceTypeCode == serviceType.serviceTypeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_service_type), selectableList)
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(position: Int, data: Any) {
        selectionDialog?.dismiss()

        when (data) {
            is CaseStatus -> {
                selectedStatus = data

                text_view_status.text = data.value
            }

            is ServiceType -> {
                selectedServiceType = data

                text_view_service_type.text = data.serviceTypeDesc
            }
        }
    }

    override fun onReturnItems(items: List<Any>) {
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        layout_screenshot.gone()

        caseDetail?.let { caseDetail ->
            if (caseDetail.editable) {
                edit_text_note.enable()
                fab.show()
            } else {
                edit_text_note.disable()
                fab.hide()
            }

            loadInitialScreenshot(caseDetail.screenshotInUrl)
        } ?: run {
            text_view_title.setText(R.string.title_create_new_case_detail)
        }
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        fab.click {
            attemptCreateEditCaseDetail()
        }

        card_view_status.click {
            showStatusSelections()
        }

        card_view_service_type.click {
            if (caseDetail == null || caseDetail!!.editable) {
                getServiceTypes()
            }
        }

        text_view_user_negligence.click {
            if (caseDetail == null || caseDetail!!.editable) {
                isUserNegligenceChecked = isUserNegligenceChecked.not()

                if (isUserNegligenceChecked) {
                    text_view_user_negligence.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        android.R.drawable.checkbox_on_background,
                        0,
                        0,
                        0
                    )
                } else {
                    text_view_user_negligence.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        android.R.drawable.checkbox_off_background,
                        0,
                        0,
                        0
                    )
                }

            }
        }

        image_view_add_screenshot.click {
            if (caseDetail == null || caseDetail!!.editable) {
                showImagePicker()
            }
        }

        image_view_remove.click {
            if (caseDetail == null || caseDetail!!.editable) {
                selectedScreenshot = null

                setImageToScreenshot()
            }
        }

        image_view_screenshot.click {
            showImageViewer()
        }
    }

    private fun loadCasDetail() {
        viewModel.start(caseDetail)

        caseDetail?.let { caseDetail ->
            selectedStatus = CaseStatus.parse(caseDetail.status)

            if (caseDetail.serviceTypeCode.isNotBlank()) {
                selectedServiceType = ServiceType(
                    serviceTypeCode = caseDetail.serviceTypeCode,
                    serviceTypeDesc = caseDetail.serviceTypeDesc
                )
            }

            isUserNegligenceChecked = caseDetail.isUserNegligence
        }
    }

    private fun getServiceTypes() {
        presenter.doGetServiceTypes()
    }

    private fun showStatusSelections() {
        if (caseDetail == null || caseDetail!!.editable) {
            val selectableList = CaseStatus.toList(caseDetail?.allowPost ?: false).map { Selectable(it as Any) }
            selectedStatus?.let { status ->
                val selectable = selectableList.find {
                    (it.data as CaseStatus).name == status.name
                }
                selectable?.isSelected = true
            }

            showBottomSheetSelection(getString(R.string.label_case_status), selectableList)
        }
    }

    private fun showBottomSheetSelection(title: String, data: List<Selectable<Any>>) {
        selectionDialog?.dismiss()
        selectionDialog = showBottomSheet(this, this, title, data)
        selectionDialog?.show()
    }

    private fun loadInitialScreenshot(screenshotInUrl: String) {
        screenshotInUrl.takeIf { it.isNotBlank() }?.let { url ->
            AsyncTask.execute {
                try {
                    selectedScreenshot = Glide.with(this).asBitmap().load(url).submit().get()
                    runOnUiThread {
                        setImageToScreenshot()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }
    }

    private fun showImagePicker() {
        ImagePicker.with(this)
            .compress(1024)
            .start()
    }

    private fun showImageViewer() {
        selectedScreenshot?.let { screenshot ->
            StfalconImageViewer.Builder(this, arrayOf(screenshot)) { view, image ->
                view.setImageBitmap(image)
            }.withTransitionFrom(image_view_screenshot)
                .withHiddenStatusBar(false)
                .show()
        }
    }

    private fun setImageToScreenshot() {
        selectedScreenshot?.let { screenshot ->
            image_view_screenshot.setImageBitmap(screenshot)
            layout_screenshot.visible()

            if (caseDetail != null && caseDetail!!.editable.not()) {
                image_view_remove.gone()
            } else {
                image_view_remove.visible()
            }
        } ?: run {
            image_view_screenshot.setImageBitmap(null)
            layout_screenshot.gone()
        }
    }

    private fun attemptCreateEditCaseDetail() {
        if (selectedStatus == null) {
            showErrorAlertDialog(getString(R.string.error_status_required))
            return
        }

        if ((edit_text_note.text?.toString() ?: "").isBlank()) {
            showErrorAlertDialog(getString(R.string.error_note_required))
            return
        }

        if (caseDetail == null) {
            createCaseDetail()
        } else {
            // Check whether status has changed
            viewModel.caseDetail.get()?.let { caseDetail ->
                if (caseDetail.status != selectedStatus?.name && selectedServiceType == null) {
                    // Status changed
                    showErrorAlertDialog(getString(R.string.error_service_type_required))
                    return
                }
            }

            editCaseDetail()
        }
    }

    private fun verifyStatusAndCaseSubType(callback: () -> Unit) {
        caseId?.let { caseId ->
            if (selectedStatus == CaseStatus.RESOLVED) {
                val model1 = GetCaseInfoRequestModel(
                    caseId = caseId
                )

                presenter.doVerifyCase(model1) { caseInfo ->
                    when {
                        caseInfo.hasCaseSubType1 && caseInfo.caseSubTypeCode1 == null -> {
                            showErrorAlertDialog(getString(R.string.error_case_sub_type_1_required))
                        }
                        caseInfo.hasCaseSubType2 && caseInfo.caseSubTypeCode2 == null -> {
                            showErrorAlertDialog(getString(R.string.error_case_sub_type_2_required))
                        }
                        caseInfo.hasCaseSubType3 && caseInfo.caseSubTypeCode3 == null -> {
                            showErrorAlertDialog(getString(R.string.error_case_sub_type_3_required))
                        }
                        else -> {
                            callback()
                        }
                    }
                }
            } else {
                callback()
            }
        }
    }

    private fun createCaseDetail() {
        caseId?.let { caseId ->
            verifyStatusAndCaseSubType {
                val model = CreateCaseDetailRequestModel(
                    caseId = caseId,
                    notes = edit_text_note.text?.toString() ?: "",
                    status = selectedStatus!!.name,
                    serviceType = selectedServiceType?.serviceTypeCode,
                    serviceReportNo = edit_text_service_report_number.text?.toString() ?: "",
                    screenshot = encodeImageToBase64(),
                    userNegligence = isUserNegligenceChecked
                )

                presenter.doCreateCaseDetail(model)
            }
        }
    }

    private fun editCaseDetail() {
        caseId?.let { caseId ->
            verifyStatusAndCaseSubType {
                val model = EditCaseDetailRequestModel(
                    caseId = caseId,
                    caseDetailId = caseDetail!!.caseDetailId,
                    notes = edit_text_note.text?.toString() ?: "",
                    status = selectedStatus!!.name,
                    serviceType = selectedServiceType?.serviceTypeCode,
                    serviceReportNo = edit_text_service_report_number.text?.toString() ?: "",
                    screenshot = encodeImageToBase64(),
                    userNegligence = isUserNegligenceChecked
                )

                presenter.doEditCaseDetail(model)
            }
        }
    }

    private fun encodeImageToBase64(): String? {
        selectedScreenshot?.let { screenshot ->
            val outputStream = ByteArrayOutputStream()
            screenshot.compress(Bitmap.CompressFormat.JPEG, 75, outputStream)
            val byteArray = outputStream.toByteArray()
            return Base64.encodeToString(byteArray, Base64.DEFAULT)
        }

        return null
    }
    //endregion
}
