package com.easipos.helpdesk.activities.create_edit_case.mvp

import android.app.Application
import com.easipos.helpdesk.R
import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.event_bus.RefreshCaseListing
import com.easipos.helpdesk.models.*
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.kodein.di.generic.instance
import java.util.*

class CreateEditCasePresenter(application: Application)
    : Presenter<CreateEditCaseView>(application) {

    private val caseRepository by instance<CaseRepository>()

    fun doGetCaseInfo(model: GetCaseInfoRequestModel, caseDuplication: CaseDuplication) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCaseInfo(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<CaseInfo> -> {
                        view?.setLoadingIndicator(false)
                        view?.populateDuplicateCase(caseDuplication, result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetProjects() {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getProjects()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<Project>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showProjectSelections(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetCustomerOutlets(model: GetCustomerOutletsRequestModel, autoPopulate: Boolean) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCustomerOutlets(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CustomerOutlet>> -> {
                        view?.setLoadingIndicator(false)
                        if (autoPopulate) {
                            if (result.data.isNotEmpty()) {
                                view?.autoPopulateFirstCustomerOutlet(result.data[0])
                            } else {
                                // Workaround to fix if else expression compile error
                                Unit
                            }
                        } else {
                            view?.showCustomerOutletSelections(result.data)
                        }
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetOutletPosList(model: GetOutletPosListRequestModel, autoPopulate: Boolean) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getOutletPosList(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<OutletPos>> -> {
                        view?.setLoadingIndicator(false)
                        if (autoPopulate) {
                            if (result.data.isNotEmpty()) {
                                view?.autoPopulateFirstOutletPos(result.data[0])
                            } else {
                                // Workaround to fix if else expression compile error
                                Unit
                            }
                        } else {
                            view?.showOutletPosSelections(result.data)
                        }
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetProjectSla(model: GetProjectSlaRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getProjectSla(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<ProjectSla> -> {
                        view?.setLoadingIndicator(false)
                        view?.autoPopulateFirstProjectSla(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.clearProjectSla()
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception)) {
                            doGetProjectSla(model)
                        }
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetOperationEmployees() {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getOperationEmployees()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<OperationEmployee>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showOperationEmployeeSelections(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetCaseTypes(autoPopulate: Boolean = false) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCaseTypes()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CaseType>> -> {
                        view?.setLoadingIndicator(false)

                        if (autoPopulate) {
                            result.data.find { it.caseTypeDesc.toLowerCase(Locale.getDefault()) == "maintenance" }?.let { caseType ->
                                view?.autoPopulateCaseType(caseType)
                            }
                        } else {
                            view?.showCaseTypeSelection(result.data)
                        }
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetCaseSubTypes1(model: GetCaseSubTypesRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCaseSubTypes1(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CaseSubType>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showCaseSubType1Selection(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetCaseSubTypes2(model: GetCaseSubTypesRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCaseSubTypes2(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CaseSubType>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showCaseSubType2Selection(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetCaseSubTypes3(model: GetCaseSubTypesRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getCaseSubTypes3(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<CaseSubType>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showCaseSubType3Selection(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doCreateCase(model: CreateCaseRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.createCase(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<String> -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(application.getString(R.string.prompt_case_create_successful)) {
                            view?.finishScreen()
                            view?.navigateToViewCaseDetails(result.data)
                        }

                        EventBus.getDefault().post(RefreshCaseListing())
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doEditCase(model: EditCaseRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.editCase(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(application.getString(R.string.prompt_case_update_successful)) {
                            view?.finishScreen()
                        }

                        EventBus.getDefault().post(RefreshCaseListing())
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
