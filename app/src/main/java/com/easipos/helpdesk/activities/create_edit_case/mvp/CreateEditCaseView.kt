package com.easipos.helpdesk.activities.create_edit_case.mvp

import com.easipos.helpdesk.base.View
import com.easipos.helpdesk.models.*

interface CreateEditCaseView : View {

    fun populateDuplicateCase(caseDuplication: CaseDuplication, caseInfo: CaseInfo)

    fun showProjectSelections(projects: List<Project>)

    fun showCustomerOutletSelections(customerOutlets: List<CustomerOutlet>)

    fun autoPopulateFirstCustomerOutlet(customerOutlet: CustomerOutlet)

    fun showOutletPosSelections(outletPosList: List<OutletPos>)

    fun autoPopulateFirstOutletPos(outletPos: OutletPos)

    fun showProjectSlaSelections(projectSlaList: List<ProjectSla>)

    fun autoPopulateFirstProjectSla(projectSla: ProjectSla)

    fun showOperationEmployeeSelections(operationEmployees: List<OperationEmployee>)

    fun showCaseTypeSelection(caseTypes: List<CaseType>)

    fun autoPopulateCaseType(caseType: CaseType)

    fun showCaseSubType1Selection(caseSubTypes: List<CaseSubType>)

    fun showCaseSubType2Selection(caseSubTypes: List<CaseSubType>)

    fun showCaseSubType3Selection(caseSubTypes: List<CaseSubType>)

    fun navigateToViewCaseDetails(caseId: String)

    fun clearProjectSla()

    fun finishScreen()
}
