package com.easipos.helpdesk.activities.reset_password

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.reset_password.mvp.ResetPasswordPresenter
import com.easipos.helpdesk.activities.reset_password.mvp.ResetPasswordView
import com.easipos.helpdesk.activities.reset_password.navigation.ResetPasswordNavigation
import com.easipos.helpdesk.api.requests.user.ForgotPasswordRequestModel
import com.easipos.helpdesk.base.CustomBaseAppCompatActivity
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.notBlank
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class ResetPasswordActivity : CustomBaseAppCompatActivity(), ResetPasswordView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ResetPasswordActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<ResetPasswordNavigation>()
    private val validator by instance<Validator>()

    private val presenter by lazy { ResetPasswordPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_reset_password

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_reset.click {
            attemptForgotPassword()
        }
    }

    private fun attemptForgotPassword() {
        val userIdlValidation = text_input_layout_user_id.validate()
            .notBlank(R.string.error_field_required)

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val userId = values[0]

                val model = ForgotPasswordRequestModel(
                    id = userId
                )

                presenter.doForgotPassword(model)
            }
        }).validate(userIdlValidation)
    }
    //endregion
}
