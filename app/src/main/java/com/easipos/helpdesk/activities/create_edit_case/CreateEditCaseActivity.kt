package com.easipos.helpdesk.activities.create_edit_case

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.util.Base64
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.easipos.helpdesk.BuildConfig
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.create_edit_case.mvp.CreateEditCasePresenter
import com.easipos.helpdesk.activities.create_edit_case.mvp.CreateEditCaseView
import com.easipos.helpdesk.activities.create_edit_case.navigation.CreateEditCaseNavigation
import com.easipos.helpdesk.activities.create_edit_case.viewmodel.CreateEditCaseViewModel
import com.easipos.helpdesk.activities.create_edit_case.viewmodel.CreateEditCaseViewModelFactory
import com.easipos.helpdesk.adapters.SelectionAdapter
import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.base.CustomLifecycleActivity
import com.easipos.helpdesk.bundle.ParcelData
import com.easipos.helpdesk.constant.CasePriority
import com.easipos.helpdesk.event_bus.RefreshCase
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.models.*
import com.easipos.helpdesk.tools.BindingAdapterUtil
import com.easipos.helpdesk.util.showBottomSheet
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.stfalcon.imageviewer.StfalconImageViewer
import io.github.anderscheow.library.constant.EventBusType
import io.github.anderscheow.library.kotlinExt.*
import io.github.anderscheow.library.viewModel.util.AlertDialogData
import kotlinx.android.synthetic.main.activity_create_edit_case.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.longToast
import org.jetbrains.anko.share
import org.kodein.di.generic.instance
import java.io.ByteArrayOutputStream

class CreateEditCaseActivity : CustomLifecycleActivity<CreateEditCaseViewModel>(), CreateEditCaseView,
    SelectionAdapter.OnGestureDetectedListener {

    companion object {
        // caseId for View or Edit Case
        // caseDuplication for Copy or Duplicate Case
        fun newIntent(context: Context, caseId: String?, caseDuplication: CaseDuplication?): Intent {
            return Intent(context, CreateEditCaseActivity::class.java).apply {
                this.putExtra(ParcelData.CASE_ID, caseId)
                this.putExtra(ParcelData.DUPLICATE_CASE, caseDuplication)
            }
        }
    }

    //region Variables
    private val presenter by lazy { CreateEditCasePresenter(application) }
    private val factory by lazy { CreateEditCaseViewModelFactory(application) }

    private val navigation by instance<CreateEditCaseNavigation>()

    private val caseId by argument<String>(ParcelData.CASE_ID)
    private val caseDuplication by argument<CaseDuplication>(ParcelData.DUPLICATE_CASE)

    private var caseInfo: CaseInfo? = null

    private var selectedAssignee: OperationEmployee? = null
    private var selectedPriority: CasePriority? = null
    private var selectedCaseType: CaseType? = null
    private var selectedCaseSubType1: CaseSubType? = null
    private var selectedCaseSubType2: CaseSubType? = null
    private var selectedCaseSubType3: CaseSubType? = null
    private var selectedProject: Project? = null
    private var selectedOutlet: CustomerOutlet? = null
    private var selectedPos: OutletPos? = null
    private var selectedSla: ProjectSla? = null
    private var selectedScreenshot: Bitmap? = null

    private var selectionDialog: BottomSheetDialog? = null
    private var isCaseSubType1Required = false
    private var isCaseSubType2Required = false
    private var isCaseSubType3Required = false
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            try {
                ImagePicker.getFilePath(data)?.let { filePath ->
                    selectedScreenshot = BitmapFactory.decodeFile(filePath)
                    setImageToScreenshot()
                }
            } catch (ex: Exception) {
                toastMessage(ex.localizedMessage?.toString() ?: "")
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            toastMessage(ImagePicker.getError(data))
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_create_edit_case

    override fun getEventBusType(): EventBusType? = EventBusType.ON_CREATE

    override fun setupViewModel(): CreateEditCaseViewModel {
        return ViewModelProvider(this, factory)
            .get(CreateEditCaseViewModel::class.java)
    }

    override fun setupViewModelObserver() {
        viewModel.caseInfoLD.observe(this, Observer { caseInfo ->
            this.caseInfo = caseInfo

            if (caseInfo.editable) {
                edit_text_contact_person.enable()
                edit_text_contact_no.enable()
                edit_text_remark.enable()
                fab.show()
            } else {
                edit_text_contact_person.disable()
                edit_text_contact_no.disable()
                edit_text_remark.disable()
                fab.hide()
            }

            populateCaseVariables(caseInfo)
        })
    }

    override fun showAlertDialog(data: AlertDialogData) {
        showErrorAlertDialog(data.message, null, data.action ?: {})
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun populateDuplicateCase(caseDuplication: CaseDuplication, caseInfo: CaseInfo) {
        delay(500) {
            populateCaseVariables(caseInfo)

            if (caseDuplication.duplicationType == DuplicationType.DUPLICATE) {
                card_view_linked_case.visible()
                text_view_linked_case.text = caseInfo.formatCaseId()

                image_view_case_response.gone()
                image_view_add_screenshot.gone()

                card_view_project.layoutParams = (card_view_project.layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
                    this.topMargin = resources.getDimensionPixelSize(R.dimen.sixteen_dp)
                }
            } else {
                card_view_linked_case.gone()

                image_view_case_response.visible()
                image_view_add_screenshot.visible()

                card_view_project.layoutParams = (card_view_project.layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
                    this.topMargin = 0
                }
            }

            edit_text_contact_person.setText(caseInfo.contactPerson)
            edit_text_contact_no.setText(caseInfo.contactNo)
            edit_text_remark.setText(caseInfo.remarks)

            card_view_case_type.layoutParams = (card_view_case_type.layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
                this.topMargin = resources.getDimensionPixelSize(R.dimen.sixteen_dp)
            }

            selectedAssignee?.let { operationEmployee ->
                text_view_assign_to.text = operationEmployee.employeeName
            }

            selectedPriority?.let { priority ->
                text_view_priority.text = priority.value

                BindingAdapterUtil.setPriorityIconAndTextColor(text_view_priority, priority.toInt())
            }

            selectedCaseType?.let { caseType ->
                text_view_case_type.text = caseType.caseTypeDesc
            }

            selectedCaseSubType1?.let { caseSubType ->
                card_view_case_sub_type_1.visible()
                text_view_case_sub_type_1.text = caseSubType.caseSubTypeDesc
            } ?: run {
                card_view_case_sub_type_1.invisible()
            }

            selectedCaseSubType2?.let { caseSubType ->
                card_view_case_sub_type_2.visible()
                text_view_case_sub_type_2.text = caseSubType.caseSubTypeDesc
            } ?: run {
                card_view_case_sub_type_2.gone()
            }

            selectedCaseSubType3?.let { caseSubType ->
                card_view_case_sub_type_3.visible()
                text_view_case_sub_type_3.text = caseSubType.caseSubTypeDesc
            } ?: run {
                card_view_case_sub_type_3.gone()
            }

            selectedProject?.let { project ->
                text_view_project.text = project.projectName
            }

            selectedOutlet?.let { customerOutlet ->
                card_view_outlet.visible()
                text_view_outlet.text = customerOutlet.toString()
            } ?: run {
                card_view_outlet.gone()
            }

            selectedPos?.let { outletPos ->
                card_view_pos.visible()
                text_view_pos.text = outletPos.posDesc
            } ?: run {
                card_view_pos.gone()
            }

            selectedSla?.let { sla ->
                card_view_sla.visible()
                text_view_sla.text = sla.toString()
            } ?: run {
                card_view_sla.gone()
            }
        }
    }

    override fun showProjectSelections(projects: List<Project>) {
        val selectableList = projects.map { Selectable(it as Any) }
        selectedProject?.let { project ->
            val selectable = selectableList.find {
                (it.data as Project).projectCode == project.projectCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_project), selectableList)
    }

    override fun showCustomerOutletSelections(customerOutlets: List<CustomerOutlet>) {
        val selectableList = customerOutlets.map { Selectable(it as Any) }
        selectedOutlet?.let { customerOutlet ->
            val selectable = selectableList.find {
                (it.data as CustomerOutlet).outletCode == customerOutlet.outletCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_outlet), selectableList)
    }

    override fun autoPopulateFirstCustomerOutlet(customerOutlet: CustomerOutlet) {
        onSelectItem(-1, customerOutlet)
    }

    override fun showOutletPosSelections(outletPosList: List<OutletPos>) {
        val selectableList = outletPosList.map { Selectable(it as Any) }
        selectedPos?.let { outletPos ->
            val selectable = selectableList.find {
                (it.data as OutletPos).posNo == outletPos.posNo
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_pos), selectableList)
    }

    override fun autoPopulateFirstOutletPos(outletPos: OutletPos) {
        onSelectItem(-1, outletPos)
    }

    override fun showProjectSlaSelections(projectSlaList: List<ProjectSla>) {
        val selectableList = projectSlaList.map { Selectable(it as Any) }
        selectedSla?.let { sla ->
            val selectable = selectableList.find {
                (it.data as ProjectSla).slaCode == sla.slaCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_sla), selectableList)
    }

    override fun autoPopulateFirstProjectSla(projectSla: ProjectSla) {
        onSelectItem(-1, projectSla)
    }

    override fun showOperationEmployeeSelections(operationEmployees: List<OperationEmployee>) {
        val selectableList = operationEmployees.map { Selectable(it as Any) }
        selectedAssignee?.let { operationEmployee ->
            val selectable = selectableList.find {
                (it.data as OperationEmployee).employeeCode == operationEmployee.employeeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_assign_to), selectableList)
    }

    override fun showCaseTypeSelection(caseTypes: List<CaseType>) {
        val selectableList = caseTypes.map { Selectable(it as Any) }
        selectedCaseType?.let { caseType ->
            val selectable = selectableList.find {
                (it.data as CaseType).caseTypeCode == caseType.caseTypeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_type), selectableList)
    }

    override fun autoPopulateCaseType(caseType: CaseType) {
        selectedCaseType = caseType

        text_view_case_type.text = caseType.caseTypeDesc
    }

    override fun showCaseSubType1Selection(caseSubTypes: List<CaseSubType>) {
        val selectableList = caseSubTypes.map { Selectable(CaseSubTypeLayering(it, 1) as Any) }
        selectedCaseSubType1?.let { caseSubType ->
            val selectable = selectableList.find {
                (it.data as CaseSubTypeLayering).caseSubType.caseSubTypeCode == caseSubType.caseSubTypeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_sub_type_1), selectableList)
    }

    override fun showCaseSubType2Selection(caseSubTypes: List<CaseSubType>) {
        val selectableList = caseSubTypes.map { Selectable(CaseSubTypeLayering(it, 2) as Any) }
        selectedCaseSubType2?.let { caseSubType ->
            val selectable = selectableList.find {
                (it.data as CaseSubTypeLayering).caseSubType.caseSubTypeCode == caseSubType.caseSubTypeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_sub_type_2), selectableList)
    }

    override fun showCaseSubType3Selection(caseSubTypes: List<CaseSubType>) {
        val selectableList = caseSubTypes.map { Selectable(CaseSubTypeLayering(it, 3) as Any) }
        selectedCaseSubType3?.let { caseSubType ->
            val selectable = selectableList.find {
                (it.data as CaseSubTypeLayering).caseSubType.caseSubTypeCode == caseSubType.caseSubTypeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_sub_type_3), selectableList)
    }

    override fun navigateToViewCaseDetails(caseId: String) {
        navigation.navigateToViewCaseDetails(this, caseId, true)
    }

    override fun clearProjectSla() {
        selectedSla = null
        text_view_sla.text = null
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(position: Int, data: Any) {
        selectionDialog?.dismiss()

        when (data) {
            is OperationEmployee -> {
                selectedAssignee = data

                text_view_assign_to.text = data.employeeName
            }

            is Project -> {
                selectedProject = data
                selectedOutlet = null
                selectedPos = null
                selectedSla = null

                text_view_project.text = data.toString()
                text_view_outlet.text = null
                text_view_pos.text = null
                text_view_sla.text = null

                card_view_outlet.visible()
                card_view_pos.gone()
                card_view_sla.visible()

                if (data.outletCount == 1) {
                    getCustomerOutlets(true)
                }

                getProjectSlaList()
            }

            is CustomerOutlet -> {
                selectedOutlet = data
                selectedPos = null

                text_view_outlet.text = data.toString()
                text_view_pos.text = null

                card_view_pos.visible()

                if (data.posCount == 1) {
                    getOutletPosList(true)
                }
            }

            is OutletPos -> {
                selectedPos = data

                text_view_pos.text = data.toString()
            }

            is CaseType -> {
                selectedCaseType = data
                selectedCaseSubType1 = null
                selectedCaseSubType2 = null
                selectedCaseSubType3 = null

                text_view_case_type.text = data.toString()
                text_view_case_sub_type_1.text = null
                text_view_case_sub_type_2.text = null
                text_view_case_sub_type_3.text = null

                card_view_case_sub_type_2.gone()
                card_view_case_sub_type_3.gone()

                if (data.hasCaseSubType) {
                    isCaseSubType1Required = true
                    card_view_case_sub_type_1.visible()
                } else {
                    isCaseSubType1Required = false
                    card_view_case_sub_type_1.invisible()
                }

                isCaseSubType2Required = false
                isCaseSubType3Required = false
            }

            is CaseSubTypeLayering -> {
                when (data.level) {
                    1 -> { // CaseSubType1
                        selectedCaseSubType1 = data.caseSubType
                        selectedCaseSubType2 = null
                        selectedCaseSubType3 = null

                        text_view_case_sub_type_1.text = data.caseSubType.toString()
                        text_view_case_sub_type_2.text = null
                        text_view_case_sub_type_3.text = null

                        if (data.caseSubType.hasCaseSubType) {
                            isCaseSubType2Required = true
                            card_view_case_sub_type_2.visible()
                            card_view_case_sub_type_3.invisible()
                        } else {
                            isCaseSubType2Required = false
                            card_view_case_sub_type_2.gone()
                            card_view_case_sub_type_3.gone()
                        }

                        isCaseSubType3Required = false
                    }

                    2 -> { // CaseSubType2
                        selectedCaseSubType2 = data.caseSubType
                        selectedCaseSubType3 = null

                        text_view_case_sub_type_2.text = data.caseSubType.toString()
                        text_view_case_sub_type_3.text = null

                        if (data.caseSubType.hasCaseSubType) {
                            isCaseSubType3Required = true
                            card_view_case_sub_type_3.visible()
                        } else {
                            isCaseSubType3Required = false
                            card_view_case_sub_type_3.invisible()
                        }
                    }

                    3 -> { // CaseSubType3
                        selectedCaseSubType3 = data.caseSubType

                        text_view_case_sub_type_3.text = data.caseSubType.toString()
                    }
                }
            }

            is CasePriority -> {
                selectedPriority = data

                text_view_priority.text = data.value

                BindingAdapterUtil.setPriorityIconAndTextColor(text_view_priority, data.toInt())

                getProjectSlaList()
            }

            is ProjectSla -> {
                selectedSla = data

                text_view_sla.text = data.toString()
            }
        }
    }

    override fun onReturnItems(items: List<Any>) {
    }
    //endregion

    //region Action Methods
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: Any) {
        if (event is RefreshCase) {
            loadCaseIfNecessary()
        }
    }

    private fun setupViews() {
        card_view_case_sub_type_2.gone()
        card_view_case_sub_type_3.gone()
        card_view_sla.gone()
        layout_screenshot.gone()
        
        if (caseId == null) {
            text_view_title.setText(R.string.title_create_new_case)
        }

        loadCaseIfNecessary()
        setDefaultPriorityIfNecessary()
        setDefaultCaseType()
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        image_view_case_response.click {
            caseInfo?.let { case ->
                navigation.navigateToViewCaseDetails(this, case.caseId, case.editable)
            }
        }

        image_view_share.click {
            generateDynamicLink()
        }

        fab.click {
            attemptCreateEditCase()
        }

        card_view_linked_case.click {
            if (caseDuplication != null) {
                navigation.navigateToEditCase(this, caseDuplication!!.caseId)
            } else if (caseInfo != null && caseInfo!!.originalCaseId != null) {
                navigation.navigateToEditCase(this, caseInfo!!.originalCaseId!!)
            }
        }

        card_view_assign_to.click {
            if (caseInfo == null || caseInfo!!.editable) {
                getOperationEmployees()
            }
        }

        card_view_priority.click {
            if (caseInfo == null || caseInfo!!.editable) {
                showPrioritySelections()
            }
        }

        card_view_case_type.click {
            if (caseInfo == null || caseInfo!!.editable) {
                getCaseTypes()
            }
        }

        card_view_case_sub_type_1.click {
            if (caseInfo == null || caseInfo!!.editable) {
                getCaseSubTypes1()
            }
        }

        card_view_case_sub_type_2.click {
            if (caseInfo == null || caseInfo!!.editable) {
                getCaseSubTypes2()
            }
        }

        card_view_case_sub_type_3.click {
            if (caseInfo == null || caseInfo!!.editable) {
                getCaseSubTypes3()
            }
        }

        card_view_project.click {
            if (caseInfo == null || caseInfo!!.editable) {
                getProjects()
            }
        }

        card_view_outlet.click {
            if (caseInfo == null || caseInfo!!.editable) {
                getCustomerOutlets(false)
            }
        }

        card_view_pos.click {
            if (caseInfo == null || caseInfo!!.editable) {
                getOutletPosList(false)
            }
        }

        image_view_add_screenshot.click {
            if (caseInfo == null || caseInfo!!.editable) {
                showImagePicker()
            }
        }

        image_view_remove.click {
            if (caseInfo == null || caseInfo!!.editable) {
                selectedScreenshot = null

                setImageToScreenshot()
            }
        }

        image_view_screenshot.click {
            showImageViewer()
        }
    }

    private fun loadCaseIfNecessary() {
        caseId?.let { caseId ->
            val model = GetCaseInfoRequestModel(
                caseId = caseId
            )

            viewModel.getCaseInfo(this, model)
        }

        caseDuplication?.let { caseDuplication ->
            val model = GetCaseInfoRequestModel(
                caseId = caseDuplication.caseId
            )

            presenter.doGetCaseInfo(model, caseDuplication)
        }
    }

    private fun setDefaultPriorityIfNecessary() {
        if (caseId == null && caseDuplication == null) {
            delay(500) {
                val priority = CasePriority.LOW
                selectedPriority = priority

                text_view_priority.text = priority.value

                BindingAdapterUtil.setPriorityIconAndTextColor(text_view_priority, priority.toInt())
            }
        }
    }

    private fun setDefaultCaseType() {
        if (caseId == null && caseDuplication == null) {
            presenter.doGetCaseTypes(true)
        }
    }

    private fun populateCaseVariables(caseInfo: CaseInfo) {
        if (caseInfo.assignedTo.isNotBlank()) {
            selectedAssignee = OperationEmployee(
                employeeCode = caseInfo.assignedTo,
                employeeName = caseInfo.assignedToName
            )
        }

        selectedPriority = CasePriority.parse(caseInfo.priority)

        if (caseInfo.caseTypeCode.isNotBlank()) {
            selectedCaseType = CaseType(
                caseTypeCode = caseInfo.caseTypeCode,
                caseTypeDesc = caseInfo.caseTypeDesc,
                hasCaseSubType = caseInfo.hasCaseSubType1
            )
        }

        if (caseInfo.caseSubTypeCode1 != null) {
            selectedCaseSubType1 = CaseSubType(
                caseSubTypeCode = caseInfo.caseSubTypeCode1,
                caseSubTypeDesc = caseInfo.caseSubTypeDesc1 ?: "",
                hasCaseSubType = caseInfo.hasCaseSubType2
            )
        }

        if (caseInfo.caseSubTypeCode2 != null) {
            selectedCaseSubType2 = CaseSubType(
                caseSubTypeCode = caseInfo.caseSubTypeCode2,
                caseSubTypeDesc = caseInfo.caseSubTypeDesc2 ?: "",
                hasCaseSubType = caseInfo.hasCaseSubType3
            )
        }

        if (caseInfo.caseSubTypeCode3 != null) {
            selectedCaseSubType3 = CaseSubType(
                caseSubTypeCode = caseInfo.caseSubTypeCode3,
                caseSubTypeDesc = caseInfo.caseSubTypeDesc3 ?: "",
                hasCaseSubType = false
            )
        }

        if (caseInfo.projectCode.isNotBlank()) {
            selectedProject = Project(
                projectCode = caseInfo.projectCode,
                projectName = caseInfo.projectName,
                outletCount = 0,
                slaCount = 0
            )
        }

        if (caseInfo.outletCode.isNotBlank()) {
            selectedOutlet = CustomerOutlet(
                outletCode = caseInfo.outletCode,
                outletName = caseInfo.outletName,
                address1 = caseInfo.address1,
                address2 = caseInfo.address2,
                address3 = caseInfo.address3,
                address4 = caseInfo.address4,
                address5 = caseInfo.address5,
                postalCode = caseInfo.postalCode,
                posCount = 0
            )
        }

        if (caseInfo.posNo.isNotBlank()) {
            selectedPos = OutletPos(
                posNo = caseInfo.posNo,
                posDesc = caseInfo.posDesc,
                ipAddress = caseInfo.ipAddress
            )
        }

        if (caseInfo.slaCode.isNotBlank()) {
            selectedSla = ProjectSla(
                slaCode = caseInfo.slaCode,
                priority = caseInfo.priority,
                responseTime = caseInfo.responseTime,
                resolutionTime = caseInfo.resolutionTime
            )
        }

        loadInitialScreenshot(caseInfo.screenshotInUrl)
    }

    private fun getOperationEmployees() {
        presenter.doGetOperationEmployees()
    }

    private fun getCaseTypes() {
        presenter.doGetCaseTypes()
    }

    private fun getCaseSubTypes1() {
        selectedCaseType?.let {
            val model = GetCaseSubTypesRequestModel(
                code = it.caseTypeCode
            )

            presenter.doGetCaseSubTypes1(model)
        }
    }

    private fun getCaseSubTypes2() {
        selectedCaseSubType1?.let {
            val model = GetCaseSubTypesRequestModel(
                code = it.caseSubTypeCode
            )

            presenter.doGetCaseSubTypes2(model)
        }
    }

    private fun getCaseSubTypes3() {
        selectedCaseSubType2?.let {
            val model = GetCaseSubTypesRequestModel(
                code = it.caseSubTypeCode
            )

            presenter.doGetCaseSubTypes3(model)
        }
    }

    private fun getProjects() {
        presenter.doGetProjects()
    }

    private fun getCustomerOutlets(autoPopulate: Boolean) {
        selectedProject?.let {
            val model = GetCustomerOutletsRequestModel(
                projectCode = it.projectCode
            )

            presenter.doGetCustomerOutlets(model, autoPopulate)
        }
    }

    private fun getOutletPosList(autoPopulate: Boolean) {
        selectedOutlet?.let {
            val model = GetOutletPosListRequestModel(
                outletCode = it.outletCode
            )

            presenter.doGetOutletPosList(model, autoPopulate)
        }
    }

    private fun getProjectSlaList() {
        selectedProject?.let { project ->
            selectedPriority?.let { priority ->
                val model = GetProjectSlaRequestModel(
                    projectCode = project.projectCode,
                    priority = priority.toInt()
                )

                presenter.doGetProjectSla(model)
            }
        }
    }

    private fun showPrioritySelections() {
        val selectableList = CasePriority.toList().map { Selectable(it as Any) }
        selectedPriority?.let { priority ->
            val selectable = selectableList.find {
                (it.data as CasePriority).name == priority.name
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_priority), selectableList)
    }

    private fun showBottomSheetSelection(title: String, data: List<Selectable<Any>>) {
        selectionDialog?.dismiss()
        selectionDialog = showBottomSheet(this, this, title, data)
        selectionDialog?.show()
    }

    private fun loadInitialScreenshot(screenshotInUrl: String) {
        screenshotInUrl.takeIf { it.isNotBlank() }?.let { url ->
            AsyncTask.execute {
                try {
                    selectedScreenshot = Glide.with(this).asBitmap().load(url).submit().get()
                    runOnUiThread {
                        setImageToScreenshot()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }
    }

    private fun showImagePicker() {
        ImagePicker.with(this)
            .compress(1024)
            .start()
    }

    private fun showImageViewer() {
        selectedScreenshot?.let { screenshot ->
            StfalconImageViewer.Builder(this, arrayOf(screenshot)) { view, image ->
                view.setImageBitmap(image)
            }.withTransitionFrom(image_view_screenshot)
                .withHiddenStatusBar(false)
                .show()
        }
    }

    private fun setImageToScreenshot() {
        selectedScreenshot?.let { screenshot ->
            image_view_screenshot.setImageBitmap(screenshot)
            layout_screenshot.visible()

            if (caseInfo != null && caseInfo!!.editable.not()) {
                image_view_remove.gone()
            } else {
                image_view_remove.visible()
            }
        } ?: run {
            image_view_screenshot.setImageBitmap(null)
            layout_screenshot.gone()
        }
    }

    private fun generateDynamicLink() {
        caseId?.let { caseId ->
            setLoadingIndicator(true)
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://www.easipos.com/hds/case/$caseId"))
                .setDomainUriPrefix("https://easipos.page.link")
                .setAndroidParameters(DynamicLink.AndroidParameters.Builder(BuildConfig.APPLICATION_ID).build())
                .setSocialMetaTagParameters(
                    DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle("Case #$caseId")
                        .build()
                )
                .buildShortDynamicLink()
                .addOnCompleteListener(this) { task ->
                    setLoadingIndicator(false)
                    if (task.isSuccessful) {
                        task.result?.let { result ->
                            val textToShare = "Case #$caseId\n\n${result.shortLink}"
                            share(textToShare)
                        }
                    } else {
                        showErrorAlertDialog(task.exception?.localizedMessage ?: "Unknown error.")
                    }
                }
        }
    }

    private fun attemptCreateEditCase() {
        if (selectedProject == null) {
            showErrorAlertDialog(getString(R.string.error_project_required))
            return
        }

        if (selectedOutlet == null) {
            showErrorAlertDialog(getString(R.string.error_outlet_required))
            return
        }

        if (selectedPos == null) {
            showErrorAlertDialog(getString(R.string.error_pos_required))
            return
        }

        if (selectedCaseType == null) {
            showErrorAlertDialog(getString(R.string.error_case_type_required))
            return
        }

        if (isCaseSubType1Required && selectedCaseSubType1 == null) {
            showErrorAlertDialog(getString(R.string.error_case_sub_type_1_required))
            return
        }

        if (edit_text_remark.text.isNullOrBlank()) {
            showErrorAlertDialog(getString(R.string.error_remark_required))
            return
        }

        if (selectedPriority == null) {
            showErrorAlertDialog(getString(R.string.error_priority_required))
            return
        }

        // Remove mandatory checking on case sub type 2 and 3
//        if (isCaseSubType2Required && selectedCaseSubType2 == null) {
//            showErrorAlertDialog(getString(R.string.error_case_sub_type_2_required))
//            return
//        }
//
//        if (isCaseSubType3Required && selectedCaseSubType3 == null) {
//            showErrorAlertDialog(getString(R.string.error_case_sub_type_3_required))
//            return
//        }

        if (selectedSla == null) {
            showErrorAlertDialog(getString(R.string.error_sla_required))
            return
        }

        if (caseId == null) {
            createCase()
        } else {
            editCase()
        }
    }

    private fun createCase() {
        val originalCaseId = if (caseDuplication?.duplicationType == DuplicationType.DUPLICATE) {
            caseDuplication?.caseId
        } else {
            null
        }

        val model = CreateCaseRequestModel(
            projectCode = selectedProject!!.projectCode,
            assignedTo = selectedAssignee?.employeeCode,
            outletCode = selectedOutlet!!.outletCode,
            posNo = selectedPos!!.posNo,
            priority = selectedPriority!!.toInt(),
            contactPerson = edit_text_contact_person.text?.toString(),
            contactNo = edit_text_contact_no.text?.toString(),
            remarks = edit_text_remark.text?.toString(),
            caseTypeCode = selectedCaseType!!.caseTypeCode,
            caseSubTypeCode1 = selectedCaseSubType1?.caseSubTypeCode,
            caseSubTypeCode2 = selectedCaseSubType2?.caseSubTypeCode,
            caseSubTypeCode3 = selectedCaseSubType3?.caseSubTypeCode,
            screenshot = encodeImageToBase64(),
            originalCaseId = originalCaseId,
            slaCode = selectedSla!!.slaCode
        )

        presenter.doCreateCase(model)
    }

    private fun editCase() {
        val model = EditCaseRequestModel(
            caseId = caseId!!,
            projectCode = selectedProject!!.projectCode,
            assignedTo = selectedAssignee?.employeeCode,
            outletCode = selectedOutlet!!.outletCode,
            posNo = selectedPos!!.posNo,
            priority = selectedPriority!!.toInt(),
            contactPerson = edit_text_contact_person.text?.toString(),
            contactNo = edit_text_contact_no.text?.toString(),
            remarks = edit_text_remark.text?.toString(),
            caseTypeCode = selectedCaseType!!.caseTypeCode,
            caseSubTypeCode1 = selectedCaseSubType1?.caseSubTypeCode,
            caseSubTypeCode2 = selectedCaseSubType2?.caseSubTypeCode,
            caseSubTypeCode3 = selectedCaseSubType3?.caseSubTypeCode,
            screenshot = encodeImageToBase64(),
            slaCode = selectedSla!!.slaCode
        )

        presenter.doEditCase(model)
    }

    private fun encodeImageToBase64(): String? {
        selectedScreenshot?.let { screenshot ->
            val outputStream = ByteArrayOutputStream()
            screenshot.compress(Bitmap.CompressFormat.JPEG, 75, outputStream)
            val byteArray = outputStream.toByteArray()
            return Base64.encodeToString(byteArray, Base64.DEFAULT)
        }

        return null
    }
    //endregion
}
