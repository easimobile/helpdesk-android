package com.easipos.helpdesk.activities.login.mvp

import com.easipos.helpdesk.base.View

interface LoginView : View {

    fun navigateToChangePassword()

    fun navigateToResetPassword()

    fun navigateToMain()
}
