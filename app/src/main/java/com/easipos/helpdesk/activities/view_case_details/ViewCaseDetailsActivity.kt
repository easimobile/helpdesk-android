package com.easipos.helpdesk.activities.view_case_details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.view_case_details.mvp.ViewCaseDetailsPresenter
import com.easipos.helpdesk.activities.view_case_details.mvp.ViewCaseDetailsView
import com.easipos.helpdesk.activities.view_case_details.navigation.ViewCaseDetailsNavigation
import com.easipos.helpdesk.activities.view_case_details.viewmodel.ViewCaseDetailsViewModel
import com.easipos.helpdesk.activities.view_case_details.viewmodel.ViewCaseDetailsViewModelFactory
import com.easipos.helpdesk.adapters.CaseDetailAdapter
import com.easipos.helpdesk.api.requests.cases.ChangeCaseDetailStatusRequestModel
import com.easipos.helpdesk.api.requests.cases.GetCaseDetailsRequestModel
import com.easipos.helpdesk.base.CustomLifecycleActivity
import com.easipos.helpdesk.bundle.ParcelData
import com.easipos.helpdesk.constant.CaseStatus
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.models.CaseDetail
import com.easipos.helpdesk.tools.EqualSpacingItemDecoration
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_view_case_details.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class ViewCaseDetailsActivity : CustomLifecycleActivity<ViewCaseDetailsViewModel>(), ViewCaseDetailsView,
    CaseDetailAdapter.OnGestureDetectedListener {

    companion object {
        fun newIntent(context: Context, caseId: String, isEditable: Boolean, createNewCaseDetail: Boolean): Intent {
            return Intent(context, ViewCaseDetailsActivity::class.java).apply {
                this.putExtra(ParcelData.CASE_ID, caseId)
                this.putExtra(ParcelData.IS_EDITABLE, isEditable)
                this.putExtra(ParcelData.CREATE_NEW_CASE_DETAIL, createNewCaseDetail)
            }
        }
    }

    //region Variables
    private val presenter by lazy { ViewCaseDetailsPresenter(application) }
    private val factory by lazy { ViewCaseDetailsViewModelFactory(application) }

    private val navigation by instance<ViewCaseDetailsNavigation>()

    private val caseId by argument<String>(ParcelData.CASE_ID)
    private val isEditable by argument(ParcelData.IS_EDITABLE, false)
    private val createNewCaseDetail by argument(ParcelData.CREATE_NEW_CASE_DETAIL, false)
    //endregion

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        (recycler_view_case_detail.adapter as? CaseDetailAdapter)?.saveStates(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        (recycler_view_case_detail.adapter as? CaseDetailAdapter)?.restoreStates(savedInstanceState)
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        getCaseDetails()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_view_case_details

    override fun setupViewModel(): ViewCaseDetailsViewModel {
        return ViewModelProvider(this, factory)
            .get(ViewCaseDetailsViewModel::class.java)
    }

    override fun setupViewModelObserver() {
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun refreshCaseDetails() {
        getCaseDetails()
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(item: CaseDetail) {
        caseId?.let { caseId ->
            navigation.navigateToEditCaseDetail(this, caseId, item)
        }
    }

    override fun onMarkAsClosed(item: CaseDetail) {
        if (item.serviceTypeCode.isBlank()) {
            showErrorAlertDialog(getString(R.string.error_service_type_required))
            return
        }

        val model = ChangeCaseDetailStatusRequestModel(
            caseDetailID = item.caseDetailId,
            status = CaseStatus.CLOSED.name
        )

        presenter.doChangeCaseDetailStatus(model)
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        caseId?.let { caseId ->
            text_view_title.text = getString(R.string.label_case_id_placeholder, caseId)
        }

        if (isEditable) {
            fab.show()
        } else {
            fab.hide()
        }

        if (createNewCaseDetail) {
            caseId?.let { caseId ->
                navigation.navigateToCreateCaseDetail(this, caseId)
            }
        }

        setupRecyclerView()
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        fab.click {
            caseId?.let { caseId ->
                navigation.navigateToCreateCaseDetail(this, caseId)
            }
        }
    }

    private fun getCaseDetails() {
        caseId?.let { caseId ->
            val model = GetCaseDetailsRequestModel(
                caseId = caseId
            )

            viewModel.getCaseDetails(model)
        }
    }

    private fun setupRecyclerView() {
        recycler_view_case_detail.apply {
            this.adapter = CaseDetailAdapter(this@ViewCaseDetailsActivity, this@ViewCaseDetailsActivity)
            this.layoutManager = LinearLayoutManager(context)
            this.addItemDecoration(EqualSpacingItemDecoration(resources.getDimensionPixelSize(R.dimen.sixteen_dp), EqualSpacingItemDecoration.VERTICAL))
        }
    }
    //endregion
}
