package com.easipos.helpdesk.activities.main.mvp

import com.easipos.helpdesk.base.View

interface MainView : View {

    fun navigateToSplash()
}
