package com.easipos.helpdesk.activities.view_case_details.mvp

import com.easipos.helpdesk.base.View

interface ViewCaseDetailsView : View {

    fun refreshCaseDetails()
}
