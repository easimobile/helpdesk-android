package com.easipos.helpdesk.activities.view_case_details.navigation

import android.app.Activity
import com.easipos.helpdesk.models.CaseDetail

interface ViewCaseDetailsNavigation {

    fun navigateToCreateCaseDetail(activity: Activity, caseId: String)

    fun navigateToEditCaseDetail(activity: Activity, caseId: String, caseDetail: CaseDetail)
}