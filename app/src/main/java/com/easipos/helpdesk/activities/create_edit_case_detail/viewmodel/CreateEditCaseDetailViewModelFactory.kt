package com.easipos.helpdesk.activities.create_edit_case_detail.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CreateEditCaseDetailViewModelFactory(private val application: Application)
    : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateEditCaseDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CreateEditCaseDetailViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}