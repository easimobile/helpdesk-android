package com.easipos.helpdesk.activities.change_password

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.change_password.mvp.ChangePasswordPresenter
import com.easipos.helpdesk.activities.change_password.mvp.ChangePasswordView
import com.easipos.helpdesk.activities.change_password.navigation.ChangePasswordNavigation
import com.easipos.helpdesk.api.requests.user.ChangePasswordRequestModel
import com.easipos.helpdesk.base.CustomBaseAppCompatActivity
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.managers.UserManager
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.visible
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.equalTo
import io.github.anderscheow.validator.rules.common.minimumLength
import io.github.anderscheow.validator.rules.common.notBlank
import kotlinx.android.synthetic.main.activity_change_password.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class ChangePasswordActivity : CustomBaseAppCompatActivity(), ChangePasswordView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ChangePasswordActivity::class.java)
        }
    }

    //region Variables
    private val presenter by lazy { ChangePasswordPresenter(application) }

    private val navigation by instance<ChangePasswordNavigation>()
    private val validator by instance<Validator>()
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_change_password

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        if (UserManager.token == null) {
            text_input_layout_user_id.visible()
        } else {
            text_input_layout_user_id.gone()
        }
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        button_save.click {
            attemptChangePassword()
        }
    }

    private fun attemptChangePassword() {
        val userIdValidation = text_input_layout_user_id.validate()
            .notBlank(R.string.error_field_required)
        val currentPasswordValidation = text_input_layout_current_password.validate()
            .notBlank(R.string.error_field_required)
        val newPasswordValidation = text_input_layout_new_password.validate()
            .notBlank(R.string.error_field_required)
            .minimumLength(6, R.string.error_password_min_length)
        val confirmNewPasswordValidation = text_input_layout_confirm_new_password.validate()
            .notBlank(R.string.error_field_required)
            .minimumLength(6, R.string.error_password_min_length)
            .equalTo(text_input_edit_text_new_password.text?.toString() ?: "", R.string.error_password_not_match)

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val model = if (UserManager.token == null) {
                    val userId = values[0]
                    val currentPassword = values[1]
                    val newPassword = values[2]
                    val confirmNewPassword = values[3]

                    ChangePasswordRequestModel(
                        apiKey = null,
                        userId = userId,
                        currentPassword = currentPassword,
                        newPassword = newPassword,
                        newPasswordConfirmation = confirmNewPassword
                    )
                } else {
                    val currentPassword = values[0]
                    val newPassword = values[1]
                    val confirmNewPassword = values[2]

                    ChangePasswordRequestModel(
                        currentPassword = currentPassword,
                        newPassword = newPassword,
                        newPasswordConfirmation = confirmNewPassword
                    )
                }

                presenter.doChangePassword(model)
            }
        }).apply {
            if (UserManager.token == null) {
                this.validate(userIdValidation, currentPasswordValidation, newPasswordValidation, confirmNewPasswordValidation)
            } else {
                this.validate(currentPasswordValidation, newPasswordValidation, confirmNewPasswordValidation)
            }
        }
    }
    //endregion
}
