package com.easipos.helpdesk.activities.main.mvp

import android.app.Application
import com.easipos.helpdesk.Easi
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.managers.FcmManager
import io.github.anderscheow.library.kotlinExt.delay
import org.kodein.di.generic.instance

class MainPresenter(application: Application)
    : Presenter<MainView>(application) {

    private val fcmManager by kodein.instance<FcmManager>()

    fun doLogout() {
        fcmManager.service.removeFcmToken()

        delay(1000) {
            (application as? Easi)?.logout()
            view?.navigateToSplash()
        }
    }
}
