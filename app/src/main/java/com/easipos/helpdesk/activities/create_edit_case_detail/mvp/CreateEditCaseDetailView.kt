package com.easipos.helpdesk.activities.create_edit_case_detail.mvp

import com.easipos.helpdesk.base.View
import com.easipos.helpdesk.models.ServiceType

interface CreateEditCaseDetailView : View {

    fun showServiceTypeSelections(serviceTypes: List<ServiceType>)

    fun finishScreen()
}
