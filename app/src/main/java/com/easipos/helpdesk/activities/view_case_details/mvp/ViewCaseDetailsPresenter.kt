package com.easipos.helpdesk.activities.view_case_details.mvp

import android.app.Application
import com.easipos.helpdesk.api.requests.cases.ChangeCaseDetailStatusRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class ViewCaseDetailsPresenter(application: Application)
    : Presenter<ViewCaseDetailsView>(application) {

    private val caseRepository by instance<CaseRepository>()

    fun doChangeCaseDetailStatus(model: ChangeCaseDetailStatusRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.changeCaseDetailStatus(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        view?.setLoadingIndicator(false)
                        view?.refreshCaseDetails()
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
