package com.easipos.helpdesk.activities.main.navigation

import android.app.Activity
import com.easipos.helpdesk.models.CaseDuplication

interface MainNavigation {

    fun navigateToSplash(activity: Activity)

    fun navigateToCreateCase(activity: Activity)

    fun navigateToCreateCase(activity: Activity, caseDuplication: CaseDuplication)

    fun navigateToEditCase(activity: Activity, caseId: String)

    fun navigateToChangePassword(activity: Activity)

    fun navigateToViewCaseDetails(activity: Activity, caseId: String, isEditable: Boolean)
}