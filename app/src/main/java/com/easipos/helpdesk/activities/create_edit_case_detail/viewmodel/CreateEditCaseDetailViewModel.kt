package com.easipos.helpdesk.activities.create_edit_case_detail.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import com.easipos.helpdesk.base.CustomBaseAndroidViewModel
import com.easipos.helpdesk.models.CaseDetail

class CreateEditCaseDetailViewModel(application: Application) : CustomBaseAndroidViewModel<CaseDetail>(application) {

    //region Observable Field
    val caseDetail = ObservableField<CaseDetail?>()
    //endregion

    //region LiveData
    //endregion

    override fun start(args: CaseDetail?) {
        setCaseDetail(args)
    }

    private fun setCaseDetail(caseDetail: CaseDetail?) {
        this.caseDetail.set(caseDetail)
        this.caseDetail.notifyChange()
    }
}