package com.easipos.helpdesk.activities.create_edit_case.navigation

import android.app.Activity

interface CreateEditCaseNavigation {

    fun navigateToViewCaseDetails(activity: Activity, caseId: String, isEditable: Boolean)

    fun navigateToEditCase(activity: Activity, caseId: String)
}