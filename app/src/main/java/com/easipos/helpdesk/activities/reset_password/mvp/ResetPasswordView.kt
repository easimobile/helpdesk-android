package com.easipos.helpdesk.activities.reset_password.mvp

import com.easipos.helpdesk.base.View

interface ResetPasswordView : View {

    fun finishScreen()
}
