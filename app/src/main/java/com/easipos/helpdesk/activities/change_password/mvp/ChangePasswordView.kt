package com.easipos.helpdesk.activities.change_password.mvp

import com.easipos.helpdesk.base.View

interface ChangePasswordView : View {

    fun finishScreen()
}
