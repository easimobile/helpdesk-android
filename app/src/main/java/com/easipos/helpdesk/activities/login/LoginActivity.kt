package com.easipos.helpdesk.activities.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.login.mvp.LoginPresenter
import com.easipos.helpdesk.activities.login.mvp.LoginView
import com.easipos.helpdesk.activities.login.navigation.LoginNavigation
import com.easipos.helpdesk.api.requests.auth.LoginRequestModel
import com.easipos.helpdesk.base.CustomBaseAppCompatActivity
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.notBlank
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class LoginActivity : CustomBaseAppCompatActivity(), LoginView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<LoginNavigation>()
    private val validator by instance<Validator>()

    private val presenter by lazy { LoginPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_login

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }

    override fun navigateToResetPassword() {
        navigation.navigateToResetPassword(this)
    }

    override fun navigateToChangePassword() {
        navigation.navigateToChangePassword(this)
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
    }

    private fun setupListeners() {
        text_view_forgot_password.click {
            navigateToResetPassword()
        }

        button_login.click {
            attemptLogin()
        }
    }

    private fun attemptLogin() {
        val userIdValidation = text_input_layout_user_id.validate()
            .notBlank(R.string.error_field_required)
        val passwordValidation = text_input_layout_password.validate()
            .notBlank(R.string.error_field_required)

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val userId = values[0]
                val password = values[1]

                val model = LoginRequestModel(
                    id = userId,
                    password = password
                )

                presenter.doLogin(model)
            }
        }).validate(userIdValidation, passwordValidation)
    }
    //endregion
}
