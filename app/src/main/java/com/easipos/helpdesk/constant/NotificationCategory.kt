package com.easipos.helpdesk.constant

import java.util.*

enum class NotificationCategory {
    CASE_ASSIGNED_TO,
    CASE_EXCEED_SLA_RESPONSE_TIME,
    CASE_EXCEED_SLA_RESOLUTION_TIME,
    CASE_CREATED_BY_CUSTOMER;

    companion object {
        fun parse(value: String): NotificationCategory? {
            return when (value.toUpperCase(Locale.getDefault())) {
                "CASE_ASSIGNED_TO" -> CASE_ASSIGNED_TO
                "CASE_EXCEED_SLA_RESPONSE_TIME" -> CASE_EXCEED_SLA_RESPONSE_TIME
                "CASE_EXCEED_SLA_RESOLUTION_TIME" -> CASE_EXCEED_SLA_RESOLUTION_TIME
                "CASE_CREATED_BY_CUSTOMER" -> CASE_CREATED_BY_CUSTOMER
                else -> null
            }
        }
    }
}