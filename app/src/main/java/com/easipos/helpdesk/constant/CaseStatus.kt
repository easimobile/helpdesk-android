package com.easipos.helpdesk.constant

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
enum class CaseStatus(val value: String) : Parcelable {
    OPEN("Open"),
    IN_PROGRESS("In Progress"),
    PENDING("Pending"),
    RESOLVED("Resolved"),
    CLOSED("Closed"),
    CLOSED_WITH_EXCEPTION("Closed with Exception"),
    DUPLICATE("Duplicate");

    companion object {
        fun parse(value: String): CaseStatus? {
            return when (value.toLowerCase(Locale.getDefault())) {
                "open" -> OPEN
                "in_progress" -> IN_PROGRESS
                "pending" -> PENDING
                "resolved" -> RESOLVED
                "closed" -> CLOSED
                "closed_with_exception" -> CLOSED_WITH_EXCEPTION
                "duplicate" -> DUPLICATE
                else -> null
            }
        }

        fun toList(allowPost: Boolean): List<CaseStatus> {
            return arrayListOf<CaseStatus>().apply {
                this.add(OPEN)
                this.add(IN_PROGRESS)
                this.add(PENDING)
                this.add(RESOLVED)

                if (allowPost) {
                    this.add(CLOSED)
                    this.add(CLOSED_WITH_EXCEPTION)
                }

                this.add(DUPLICATE)
            }
        }
    }

    override fun toString(): String {
        return this.value
    }
}