package com.easipos.helpdesk.tools

object ModulePermissionParser {

    private const val PROGRAM_NAME_CASES = "Cases"

    fun isCasesAllowPost(): Boolean {
        val permission =  Preference.prefModulePermission.find { it.programName == PROGRAM_NAME_CASES }
        return permission != null && permission.allowPost
    }
}