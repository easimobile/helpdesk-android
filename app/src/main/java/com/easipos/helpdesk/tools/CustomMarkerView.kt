package com.easipos.helpdesk.tools

import android.content.Context
import android.widget.TextView
import com.easipos.helpdesk.R
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF

class CustomMarkerView(context: Context) :
    MarkerView(context, R.layout.view_custom_marker) {

    override fun refreshContent(e: Entry, highlight: Highlight) {
        val entry = e as PieEntry
        findViewById<TextView>(R.id.text_view_marker).text = context.getString(
            R.string.label_marker_placeholder,
            entry.label,
            entry.y.toInt()
        )
        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }
}