package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.OperationEmployeeResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.OperationEmployee

class OperationEmployeeMapper : Mapper<OperationEmployee, OperationEmployeeResponseModel>() {

    override fun transform(item: OperationEmployeeResponseModel): OperationEmployee {
        return OperationEmployee(
            employeeCode = item.employeeCode ?: "",
            employeeName = item.employeeName ?: ""
        )
    }
}