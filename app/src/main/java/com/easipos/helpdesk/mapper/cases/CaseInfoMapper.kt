package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.CaseInfoResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.CaseInfo

class CaseInfoMapper : Mapper<CaseInfo, CaseInfoResponseModel>() {

    override fun transform(item: CaseInfoResponseModel): CaseInfo {
        return CaseInfo(
            caseId = item.caseId ?: "",
            projectCode = item.projectCode ?: "",
            projectName = item.projectName ?: "",
            createdBy = item.createdBy ?: "",
            createdByName = item.createdByName ?: "",
            assignedTo = item.assignedTo ?: "",
            assignedToName = item.assignedToName ?: "",
            outletCode = item.outletCode ?: "",
            outletName = item.outletName ?: "",
            address1 = item.address1 ?: "",
            address2 = item.address2 ?: "",
            address3 = item.address3 ?: "",
            address4 = item.address4 ?: "",
            address5 = item.address5 ?: "",
            postalCode = item.postalCode ?: "",
            posNo = item.posNo ?: "",
            posDesc = item.posDesc ?: "",
            ipAddress = item.ipAddress ?: "",
            priority = item.priority ?: -1,
            createdDate = item.createdDate ?: "",
            createdTime = item.createdTime ?: "",
            contactPerson = item.contactPerson ?: "",
            contactNo = item.contactNo ?: "",
            lastUpdatedDate = item.lastUpdatedDate ?: "",
            lastUpdatedTime = item.lastUpdatedTime ?: "",
            remarks = item.remarks ?: "",
            status = item.status ?: "",
            caseTypeCode = item.caseTypeCode ?: "",
            caseTypeDesc = item.caseTypeDesc ?: "",
            caseSubTypeCode1 = item.caseSubTypeCode1,
            caseSubTypeDesc1 = item.caseSubTypeDesc1,
            caseSubTypeCode2 = item.caseSubTypeCode2,
            caseSubTypeDesc2 = item.caseSubTypeDesc2,
            caseSubTypeCode3 = item.caseSubTypeCode3,
            caseSubTypeDesc3 = item.caseSubTypeDesc3,
            screenshotInUrl = item.screenshotInUrl ?: "",
            hasCaseSubType1 = item.hasCaseSubType1 == 1,
            hasCaseSubType2 = item.hasCaseSubType2 == 1,
            hasCaseSubType3 = item.hasCaseSubType3 == 1,
            allowPost = item.allowPost == "1",
            editable = item.editable == 1,
            originalCaseId = item.originalCaseId,
            slaCode = item.slaCode ?: "",
            responseTime = item.responseTime ?: "",
            resolutionTime = item.resolutionTime ?: "",
            pendingDays = item.pendingDays ?: 0
        )
    }
}