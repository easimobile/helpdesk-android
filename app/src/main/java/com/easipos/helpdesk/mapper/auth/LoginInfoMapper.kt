package com.easipos.helpdesk.mapper.auth

import com.easipos.helpdesk.api.responses.auth.LoginInfoResponseModel
import com.easipos.helpdesk.api.responses.auth.ModulePermissionResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.LoginInfo
import com.easipos.helpdesk.models.ModulePermission

class LoginInfoMapper : Mapper<LoginInfo, LoginInfoResponseModel>() {

    private val modulePermissionMapper by lazy { ModulePermissionMapper() }

    override fun transform(item: LoginInfoResponseModel): LoginInfo {
        return LoginInfo(
            apiKey = item.apiKey ?: "",
            modules = if (item.modules == null) emptyList() else modulePermissionMapper.transform(item.modules)
        )
    }
}

private class ModulePermissionMapper : Mapper<ModulePermission, ModulePermissionResponseModel>() {

    override fun transform(item: ModulePermissionResponseModel): ModulePermission {
        return ModulePermission(
            programName = item.programName ?: "",
            allowPost = item.allowPost == "1"
        )
    }
}