package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.CustomerOutletResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.CustomerOutlet

class CustomerOutletMapper : Mapper<CustomerOutlet, CustomerOutletResponseModel>() {

    override fun transform(item: CustomerOutletResponseModel): CustomerOutlet {
        return CustomerOutlet(
            outletCode = item.outletCode ?: "",
            outletName = item.outletName ?: "",
            address1 = item.address1 ?: "",
            address2 = item.address2 ?: "",
            address3 = item.address3 ?: "",
            address4 = item.address4 ?: "",
            address5 = item.address5 ?: "",
            postalCode = item.postalCode ?: "",
            posCount = item.posCount ?: 0
        )
    }
}