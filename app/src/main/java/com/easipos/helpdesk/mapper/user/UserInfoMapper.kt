package com.easipos.helpdesk.mapper.user

import com.easipos.helpdesk.api.responses.user.UserInfoResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.UserInfo

class UserInfoMapper : Mapper<UserInfo, UserInfoResponseModel>() {

    override fun transform(item: UserInfoResponseModel): UserInfo {
        return UserInfo(
            userCode = item.userCode ?: "",
            name = item.name ?: "",
            email = item.email ?: "",
            userProfile = item.userProfile ?: ""
        )
    }
}