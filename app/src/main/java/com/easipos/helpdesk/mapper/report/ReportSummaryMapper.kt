package com.easipos.helpdesk.mapper.report

import com.easipos.helpdesk.api.responses.report.ReportSummaryResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.ReportSummary

class ReportSummaryMapper : Mapper<ReportSummary, ReportSummaryResponseModel>() {

    override fun transform(item: ReportSummaryResponseModel): ReportSummary {
        return ReportSummary(
            total = item.total?.toIntOrNull() ?: 0,
            open = item.open?.toIntOrNull() ?: 0,
            pending = item.pending?.toIntOrNull() ?: 0,
            inProgress = item.inProgress?.toIntOrNull() ?: 0,
            resolved = item.resolved?.toIntOrNull() ?: 0,
            closed = item.closed?.toIntOrNull() ?: 0
        )
    }
}