package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.CaseResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.Case

class CaseMapper : Mapper<Case, CaseResponseModel>() {

    override fun transform(item: CaseResponseModel): Case {
        return Case(
            caseId = item.caseId ?: "",
            projectName = item.projectName ?: "",
            assignedToName = item.assignedToName ?: "",
            priority = item.priority ?: -1,
            createdDate = item.createdDate ?: "",
            createdTime = item.createdTime ?: "",
            remarks = item.remarks ?: "",
            status = item.status ?: "",
            editable = item.editable == 1,
            isRated = item.isRated == "1",
            numOfRating = item.numOfRating?.toIntOrNull() ?: 1,
        )
    }
}