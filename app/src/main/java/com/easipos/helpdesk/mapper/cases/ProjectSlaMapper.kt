package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.ProjectSlaResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.ProjectSla

class ProjectSlaMapper : Mapper<ProjectSla, ProjectSlaResponseModel>() {

    override fun transform(item: ProjectSlaResponseModel): ProjectSla {
        return ProjectSla(
            slaCode = item.slaCode ?: "",
            priority = item.priority ?: 0,
            responseTime = item.responseTime ?: "",
            resolutionTime = item.resolutionTime ?: ""
        )
    }
}