package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.ProjectResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.Project

class ProjectMapper : Mapper<Project, ProjectResponseModel>() {

    override fun transform(item: ProjectResponseModel): Project {
        return Project(
            projectCode = item.projectCode ?: "",
            projectName = item.projectName ?: "",
            outletCount = item.outletCount ?: 0,
            slaCount = item.slaCount ?: 0
        )
    }
}

