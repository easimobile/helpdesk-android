package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.CaseDetailResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.CaseDetail

class CaseDetailMapper : Mapper<CaseDetail, CaseDetailResponseModel>() {

    override fun transform(item: CaseDetailResponseModel): CaseDetail {
        return CaseDetail(
            caseDetailId = item.caseDetailId ?: "",
            notes = item.notes ?: "",
            createdDate = item.createdDate ?: "",
            createdTime = item.createdTime ?: "",
            createdBy = item.createdBy ?: "",
            createdByName = item.createdByName ?: "",
            status = item.status ?: "",
            serviceTypeCode = item.serviceTypeCode ?: "",
            serviceTypeDesc = item.serviceTypeDesc ?: "",
            serviceReportNo = item.serviceReportNo ?: "",
            screenshotInUrl = item.screenshotInUrl ?: "",
            isUserNegligence = item.isUserNegligence == "1",
            sequenceNo = item.sequenceNo,
            allowPost = item.allowPost == "1",
            editable = item.editable == 1
        )
    }
}