package com.easipos.helpdesk.mapper

abstract class Mapper<Out, In> {

    abstract fun transform(item: In): Out

    fun transform(collection: Collection<In>): List<Out> {
        return collection.map { transform(it) }
    }
}
