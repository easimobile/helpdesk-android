package com.easipos.helpdesk.mapper.cases

import com.easipos.helpdesk.api.responses.cases.CaseRatingResponseModel
import com.easipos.helpdesk.mapper.Mapper
import com.easipos.helpdesk.models.CaseRating

class CaseRatingMapper : Mapper<CaseRating, CaseRatingResponseModel>() {

    override fun transform(item: CaseRatingResponseModel): CaseRating {
        return CaseRating(
            caseId = item.caseId ?: "",
            rate = item.rate?.toIntOrNull() ?: 1,
            remark = item.remark ?: "",
            rateDateTime = item.rateDateTime ?: ""
        )
    }
}