package com.easipos.helpdesk.fragments.dialog_fragment

import android.os.Bundle
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.easipos.helpdesk.R
import com.easipos.helpdesk.adapters.SelectionAdapter
import com.easipos.helpdesk.api.requests.cases.ChangeCaseAssigneeRequestModel
import com.easipos.helpdesk.base.CustomBaseDialogFragment
import com.easipos.helpdesk.bundle.ParcelData
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.OperationEmployee
import com.easipos.helpdesk.models.Selectable
import com.easipos.helpdesk.util.showBottomSheet
import com.google.android.material.bottomsheet.BottomSheetDialog
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.withActivity
import kotlinx.android.synthetic.main.dialog_fragment_change_assignee.*
import org.jetbrains.anko.displayMetrics

class ChangeAssigneeDialogFragment : CustomBaseDialogFragment(), SelectionAdapter.OnGestureDetectedListener {

    companion object {
        fun newInstance(case: Case, operationEmployees: List<OperationEmployee>): ChangeAssigneeDialogFragment {
            return ChangeAssigneeDialogFragment().apply {
                this.arguments = Bundle().apply {
                    this.putParcelable(ParcelData.CASE, case)
                    this.putSerializable(ParcelData.OPERATION_EMPLOYEES, ArrayList(operationEmployees))
                }
            }
        }
    }

    interface OnChangeAssigneeListener {
        fun onChangeAssignee(dialogFragment: DialogFragment, model: ChangeCaseAssigneeRequestModel)
    }

    private val case by argument<Case>(ParcelData.CASE)
    private val operationEmployees by argument<ArrayList<OperationEmployee>>(ParcelData.OPERATION_EMPLOYEES, arrayListOf())

    private var selectedAssignee: OperationEmployee? = null

    private var selectionDialog: BottomSheetDialog? = null
    private lateinit var listener: OnChangeAssigneeListener

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_change_assignee

    override fun init() {
        super.init()

        case?.let { case ->
            if (case.assignedToName.isNotBlank()) {
                text_view_assign_to.text = case.assignedToName
            }
        }

        card_view_assign_to.click {
            showOperationEmployeeSelections(operationEmployees)
        }

        button_cancel.click {
            dismissAllowingStateLoss()
        }

        button_change.click {
            changeAssignee()
        }
    }

    override fun onSelectItem(position: Int, data: Any) {
        selectionDialog?.dismiss()

        if (data is OperationEmployee) {
            selectedAssignee = data

            text_view_assign_to.text = data.employeeName
        }
    }

    override fun onReturnItems(items: List<Any>) {
    }

    fun setListener(listener: OnChangeAssigneeListener) {
        this.listener = listener
    }

    private fun showOperationEmployeeSelections(operationEmployees: List<OperationEmployee>) {
        val selectableList = operationEmployees.map { Selectable(it as Any) }
        selectedAssignee?.let { operationEmployee ->
            val selectable = selectableList.find {
                (it.data as OperationEmployee).employeeCode == operationEmployee.employeeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_assign_to), selectableList)
    }

    private fun showBottomSheetSelection(title: String, data: List<Selectable<Any>>) {
        withActivity { activity ->
            selectionDialog?.dismiss()
            selectionDialog = showBottomSheet(activity, this, title, data)
            selectionDialog?.show()
        }
    }

    private fun changeAssignee() {
        case?.let { case ->
            selectedAssignee?.let { assignee ->
                val model = ChangeCaseAssigneeRequestModel(
                    caseId = case.caseId,
                    assignedTo = assignee.employeeCode
                )

                listener.onChangeAssignee(this, model)
            } ?: run {
                dismissAllowingStateLoss()
            }
        }
    }
}