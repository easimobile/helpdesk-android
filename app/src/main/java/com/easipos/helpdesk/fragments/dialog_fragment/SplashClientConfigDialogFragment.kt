package com.easipos.helpdesk.fragments.dialog_fragment

import android.view.ViewGroup
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.splash.SplashActivity
import com.easipos.helpdesk.base.CustomBaseDialogFragment
import com.easipos.helpdesk.tools.Preference
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.withActivity
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.dialog_fragment_splash_client_config.*
import org.jetbrains.anko.displayMetrics
import org.kodein.di.generic.instance

class SplashClientConfigDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun newInstance(): SplashClientConfigDialogFragment {
            return SplashClientConfigDialogFragment()
        }
    }

    private val validator by instance<Validator>()

    override fun onStart() {
        super.onStart()
        withActivity { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.9).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_splash_client_config

    override fun init() {
        super.init()
        isCancelable = false

        button_ok.click {
            attemptSaveConfig()
        }
    }

    private fun attemptSaveConfig() {
        val clientNameValidation = text_input_layout_client_name.validate()
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val clientName = values[0].trim()

                Preference.prefClientName = clientName

                (activity as? SplashActivity)?.performDuringSplash()
                dismissAllowingStateLoss()
            }
        }).validate(clientNameValidation)
    }
}