package com.easipos.helpdesk.fragments.rating.viewmodel

import android.app.Application
import com.easipos.helpdesk.base.CustomPagingWithoutLocalAndroidViewModel
import com.easipos.helpdesk.fragments.rating.paging.CaseRatingDataSourceFactory
import com.easipos.helpdesk.models.CaseRating
import com.easipos.helpdesk.repositories.cases.CaseRepository
import io.github.anderscheow.library.paging.remote.BaseDataSourceFactory
import org.kodein.di.generic.instance
import java.util.concurrent.Executor

class CaseRatingViewModel(application: Application) :
    CustomPagingWithoutLocalAndroidViewModel<Void, String, CaseRating>(application) {

    //region Observable Field
    //endregion

    //region LiveData
    //endregion

    private val caseRepository by instance<CaseRepository>()

    //region Abstract methods
    override val numberOfThreads: Int
        get() = 5

    override val loadPageSize: Int
        get() = 20

    override fun getDataSourceFactory(executor: Executor): BaseDataSourceFactory<String, CaseRating> {
        return CaseRatingDataSourceFactory(
            executor,
            caseRepository
        )
    }
    //endregion

    fun start() {
        setIsLoading(true)
        init()
    }
}