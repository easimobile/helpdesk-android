package com.easipos.helpdesk.fragments.cases.paging

import com.easipos.helpdesk.api.requests.cases.GetCasesRequestModel
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.repositories.cases.CaseRepository
import io.github.anderscheow.library.paging.remote.BaseDataSourceFactory
import io.github.anderscheow.library.paging.remote.BaseItemKeyedDataSource
import java.util.concurrent.Executor

class CaseDataSourceFactory internal constructor(executor: Executor,
                                                 private val model: GetCasesRequestModel,
                                                 private val caseRepository: CaseRepository)
    : BaseDataSourceFactory<String, Case>(executor) {

    override fun getItemKeyedDataSource(executor: Executor): BaseItemKeyedDataSource<String, Case> {
        return CaseItemKeyedDataSource(
            executor,
            model,
            caseRepository
        )
    }
}
