package com.easipos.helpdesk.fragments.cases

import android.content.Context
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.helpdesk.R
import com.easipos.helpdesk.adapters.CaseAdapter
import com.easipos.helpdesk.adapters.SelectionAdapter
import com.easipos.helpdesk.api.requests.cases.ChangeCaseAssigneeRequestModel
import com.easipos.helpdesk.api.requests.cases.GetCasesRequestModel
import com.easipos.helpdesk.base.CustomLifecycleFragment
import com.easipos.helpdesk.constant.CasePriority
import com.easipos.helpdesk.constant.CaseStatus
import com.easipos.helpdesk.event_bus.RefreshCaseListing
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.fragments.cases.mvp.CasePresenter
import com.easipos.helpdesk.fragments.cases.mvp.CaseView
import com.easipos.helpdesk.fragments.cases.navigation.CaseNavigation
import com.easipos.helpdesk.fragments.cases.viewmodel.CaseViewModel
import com.easipos.helpdesk.fragments.cases.viewmodel.CaseViewModelFactory
import com.easipos.helpdesk.fragments.dialog_fragment.ChangeAssigneeDialogFragment
import com.easipos.helpdesk.models.*
import com.easipos.helpdesk.tools.EqualSpacingItemDecoration
import com.easipos.helpdesk.util.showBottomSheet
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import io.github.anderscheow.library.constant.EventBusType
import io.github.anderscheow.library.kotlinExt.*
import io.github.anderscheow.library.viewModel.observeItems
import io.github.anderscheow.library.viewModel.observeNetworkState
import io.github.anderscheow.library.viewModel.observeTotalItems
import kotlinx.android.synthetic.main.fragment_case.*
import kotlinx.android.synthetic.main.nav_view_filter.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.kodein.di.generic.instance
import java.util.*

class CaseFragment : CustomLifecycleFragment<CaseViewModel>(), CaseView,
    CaseAdapter.OnGestureDetectedListener, SelectionAdapter.OnGestureDetectedListener,
    ChangeAssigneeDialogFragment.OnChangeAssigneeListener {

    companion object {
        fun newInstance(): CaseFragment {
            return CaseFragment()
        }
    }

    //region Variables
    private val navigation by instance<CaseNavigation>()

    private val presenter by lazy { CasePresenter(requireActivity().application) }
    private val factory by lazy { CaseViewModelFactory(requireActivity().application) }

    private var selectedDateFrom: Calendar? = null
    private var selectedDateTo: Calendar? = null
    private var selectedStatusList = arrayListOf<CaseStatus>()
    private var selectedPriorityList = arrayListOf<CasePriority>()
    private var selectedProjectList = arrayListOf<Project>()
    private var selectedAssigneeList = arrayListOf<OperationEmployee>()

    private var selectionDialog: BottomSheetDialog? = null

    private val itemDecoration by lazy { EqualSpacingItemDecoration(resources.getDimensionPixelSize(R.dimen.sixteen_dp), EqualSpacingItemDecoration.VERTICAL) }
    //endregion

    //region Lifecycle
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            init()
        } else {
            (recycler_view_case.adapter as? CaseAdapter)?.restoreStates(savedInstanceState)
        }
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        (recycler_view_case.adapter as? CaseAdapter)?.saveStates(outState)
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        if (childFragment is ChangeAssigneeDialogFragment) {
            childFragment.setListener(this)
        }
    }

//    override fun onResume() {
//        super.onResume()
//        getCases()
//    }
    //endregion

    //region CustomLifecycleFragment Abstract Methods
    override fun getResLayout(): Int = R.layout.fragment_case

    override fun getEventBusType(): EventBusType? = EventBusType.ON_ATTACH

    override fun setupViewModel(): CaseViewModel {
        return ViewModelProvider(this, factory)
            .get(CaseViewModel::class.java)
    }

    override fun setupViewModelObserver(lifecycleOwner: LifecycleOwner) {
    }

    override fun init() {
        super.init()
        presenter.onAttachView(this)

        setupViews()
        setupListeners()

        getCases()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        withContext { context ->
            CommonAlertDialog(context, message, action).show()
        }
    }

    override fun showProjectSelections(projects: List<Project>) {
        val selectableList = projects.map { Selectable(it as Any) }
        selectedProjectList.forEach { selectedProject ->
            val selectable = selectableList.find {
                (it.data as Project).projectCode == selectedProject.projectCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_project), selectableList)
    }

    override fun showOperationEmployeeSelections(operationEmployees: List<OperationEmployee>) {
        val selectableList = operationEmployees.map { Selectable(it as Any) }
        selectedAssigneeList.forEach { selectedAssignee ->
            val selectable = selectableList.find {
                (it.data as OperationEmployee).employeeCode == selectedAssignee.employeeCode
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_assign_to), selectableList)
    }

    override fun showChangeAssignee(case: Case, operationEmployees: List<OperationEmployee>) {
        val fragment = ChangeAssigneeDialogFragment.newInstance(case, operationEmployees)
        fragment.show(childFragmentManager, ChangeAssigneeDialogFragment.TAG)
    }
    //endregion

    //region Interface methods
    override fun onSelectItem(item: Case) {
        withActivity { activity ->
            navigation.navigateToEditCase(activity, item.caseId)
        }
    }

    override fun onSelectAddResponse(item: Case) {
        withActivity { activity ->
            navigation.navigateToViewCaseDetails(activity, item)
        }
    }

    override fun onSelectChangeAssignee(item: Case) {
        presenter.doGetOperationEmployees(item)
    }

    override fun onDuplicateCase(item: Case) {
        withActivity { activity ->
            navigation.navigateToCreateCase(activity, CaseDuplication(
                caseId = item.caseId,
                duplicationType = DuplicationType.DUPLICATE
            ))
        }
    }

    override fun onCopyCase(item: Case) {
        withActivity { activity ->
            navigation.navigateToCreateCase(activity, CaseDuplication(
                caseId = item.caseId,
                duplicationType = DuplicationType.COPY
            ))
        }
    }

    override fun onSelectItem(position: Int, data: Any) {
    }

    override fun onReturnItems(items: List<Any>) {
        if (items.isNotEmpty()) {
            when (items[0]) {
                is Project -> {
                    selectedProjectList.clear()
                    items.forEach { selectedProjectList.add(it as Project) }

                    text_view_project_filter.text = selectedProjectList.joinToString("\n") { it.projectName }

                    image_view_close_project.visible()
                }

                is CasePriority -> {
                    selectedPriorityList.clear()
                    items.forEach { selectedPriorityList.add(it as CasePriority) }

                    text_view_priority_filter.text = selectedPriorityList.joinToString("\n") { it.value }

                    image_view_close_priority.visible()
                }

                is CaseStatus -> {
                    selectedStatusList.clear()
                    items.forEach { selectedStatusList.add(it as CaseStatus) }

                    text_view_status_filter.text = selectedStatusList.joinToString("\n") { it.value }

                    image_view_close_status.visible()
                }

                is OperationEmployee -> {
                    selectedAssigneeList.clear()
                    items.forEach { selectedAssigneeList.add(it as OperationEmployee) }

                    text_view_assign_to_filter.text = selectedAssigneeList.joinToString("\n") { it.employeeName }

                    image_view_close_assign_to.visible()
                }
            }
        }
    }

    override fun onChangeAssignee(dialogFragment: DialogFragment, model: ChangeCaseAssigneeRequestModel) {
        presenter.doChangeAssignee(dialogFragment, model)
    }
    //endregion

    //region Action methods
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: Any) {
        if (event is RefreshCaseListing) {
            viewModel.onRefresh()
        }
    }

    fun getCases() {
        isAdded {
            if (selectedDateFrom != null && selectedDateTo == null) {
                showErrorAlertDialog(getString(R.string.error_date_to_required))
                return@isAdded
            }

            if (selectedDateTo != null && selectedDateFrom == null) {
                showErrorAlertDialog(getString(R.string.error_date_from_required))
                return@isAdded
            }

            val model = GetCasesRequestModel(
                dateFrom = if (selectedDateFrom == null) null else selectedDateFrom?.timeInMillis?.formatDate(
                    "dd/MM/yyyy"
                ),
                dateTo = if (selectedDateTo == null) null else selectedDateTo?.timeInMillis?.formatDate(
                    "dd/MM/yyyy"
                ),
                status = selectedStatusList.map { it.name },
                priority = selectedPriorityList.map { it.toInt() },
                project = selectedProjectList.map { it.projectCode },
                assignedTo = selectedAssigneeList.map { it.employeeCode },
                projectName = null,
                outletName = text_input_edit_text_outlet.text?.toString()
            )

            viewModel.start(model)
            setupAdapter()

            closeDrawer()

        }
    }

    private fun setupViews() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    private fun setupListeners() {
        text_view_filter.click {
            drawer_layout.openDrawer(GravityCompat.END)
        }

        card_view_date_from.click {
            showDateSelector(0)
        }

        card_view_date_to.click {
            showDateSelector(1)
        }

        card_view_status.click {
            showStatusSelections()
        }

        card_view_priority.click {
            showPrioritySelections()
        }

        card_view_project.click {
            presenter.doGetProjects()
        }

        card_view_assign_to.click {
            presenter.doGetOperationEmployees()
        }

        image_view_close_date_range.click {
            resetDateRangeFilter()
        }

        image_view_close_status.click {
            resetStatusFilter()
        }

        image_view_close_priority.click {
            resetPriorityFilter()
        }

        image_view_close_project.click {
            resetProjectFilter()
        }

        image_view_close_assign_to.click {
            resetAssignToFilter()
        }

        button_reset.click {
            resetAllFilters()
        }

        button_apply.click {
            getCases()
        }

        button_search.click {
            text_input_edit_text_search_case_id.text?.toString()?.takeIf { it.isNotBlank() }?.let { caseId ->
                withActivity { activity ->
                    navigation.navigateToEditCase(activity, caseId)
                }
                closeDrawer()
            }
        }
    }

    private fun setupAdapter() {
        withContext { context ->
            val adapter = CaseAdapter(this) { viewModel.retry() }

            setupRecyclerView(context, adapter)
            setupViewModel(adapter)
        }
    }

    private fun setupRecyclerView(context: Context, adapter: CaseAdapter) {
        recycler_view_case.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
            this.removeItemDecoration(itemDecoration)
            this.addItemDecoration(itemDecoration)
        }
    }

    private fun setupViewModel(adapter: CaseAdapter) {
        with(viewModel) {
            this.observeItems(this@CaseFragment, adapter)
            this.observeNetworkState(this@CaseFragment, adapter, true)
            this.observeTotalItems(this@CaseFragment)
        }
    }

    private fun closeDrawer() {
        if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END)
        }
    }

    private fun resetAllFilters() {
        selectedDateFrom = null
        selectedDateTo = null
        selectedStatusList.clear()
        selectedPriorityList.clear()
        selectedProjectList.clear()
        selectedAssigneeList.clear()

        text_input_edit_text_search_case_id.text = null
        text_view_date_from_filter.setText(R.string.label_from)
        text_view_date_to_filter.setText(R.string.label_to)
        text_view_status_filter.text = null
        text_view_priority_filter.text = null
        text_view_project_filter.text = null
        text_view_project_filter.text = null
        text_view_assign_to_filter.text = null
        text_input_edit_text_outlet.text = null

        image_view_close_date_range.invisible()
        image_view_close_status.invisible()
        image_view_close_priority.invisible()
        image_view_close_project.invisible()
        image_view_close_assign_to.invisible()

        getCases()
    }

    private fun resetDateRangeFilter() {
        selectedDateFrom = null
        selectedDateTo = null

        text_view_date_from_filter.setText(R.string.label_from)
        text_view_date_to_filter.setText(R.string.label_to)

        image_view_close_date_range.invisible()
    }

    private fun resetStatusFilter() {
        selectedStatusList.clear()

        text_view_status_filter.text = null

        image_view_close_status.invisible()
    }

    private fun resetPriorityFilter() {
        selectedPriorityList.clear()

        text_view_priority_filter.text = null

        image_view_close_priority.invisible()
    }

    private fun resetProjectFilter() {
        selectedProjectList.clear()

        text_view_project_filter.text = null

        image_view_close_project.invisible()
    }

    private fun resetAssignToFilter() {
        selectedAssigneeList.clear()

        text_view_assign_to_filter.text = null

        image_view_close_assign_to.invisible()
    }

    private fun showDateSelector(dateType: Int) {
        withContext { context ->
            SpinnerDatePickerDialogBuilder()
                .context(context)
                .callback { _, year, monthOfYear, dayOfMonth ->
                    val calendar = Calendar.getInstance().apply {
                        this.set(year, monthOfYear, dayOfMonth)
                    }

                    if (dateType == 0) {
                        selectedDateFrom = calendar

                        text_view_date_from_filter.text = calendar.timeInMillis.formatDate("dd/MM/yyyy")
                    } else {
                        selectedDateTo = calendar

                        text_view_date_to_filter.text = calendar.timeInMillis.formatDate("dd/MM/yyyy")
                    }
                }
                .showTitle(true)
                .showDaySpinner(true)
                .apply {
                    if (dateType == 0) {
                        // From Date
                        val from = selectedDateFrom ?: Calendar.getInstance()
                        val to = selectedDateTo ?: Calendar.getInstance()

                        this.defaultDate(from.get(Calendar.YEAR), from.get(Calendar.MONTH), from.get(Calendar.DAY_OF_MONTH))
                        this.maxDate(to.get(Calendar.YEAR), to.get(Calendar.MONTH), to.get(Calendar.DAY_OF_MONTH))
                    } else {
                        // To Date
                        val from = selectedDateFrom ?: Calendar.getInstance()
                        val to = selectedDateTo ?: Calendar.getInstance()
                        val max = Calendar.getInstance().apply {
                            this.add(Calendar.DAY_OF_MONTH, 1)
                        }

                        this.defaultDate(to.get(Calendar.YEAR), to.get(Calendar.MONTH), to.get(Calendar.DAY_OF_MONTH))
                        this.minDate(from.get(Calendar.YEAR), from.get(Calendar.MONTH), from.get(Calendar.DAY_OF_MONTH))
                        this.maxDate(max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH))
                    }
                }
                .build()
                .show()
        }
    }

    private fun showStatusSelections() {
        val selectableList = CaseStatus.toList(true).map { Selectable(it as Any) }
        selectedStatusList.forEach { selectedStatus ->
            val selectable = selectableList.find {
                (it.data as CaseStatus).name == selectedStatus.name
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_status), selectableList)
    }

    private fun showPrioritySelections() {
        val selectableList = CasePriority.toList().map { Selectable(it as Any) }
        selectedPriorityList.forEach { selectedPriority ->
            val selectable = selectableList.find {
                (it.data as CasePriority).name == selectedPriority.name
            }
            selectable?.isSelected = true
        }

        showBottomSheetSelection(getString(R.string.label_case_priority), selectableList)
    }

    private fun showBottomSheetSelection(title: String,
                                         data: List<Selectable<Any>>,
                                         selectionType: SelectionAdapter.SelectionType = SelectionAdapter.SelectionType.MULTIPLE) {
        withActivity { activity ->
            selectionDialog?.dismiss()
            selectionDialog = showBottomSheet(activity, this, title, data, selectionType)
            selectionDialog?.show()
        }
    }
    //endregion
}