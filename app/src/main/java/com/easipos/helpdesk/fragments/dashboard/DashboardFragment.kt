package com.easipos.helpdesk.fragments.dashboard

import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.easipos.helpdesk.R
import com.easipos.helpdesk.api.requests.report.GetReportSummaryRequestModel
import com.easipos.helpdesk.base.CustomLifecycleFragment
import com.easipos.helpdesk.base.View
import com.easipos.helpdesk.fragments.alert_dialog.CommonAlertDialog
import com.easipos.helpdesk.fragments.dashboard.navigation.DashboardNavigation
import com.easipos.helpdesk.fragments.dashboard.viewmodel.DashboardViewModel
import com.easipos.helpdesk.fragments.dashboard.viewmodel.DashboardViewModelFactory
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.kodein.di.generic.instance
import java.util.*

class DashboardFragment : CustomLifecycleFragment<DashboardViewModel>(), View {

    companion object {
        fun newInstance(): DashboardFragment {
            return DashboardFragment()
        }
    }

    //region Variables
    private val navigation by instance<DashboardNavigation>()

    private val factory by lazy { DashboardViewModelFactory(requireActivity().application) }

    private var selectedDateFrom: Calendar? = null
    private var selectedDateTo: Calendar? = null
    //endregion

    //region Lifecycle
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            init()
        }
        super.onViewStateRestored(savedInstanceState)
    }
    //endregion

    //region CustomLifecycleFragment Abstract Methods
    override fun getResLayout(): Int = R.layout.fragment_dashboard

    override fun setupViewModel(): DashboardViewModel {
        return ViewModelProvider(this, factory)
            .get(DashboardViewModel::class.java)
    }

    override fun setupViewModelObserver(lifecycleOwner: LifecycleOwner) {
    }

    override fun init() {
        super.init()

        setupViews()
        setupListeners()

        attemptSearch()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        withContext { context ->
            CommonAlertDialog(context, message, action).show()
        }
    }
    //endregion

    //region Action methods
    private fun setupViews() {
        selectedDateFrom = Calendar.getInstance().apply {
            this.set(Calendar.DAY_OF_MONTH, 1)
        }
        text_view_date_from_filter.text = selectedDateFrom?.timeInMillis.formatDate("dd/MM/yyyy")

        selectedDateTo = Calendar.getInstance()
        text_view_date_to_filter.text = selectedDateTo?.timeInMillis.formatDate("dd/MM/yyyy")

        pie_chart_dashboard.apply {
            this.setUsePercentValues(true)
            this.description.isEnabled = false
            this.legend.isEnabled = true
            this.isDrawHoleEnabled = false
            this.isRotationEnabled = false
            this.isHighlightPerTapEnabled = true
            this.animateY(1400, Easing.EaseInOutQuad)
            this.setDrawEntryLabels(false)

            this.legend.apply {
                this.verticalAlignment = Legend.LegendVerticalAlignment.TOP
                this.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
                this.orientation = Legend.LegendOrientation.HORIZONTAL
                this.xEntrySpace = 12f
                this.yEntrySpace = 12f
                this.textSize = 12f
                this.setDrawInside(false)
            }
        }
    }

    private fun setupListeners() {
        card_view_date_from.click {
            showDateSelector(0)
        }

        card_view_date_to.click {
            showDateSelector(1)
        }

        button_search.click {
            attemptSearch()
        }
    }

    private fun showDateSelector(dateType: Int) {
        withContext { context ->
            SpinnerDatePickerDialogBuilder()
                .context(context)
                .callback { _, year, monthOfYear, dayOfMonth ->
                    val calendar = Calendar.getInstance().apply {
                        this.set(year, monthOfYear, dayOfMonth)
                    }

                    if (dateType == 0) {
                        selectedDateFrom = calendar

                        text_view_date_from_filter.text = calendar.timeInMillis.formatDate("dd/MM/yyyy")
                    } else {
                        selectedDateTo = calendar

                        text_view_date_to_filter.text = calendar.timeInMillis.formatDate("dd/MM/yyyy")
                    }
                }
                .showTitle(true)
                .showDaySpinner(true)
                .apply {
                    if (dateType == 0) {
                        // From Date
                        val from = selectedDateFrom ?: Calendar.getInstance()
                        val to = selectedDateTo ?: Calendar.getInstance()

                        this.defaultDate(from.get(Calendar.YEAR), from.get(Calendar.MONTH), from.get(Calendar.DAY_OF_MONTH))
                        this.maxDate(to.get(Calendar.YEAR), to.get(Calendar.MONTH), to.get(Calendar.DAY_OF_MONTH))
                    } else {
                        // To Date
                        val from = selectedDateFrom ?: Calendar.getInstance()
                        val to = selectedDateTo ?: Calendar.getInstance()
                        val max = Calendar.getInstance().apply {
                            this.add(Calendar.DAY_OF_MONTH, 1)
                        }

                        this.defaultDate(to.get(Calendar.YEAR), to.get(Calendar.MONTH), to.get(Calendar.DAY_OF_MONTH))
                        this.minDate(from.get(Calendar.YEAR), from.get(Calendar.MONTH), from.get(Calendar.DAY_OF_MONTH))
                        this.maxDate(max.get(Calendar.YEAR), max.get(Calendar.MONTH), max.get(Calendar.DAY_OF_MONTH))
                    }
                }
                .build()
                .show()
        }
    }

    private fun attemptSearch() {
        isAdded {
            if (selectedDateFrom != null && selectedDateTo == null) {
                showErrorAlertDialog(getString(R.string.error_date_to_required))
                return@isAdded
            }

            if (selectedDateTo != null && selectedDateFrom == null) {
                showErrorAlertDialog(getString(R.string.error_date_from_required))
                return@isAdded
            }

            val model = GetReportSummaryRequestModel(
                startDate = selectedDateFrom?.timeInMillis.formatDate("yyyy-MM-dd hh:mm:ss"),
                endDate = selectedDateTo?.timeInMillis.formatDate("yyyy-MM-dd hh:mm:ss")
            )

            viewModel.getReportSummary(model)
        }
    }
    //endregion
}