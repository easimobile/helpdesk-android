package com.easipos.helpdesk.fragments.rating.navigation

import android.app.Activity

interface CaseRatingNavigation {

    fun navigateToEditCase(activity: Activity, caseId: String)
}