package com.easipos.helpdesk.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.AlertDialog
import com.easipos.helpdesk.R
import io.github.anderscheow.library.appCompat.activity.FoundationAppCompatActivity
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.alert_dialog_logout.*

class LogoutAlertDialog(context: Context,
                        private val onLogout: () -> Unit) : AlertDialog(context) {

    companion object {
        fun show(activity: FoundationAppCompatActivity, onLogout: () -> Unit) {
            activity.displayAlertDialog {
                LogoutAlertDialog(activity, onLogout)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_logout)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        button_no.click {
            dismiss()
        }

        button_yes.click {
            onLogout()
            dismiss()
        }
    }
}