package com.easipos.helpdesk.fragments.cases.navigation

import android.app.Activity
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.CaseDuplication

interface CaseNavigation {

    fun navigateToCreateCase(activity: Activity, caseDuplication: CaseDuplication)

    fun navigateToEditCase(activity: Activity, caseId: String)

    fun navigateToViewCaseDetails(activity: Activity, case: Case)
}