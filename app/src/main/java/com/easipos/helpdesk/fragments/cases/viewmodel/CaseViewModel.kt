package com.easipos.helpdesk.fragments.cases.viewmodel

import android.app.Application
import com.easipos.helpdesk.api.requests.cases.GetCasesRequestModel
import com.easipos.helpdesk.base.CustomPagingWithoutLocalAndroidViewModel
import com.easipos.helpdesk.fragments.cases.paging.CaseDataSourceFactory
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.repositories.cases.CaseRepository
import io.github.anderscheow.library.paging.remote.BaseDataSourceFactory
import org.kodein.di.generic.instance
import java.util.concurrent.Executor

class CaseViewModel(application: Application) :
    CustomPagingWithoutLocalAndroidViewModel<Void, String, Case>(application) {

    //region Observable Field
    //endregion

    //region LiveData
    //endregion

    private lateinit var model: GetCasesRequestModel

    private val caseRepository by instance<CaseRepository>()

    //region Abstract methods
    override val numberOfThreads: Int
        get() = 5

    override val loadPageSize: Int
        get() = 20

    override fun getDataSourceFactory(executor: Executor): BaseDataSourceFactory<String, Case> {
        return CaseDataSourceFactory(
            executor,
            model,
            caseRepository
        )
    }
    //endregion

    fun start(model: GetCasesRequestModel) {
        this.model = model

        setIsLoading(true)
        init()
    }
}