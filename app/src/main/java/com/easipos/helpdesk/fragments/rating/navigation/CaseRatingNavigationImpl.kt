package com.easipos.helpdesk.fragments.rating.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.main.MainActivity

class CaseRatingNavigationImpl : CaseRatingNavigation {

    override fun navigateToEditCase(activity: Activity, caseId: String) {
        (activity as? MainActivity)?.navigateToEditCase(caseId)
    }
}