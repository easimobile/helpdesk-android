package com.easipos.helpdesk.fragments.rating.paging

import com.easipos.helpdesk.api.requests.cases.GetCaseRatingsRequestModel
import com.easipos.helpdesk.models.CaseRating
import com.easipos.helpdesk.models.DataSourceListModel
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import io.github.anderscheow.library.paging.remote.BaseItemKeyedDataSource
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.Executor
import kotlin.coroutines.CoroutineContext

class CaseRatingItemKeyedDataSource internal constructor(retryExecutor: Executor,
                                                         private val caseRepository: CaseRepository)
    : BaseItemKeyedDataSource<String, CaseRating>(retryExecutor), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    override fun loadInitial(success: (List<CaseRating>, Long) -> Unit, failed: (String) -> Unit) {
        val group = ArrayList<CaseRating>()
        val model = GetCaseRatingsRequestModel(
            page = pageNumber
        )

        launch {
            val result = caseRepository.getCaseRatings(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<DataSourceListModel<CaseRating>> -> {
                        hasNext = result.data.hasNext
                        group.addAll(result.data.itemList)
                        success.invoke(group, result.data.totalOfElements)
                    }

                    is Result.Error -> {
                        failed.invoke(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    override fun loadAfter(success: (List<CaseRating>) -> Unit, failed: (String) -> Unit) {
        val group = ArrayList<CaseRating>()
        val model = GetCaseRatingsRequestModel(
            page = pageNumber
        )

        launch {
            val result = caseRepository.getCaseRatings(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<DataSourceListModel<CaseRating>> -> {
                        hasNext = result.data.hasNext
                        group.addAll(result.data.itemList)
                        success.invoke(group)
                    }

                    is Result.Error -> {
                        failed.invoke(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    override fun getKey(item: CaseRating): String {
        return item.caseId
    }
    
    fun onCleared() {
        job.cancel()
    }
}
