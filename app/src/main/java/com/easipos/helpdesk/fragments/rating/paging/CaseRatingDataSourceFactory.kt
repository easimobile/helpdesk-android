package com.easipos.helpdesk.fragments.rating.paging

import com.easipos.helpdesk.models.CaseRating
import com.easipos.helpdesk.repositories.cases.CaseRepository
import io.github.anderscheow.library.paging.remote.BaseDataSourceFactory
import io.github.anderscheow.library.paging.remote.BaseItemKeyedDataSource
import java.util.concurrent.Executor

class CaseRatingDataSourceFactory internal constructor(executor: Executor,
                                                       private val caseRepository: CaseRepository)
    : BaseDataSourceFactory<String, CaseRating>(executor) {

    override fun getItemKeyedDataSource(executor: Executor): BaseItemKeyedDataSource<String, CaseRating> {
        return CaseRatingItemKeyedDataSource(
            executor,
            caseRepository
        )
    }
}
