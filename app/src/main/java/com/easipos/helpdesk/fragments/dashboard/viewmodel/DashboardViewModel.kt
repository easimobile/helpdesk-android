package com.easipos.helpdesk.fragments.dashboard.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.easipos.helpdesk.api.requests.report.GetReportSummaryRequestModel
import com.easipos.helpdesk.base.CustomBaseAndroidViewModel
import com.easipos.helpdesk.models.ReportSummary
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.report.ReportRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class DashboardViewModel(application: Application) : CustomBaseAndroidViewModel<Void>(application) {

    //region Observable Field
    val reportSummary = ObservableField<ReportSummary>()
    //endregion

    //region LiveData
    //endregion

    private val reportRepository by instance<ReportRepository>()

    override fun start(args: Void?) {
    }

    fun getReportSummary(model: GetReportSummaryRequestModel) {
        showProgressDialog()
        viewModelScope.launch {
            when (val result = reportRepository.getReportSummary(model)) {
                is Result.Success<ReportSummary> -> {
                    dismissProgressDialog()
                    setReportSummary(result.data)
                }

                is Result.Error -> {
                    dismissProgressDialog()
                    showToast(ErrorUtil.parseException(result.exception))
                }

                else -> {
                }
            }
        }
    }

    private fun setReportSummary(reportSummary: ReportSummary) {
        this.reportSummary.set(reportSummary)
        this.reportSummary.notifyChange()
    }
}