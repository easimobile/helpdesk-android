package com.easipos.helpdesk.fragments.cases.mvp

import android.app.Application
import androidx.fragment.app.DialogFragment
import com.easipos.helpdesk.R
import com.easipos.helpdesk.api.requests.cases.ChangeCaseAssigneeRequestModel
import com.easipos.helpdesk.base.Presenter
import com.easipos.helpdesk.event_bus.RefreshCaseListing
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.OperationEmployee
import com.easipos.helpdesk.models.Project
import com.easipos.helpdesk.models.Result
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.kodein.di.generic.instance

class CasePresenter(application: Application)
    : Presenter<CaseView>(application) {

    private val caseRepository by instance<CaseRepository>()

    fun doGetProjects() {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getProjects()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<Project>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showProjectSelections(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetOperationEmployees() {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getOperationEmployees()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<OperationEmployee>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showOperationEmployeeSelections(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doGetOperationEmployees(case: Case) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.getOperationEmployees()
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<List<OperationEmployee>> -> {
                        view?.setLoadingIndicator(false)
                        view?.showChangeAssignee(case, result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doChangeAssignee(dialogFragment: DialogFragment, model: ChangeCaseAssigneeRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = caseRepository.changeCaseAssignee(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(application.getString(R.string.prompt_case_assignee_change_successful)) {
                            dialogFragment.dismissAllowingStateLoss()
                        }

                        EventBus.getDefault().post(RefreshCaseListing())
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
