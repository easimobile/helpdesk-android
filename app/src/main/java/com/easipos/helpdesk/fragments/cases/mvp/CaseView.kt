package com.easipos.helpdesk.fragments.cases.mvp

import com.easipos.helpdesk.base.View
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.OperationEmployee
import com.easipos.helpdesk.models.Project

interface CaseView : View {

    fun showProjectSelections(projects: List<Project>)

    fun showOperationEmployeeSelections(operationEmployees: List<OperationEmployee>)

    fun showChangeAssignee(case: Case, operationEmployees: List<OperationEmployee>)
}
