package com.easipos.helpdesk.fragments.account

import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.easipos.helpdesk.BuildConfig
import com.easipos.helpdesk.R
import com.easipos.helpdesk.activities.main.MainActivity
import com.easipos.helpdesk.base.CustomLifecycleFragment
import com.easipos.helpdesk.fragments.account.navigation.AccountNavigation
import com.easipos.helpdesk.fragments.account.viewmodel.AccountViewModel
import com.easipos.helpdesk.fragments.account.viewmodel.AccountViewModelFactory
import com.easipos.helpdesk.fragments.alert_dialog.LogoutAlertDialog
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.isAdded
import io.github.anderscheow.library.kotlinExt.withActivity
import io.github.anderscheow.library.kotlinExt.withActivityAs
import kotlinx.android.synthetic.main.fragment_account.*
import org.kodein.di.generic.instance

class AccountFragment : CustomLifecycleFragment<AccountViewModel>() {

    companion object {
        fun newInstance(): AccountFragment {
            return AccountFragment()
        }
    }

    //region Variables
    private val navigation by instance<AccountNavigation>()

    private val factory by lazy { AccountViewModelFactory(requireActivity().application) }
    //endregion

    //region Lifecycle
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            init()
        }
        super.onViewStateRestored(savedInstanceState)
    }
    //endregion

    //region CustomLifecycleFragment Abstract Methods
    override fun getResLayout(): Int = R.layout.fragment_account

    override fun setupViewModel(): AccountViewModel {
        return ViewModelProvider(this, factory)
            .get(AccountViewModel::class.java)
    }

    override fun setupViewModelObserver(lifecycleOwner: LifecycleOwner) {
    }

    override fun init() {
        super.init()

        setupViews()
        setupListeners()

        getUserInfo()
    }
    //endregion

    //region Action methods
    fun getUserInfo() {
        isAdded {
            viewModel.getUserInfo()
        }
    }

    private fun setupViews() {
        text_view_version.text = getString(R.string.label_version, BuildConfig.VERSION_NAME)
    }

    private fun setupListeners() {
        text_view_change_password.click {
            withActivity { activity ->
                navigation.navigateToChangePassword(activity)
            }
        }

        button_logout.click {
            withActivityAs<MainActivity> { mainActivity ->
                LogoutAlertDialog.show(mainActivity) {
                    mainActivity.doLogout()
                }
            }
        }
    }
    //endregion
}