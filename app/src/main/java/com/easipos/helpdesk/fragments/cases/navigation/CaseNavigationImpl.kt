package com.easipos.helpdesk.fragments.cases.navigation

import android.app.Activity
import com.easipos.helpdesk.activities.main.MainActivity
import com.easipos.helpdesk.models.Case
import com.easipos.helpdesk.models.CaseDuplication

class CaseNavigationImpl : CaseNavigation {

    override fun navigateToCreateCase(activity: Activity, caseDuplication: CaseDuplication) {
        (activity as? MainActivity)?.navigateToCreateCase(caseDuplication)
    }

    override fun navigateToEditCase(activity: Activity, caseId: String) {
        (activity as? MainActivity)?.navigateToEditCase(caseId)
    }

    override fun navigateToViewCaseDetails(activity: Activity, case: Case) {
        (activity as? MainActivity)?.navigateToViewCaseDetails(case)
    }
}