package com.easipos.helpdesk.fragments.rating.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CaseRatingViewModelFactory(private val application: Application)
    : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CaseRatingViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CaseRatingViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}