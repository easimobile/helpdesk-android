package com.easipos.helpdesk.fragments.rating

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.helpdesk.R
import com.easipos.helpdesk.adapters.CaseRatingAdapter
import com.easipos.helpdesk.base.CustomLifecycleFragment
import com.easipos.helpdesk.fragments.rating.navigation.CaseRatingNavigation
import com.easipos.helpdesk.fragments.rating.viewmodel.CaseRatingViewModel
import com.easipos.helpdesk.fragments.rating.viewmodel.CaseRatingViewModelFactory
import com.easipos.helpdesk.models.*
import com.easipos.helpdesk.tools.EqualSpacingItemDecoration
import io.github.anderscheow.library.kotlinExt.*
import io.github.anderscheow.library.viewModel.observeItems
import io.github.anderscheow.library.viewModel.observeNetworkState
import io.github.anderscheow.library.viewModel.observeTotalItems
import kotlinx.android.synthetic.main.fragment_case_rating.*
import org.kodein.di.generic.instance
import java.util.*

class CaseRatingFragment : CustomLifecycleFragment<CaseRatingViewModel>(),
    CaseRatingAdapter.OnGestureDetectedListener {

    companion object {
        fun newInstance(): CaseRatingFragment {
            return CaseRatingFragment()
        }
    }

    //region Variables
    private val navigation by instance<CaseRatingNavigation>()

    private val factory by lazy { CaseRatingViewModelFactory(requireActivity().application) }

    private val itemDecoration by lazy { EqualSpacingItemDecoration(resources.getDimensionPixelSize(R.dimen.sixteen_dp), EqualSpacingItemDecoration.VERTICAL) }
    //endregion

    //region Lifecycle
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            init()
        }
        super.onViewStateRestored(savedInstanceState)
    }
    //endregion

    //region CustomLifecycleFragment Abstract Methods
    override fun getResLayout(): Int = R.layout.fragment_case_rating

    override fun setupViewModel(): CaseRatingViewModel {
        return ViewModelProvider(this, factory)
            .get(CaseRatingViewModel::class.java)
    }

    override fun setupViewModelObserver(lifecycleOwner: LifecycleOwner) {
    }

    override fun init() {
        super.init()

        setupViews()
        setupListeners()

        getCaseRatings()
    }
    //endregion

    //region Interface methods
    override fun onSelectItem(item: CaseRating) {
        withActivity { activity ->
            navigation.navigateToEditCase(activity, item.caseId)
        }
    }
    //endregion

    //region Action methods
    fun getCaseRatings() {
        isAdded {
            viewModel.start()
            setupAdapter()
        }
    }

    private fun setupViews() {
    }

    private fun setupListeners() {
    }

    private fun setupAdapter() {
        withContext { context ->
            val adapter = CaseRatingAdapter(this) { viewModel.retry() }

            setupRecyclerView(context, adapter)
            setupViewModel(adapter)
        }
    }

    private fun setupRecyclerView(context: Context, adapter: CaseRatingAdapter) {
        recycler_view_case_rating.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
            this.removeItemDecoration(itemDecoration)
            this.addItemDecoration(itemDecoration)
        }
    }

    private fun setupViewModel(adapter: CaseRatingAdapter) {
        with(viewModel) {
            this.observeItems(this@CaseRatingFragment, adapter)
            this.observeNetworkState(this@CaseRatingFragment, adapter, true)
            this.observeTotalItems(this@CaseRatingFragment)
        }
    }
    //endregion
}