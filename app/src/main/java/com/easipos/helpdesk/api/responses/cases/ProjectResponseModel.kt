package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class ProjectResponseModel(
    @SerializedName("projectCode")
    val projectCode: String?,

    @SerializedName("projectName")
    val projectName: String?,

    @SerializedName("outletCount")
    val outletCount: Int?,

    @SerializedName("slaCount")
    val slaCount: Int?
)



