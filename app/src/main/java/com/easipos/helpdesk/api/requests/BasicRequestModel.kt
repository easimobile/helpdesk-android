package com.easipos.helpdesk.api.requests

import com.easipos.helpdesk.managers.UserManager

data class BasicRequestModel(
    val apiKey: String = UserManager.token?.token ?: ""
)