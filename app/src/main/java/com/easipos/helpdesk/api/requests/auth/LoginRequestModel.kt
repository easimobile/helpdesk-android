package com.easipos.helpdesk.api.requests.auth

data class LoginRequestModel(
    val id: String,
    val password: String,
    val mobile: Int = 1
)