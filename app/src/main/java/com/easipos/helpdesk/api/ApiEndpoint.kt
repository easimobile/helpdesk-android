package com.easipos.helpdesk.api

object ApiEndpoint {

    const val GET_CLIENT_URL = "getClientUrl"

    const val CHECK_VERSION = "ddcard/version/check"

    const val LOGIN = "login"

    const val FORGOT_PASSWORD = "forgotPassword"
    const val CHANGE_PASSWORD = "changePassword"
    const val GET_USER_INFO = "api/v1/getUserInfo"

    const val GET_CASES = "api/v3/Case/getAllCases"
    const val GET_CASE_INFO = "api/v1/Case/getCaseInfo"
    const val GET_CASE_DETAILS = "api/v1/Case/getCaseDetail"
    const val CREATE_CASE = "api/v1/Case/addCase"
    const val CREATE_CASE_DETAIL = "api/v1/Case/addCaseDetail"
    const val EDIT_CASE = "api/v1/Case/editCase"
    const val EDIT_CASE_DETAIL = "api/v1/Case/editCaseDetail"
    const val GET_PROJECTS = "api/v1/Case/getProjects"
    const val GET_CUSTOMER_OUTLETS = "api/v1/Case/getCustomerOutlets"
    const val GET_OUTLET_POS_LIST = "api/v1/Case/getOutletPos"
    const val GET_PROJECT_SLA_LIST = "api/v1/Case/getSLA"
    const val GET_OPERATION_EMPLOYEES = "api/v1/Case/getOperationEmployees"
    const val GET_CASE_SERVICE_TYPES = "api/v1/Case/getCaseServiceType"
    const val GET_CASE_TYPES = "api/v1/Case/getCaseType"
    const val GET_CASE_SUB_TYPES_1 = "api/v1/Case/getCaseSubType1"
    const val GET_CASE_SUB_TYPES_2 = "api/v1/Case/getCaseSubType2"
    const val GET_CASE_SUB_TYPES_3 = "api/v1/Case/getCaseSubType3"
    const val CHANGE_CASE_ASSIGNEE = "api/v1/Case/changeCaseAssignee"
    const val CHANGE_CASE_DETAIL_STATUS = "api/v1/Case/changeCaseDetailStatus"
    const val GET_CASE_RATINGS = "api/v1/Case/getRatingList"

    const val GET_REPORT_SUMMARY = "api/v1/Report/getHelpdeskSummary"

    const val REGISTER_PUSH_TOKEN = "api/v1/Notification/registerPushToken"
    const val REMOVE_PUSH_TOKEN = "api/v1/Notification/removePushToken"
}
