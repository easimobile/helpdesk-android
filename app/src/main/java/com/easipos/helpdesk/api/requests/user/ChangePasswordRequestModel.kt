package com.easipos.helpdesk.api.requests.user

import com.easipos.helpdesk.managers.UserManager
import com.google.gson.annotations.SerializedName

data class ChangePasswordRequestModel(
    val apiKey: String? = UserManager.token?.token,
    val userId: String? = null,
    val mobile: Int = 1,
    val currentPassword: String,
    val newPassword: String,
    @SerializedName("newPassword_confirmation")
    val newPasswordConfirmation: String
)