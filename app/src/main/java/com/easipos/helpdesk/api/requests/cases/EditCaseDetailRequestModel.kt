package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager
import com.google.gson.annotations.SerializedName

data class EditCaseDetailRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val mobile: Int = 1,

    @SerializedName("CaseID")
    val caseId: String,

    @SerializedName("CaseDetailID")
    val caseDetailId: String,

    @SerializedName("Notes")
    val notes: String,

    @SerializedName("Status")
    val status: String,

    @SerializedName("ServiceType")
    val serviceType: String?,

    @SerializedName("ServiceReportNo")
    val serviceReportNo: String?,

    @SerializedName("Screenshot")
    val screenshot: String?,

    @SerializedName("UserNegligence")
    val userNegligence: Boolean
)