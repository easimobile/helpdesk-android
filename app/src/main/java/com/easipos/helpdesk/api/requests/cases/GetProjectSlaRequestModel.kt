package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager

data class GetProjectSlaRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val projectCode: String,
    val priority: Int
)