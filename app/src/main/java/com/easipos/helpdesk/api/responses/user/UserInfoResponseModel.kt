package com.easipos.helpdesk.api.responses.user

import com.google.gson.annotations.SerializedName

data class UserInfoResponseModel(
    @SerializedName("userCode")
    val userCode: String?,

    @SerializedName("name")
    val name: String?,

    @SerializedName("email")
    val email: String?,

    @SerializedName("userProfile")
    val userProfile: String?
)