package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class CaseRatingPagingResponseModel(
    val page: Long,
    val hasNext: Boolean,
    val total: Long,
    val list: List<CaseRatingResponseModel>
)

data class CaseRatingResponseModel(
    @SerializedName("CaseId")
    val caseId: String?,

    @SerializedName("Rate")
    val rate: String?,

    @SerializedName("Remark")
    val remark: String?,

    @SerializedName("RateDateTime")
    val rateDateTime: String?
)