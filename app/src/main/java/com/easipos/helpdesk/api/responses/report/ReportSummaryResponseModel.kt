package com.easipos.helpdesk.api.responses.report

import com.google.gson.annotations.SerializedName

data class ReportSummaryResponseModel(
    @SerializedName("TOTAL")
    val total: String?,

    @SerializedName("OPEN")
    val open: String?,

    @SerializedName("PENDING")
    val pending: String?,

    @SerializedName("IN_PROGRESS")
    val inProgress: String?,

    @SerializedName("RESOLVED")
    val resolved: String?,

    @SerializedName("CLOSED")
    val closed: String?
)