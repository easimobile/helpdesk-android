package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager
import com.google.gson.annotations.SerializedName

data class GetCasesRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val mobile: Int = 1,
    var page: Int = 1,

    @SerializedName("DateFrom")
    val dateFrom: String? = null,

    @SerializedName("DateTo")
    val dateTo: String? = null,

    @SerializedName("Status")
    val status: List<String>,

    @SerializedName("Priority")
    val priority: List<Int>,

    @SerializedName("Project")
    val project: List<String>,

    @SerializedName("AssignedTo")
    val assignedTo: List<String>,

    @SerializedName("ProjectName")
    val projectName: String?,

    @SerializedName("OutletName")
    val outletName: String?
)