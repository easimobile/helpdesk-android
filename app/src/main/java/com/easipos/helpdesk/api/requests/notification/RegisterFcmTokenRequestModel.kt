package com.easipos.helpdesk.api.requests.notification

import com.easipos.helpdesk.managers.UserManager

data class RegisterFcmTokenRequestModel(
    val pushToken: String,
    val os: String = "android",
    val apiKey: String? = UserManager.token?.token
)