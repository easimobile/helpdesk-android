package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class OperationEmployeeResponseModel(
    @SerializedName("EMP_CD")
    val employeeCode: String?,

    @SerializedName("EMP_NM")
    val employeeName: String?
)