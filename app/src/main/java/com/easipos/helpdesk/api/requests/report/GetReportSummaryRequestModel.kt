package com.easipos.helpdesk.api.requests.report

import com.easipos.helpdesk.managers.UserManager

data class GetReportSummaryRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val startDate: String?,
    val endDate: String?
)