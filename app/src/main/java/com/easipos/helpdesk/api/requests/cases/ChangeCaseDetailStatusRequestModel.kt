package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager
import com.google.gson.annotations.SerializedName

data class ChangeCaseDetailStatusRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val mobile: Int = 1,

    @SerializedName("CaseDetailID")
    val caseDetailID: String,

    @SerializedName("Status")
    val status: String
)