package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class ProjectSlaResponseModel(
    @SerializedName("SLACode")
    val slaCode: String?,

    @SerializedName("priority")
    val priority: Int?,

    @SerializedName("responseTime")
    val responseTime: String?,

    @SerializedName("resolutionTime")
    val resolutionTime: String?
)