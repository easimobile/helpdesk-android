package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class CasePagingResponseModel(
    val page: Long,
    val hasNext: Boolean,
    val total: Long,
    val list: List<CaseResponseModel>
)

data class CaseResponseModel(
    @SerializedName("CaseID")
    val caseId: String?,

    @SerializedName("ProjectName")
    val projectName: String?,

    @SerializedName("AssignedToName")
    val assignedToName: String?,

    @SerializedName("Priority")
    val priority: Int?,

    @SerializedName("CreatedDate")
    val createdDate: String?,

    @SerializedName("CreatedTime")
    val createdTime: String?,

    @SerializedName("Remarks")
    val remarks: String?,

    @SerializedName("Status")
    val status: String?,

    @SerializedName("editable")
    val editable: Int?,

    @SerializedName("isRated")
    val isRated: String?,

    @SerializedName("numOfRating")
    val numOfRating: String?
)