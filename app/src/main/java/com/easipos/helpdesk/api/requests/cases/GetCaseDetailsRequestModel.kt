package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager

data class GetCaseDetailsRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val caseId: String
)