package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager

data class GetCaseRatingsRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val mobile: Int = 1,
    val page: Int = 1
)