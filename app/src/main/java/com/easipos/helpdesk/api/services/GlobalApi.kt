package com.easipos.helpdesk.api.services

import com.easipos.helpdesk.api.ApiEndpoint
import com.easipos.helpdesk.api.misc.ResponseModel
import retrofit2.http.GET
import retrofit2.http.Query

interface GlobalApi {

    @GET(ApiEndpoint.GET_CLIENT_URL)
    suspend fun getClientUrl(@Query("clientName") clientName: String): ResponseModel<String>
}
