package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class CaseInfoResponseModel(
    @SerializedName("CaseID")
    val caseId: String?,

    @SerializedName("ProjectCode")
    val projectCode: String?,

    @SerializedName("ProjectName")
    val projectName: String?,

    @SerializedName("CreatedBy")
    val createdBy: String?,

    @SerializedName("CreatedByName")
    val createdByName: String?,

    @SerializedName("AssignedTo")
    val assignedTo: String?,

    @SerializedName("AssignedToName")
    val assignedToName: String?,

    @SerializedName("ShipToCode")
    val outletCode: String?,

    @SerializedName("outletName")
    val outletName: String?,

    @SerializedName("Addr1")
    val address1: String?,

    @SerializedName("Addr2")
    val address2: String?,

    @SerializedName("Addr3")
    val address3: String?,

    @SerializedName("Addr4")
    val address4: String?,

    @SerializedName("Addr5")
    val address5: String?,

    @SerializedName("postalCode")
    val postalCode: String?,

    @SerializedName("POSNumber")
    val posNo: String?,

    @SerializedName("POS_DESC")
    val posDesc: String?,

    @SerializedName("IP_ADDRESS")
    val ipAddress: String?,

    @SerializedName("Priority")
    val priority: Int?,

    @SerializedName("CreatedDate")
    val createdDate: String?,

    @SerializedName("CreatedTime")
    val createdTime: String?,

    @SerializedName("ContactPerson")
    val contactPerson: String?,

    @SerializedName("ContactNo")
    val contactNo: String?,

    @SerializedName("LastUpdatedDate")
    val lastUpdatedDate: String?,

    @SerializedName("LastUpdatedTime")
    val lastUpdatedTime: String?,

    @SerializedName("Remarks")
    val remarks: String?,

    @SerializedName("Status")
    val status: String?,

    @SerializedName("CaseTypeCode")
    val caseTypeCode: String?,

    @SerializedName("CaseTypeDesc")
    val caseTypeDesc: String?,

    @SerializedName("CaseSubTypeCode1")
    val caseSubTypeCode1: String?,

    @SerializedName("CaseSubTypeDesc1")
    val caseSubTypeDesc1: String?,

    @SerializedName("CaseSubTypeCode2")
    val caseSubTypeCode2: String?,

    @SerializedName("CaseSubTypeDesc2")
    val caseSubTypeDesc2: String?,

    @SerializedName("CaseSubTypeCode3")
    val caseSubTypeCode3: String?,

    @SerializedName("CaseSubTypeDesc3")
    val caseSubTypeDesc3: String?,

    @SerializedName("hasCaseSubType1")
    val hasCaseSubType1: Int?,

    @SerializedName("hasCaseSubType2")
    val hasCaseSubType2: Int?,

    @SerializedName("hasCaseSubType3")
    val hasCaseSubType3: Int?,

    @SerializedName("screenshot")
    val screenshotInUrl: String?,

    @SerializedName("allowPost")
    val allowPost: String?,

    @SerializedName("editable")
    val editable: Int?,

    @SerializedName("OriginalCaseID")
    val originalCaseId: String?,

    @SerializedName("SLACode")
    val slaCode: String?,

    @SerializedName("responseTime")
    val responseTime: String?,

    @SerializedName("resolutionTime")
    val resolutionTime: String?,

    @SerializedName("pendingDays")
    val pendingDays: Int?
)