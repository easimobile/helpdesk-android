package com.easipos.helpdesk.api.responses.auth

import com.google.gson.annotations.SerializedName

data class LoginInfoResponseModel(
    @SerializedName("apiKey")
    val apiKey: String?,

    @SerializedName("module")
    val modules: List<ModulePermissionResponseModel>?
)

data class ModulePermissionResponseModel(
    @SerializedName("ProgramName")
    val programName: String?,

    @SerializedName("AllowPost")
    val allowPost: String?
)