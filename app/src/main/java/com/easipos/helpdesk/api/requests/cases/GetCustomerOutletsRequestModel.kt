package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager

data class GetCustomerOutletsRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val projectCode: String
)