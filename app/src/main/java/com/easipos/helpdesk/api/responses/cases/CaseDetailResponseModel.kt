package com.easipos.helpdesk.api.responses.cases

import com.google.gson.annotations.SerializedName

data class CaseDetailResponseModel(
    @SerializedName("CaseDetailID")
    val caseDetailId: String?,

    @SerializedName("Notes")
    val notes: String?,

    @SerializedName("CreatedDate")
    val createdDate: String?,

    @SerializedName("CreatedTime")
    val createdTime: String?,

    @SerializedName("CreatedBy")
    val createdBy: String?,

    @SerializedName("CreatedByName")
    val createdByName: String?,

    @SerializedName("Status")
    val status: String?,

    @SerializedName("ServiceTypeCode")
    val serviceTypeCode: String?,

    @SerializedName("ServiceTypeDesc")
    val serviceTypeDesc: String?,

    @SerializedName("ServiceReportNo")
    val serviceReportNo: String?,

    @SerializedName("screenshot")
    val screenshotInUrl: String?,

    @SerializedName("UserNegligence")
    val isUserNegligence: String?,

    @SerializedName("SequenceNo")
    val sequenceNo: String?,

    @SerializedName("allowPost")
    val allowPost: String?,

    @SerializedName("editable")
    val editable: Int?
)