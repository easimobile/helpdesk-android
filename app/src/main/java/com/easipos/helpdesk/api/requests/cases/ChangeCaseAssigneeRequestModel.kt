package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager
import com.google.gson.annotations.SerializedName

data class ChangeCaseAssigneeRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val mobile: Int = 1,

    @SerializedName("CaseID")
    val caseId: String,

    @SerializedName("AssignedTo")
    val assignedTo: String
)