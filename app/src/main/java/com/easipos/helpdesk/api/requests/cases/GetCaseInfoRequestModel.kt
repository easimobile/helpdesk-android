package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager
import com.google.gson.annotations.SerializedName

data class GetCaseInfoRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",

    @SerializedName("CaseID")
    val caseId: String
)