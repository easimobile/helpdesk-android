package com.easipos.helpdesk.api.requests.cases

import com.easipos.helpdesk.managers.UserManager
import com.google.gson.annotations.SerializedName

data class CreateCaseRequestModel(
    val apiKey: String = UserManager.token?.token ?: "",
    val mobile: Int = 1,

    @SerializedName("ProjectCode")
    val projectCode: String,

    @SerializedName("AssignedTo")
    val assignedTo: String?,

    @SerializedName("ShipToCode")
    val outletCode: String,

    @SerializedName("POSNumber")
    val posNo: String,

    @SerializedName("Priority")
    val priority: Int,

    @SerializedName("ContactPerson")
    val contactPerson: String?,

    @SerializedName("ContactNo")
    val contactNo: String?,

    @SerializedName("Remarks")
    val remarks: String?,

    @SerializedName("CaseTypeCode")
    val caseTypeCode: String,

    @SerializedName("CaseSubTypeCode1")
    val caseSubTypeCode1: String?,

    @SerializedName("CaseSubTypeCode2")
    val caseSubTypeCode2: String?,

    @SerializedName("CaseSubTypeCode3")
    val caseSubTypeCode3: String?,

    @SerializedName("Screenshot")
    val screenshot: String?,

    @SerializedName("OriginalCaseID")
    val originalCaseId: String?,

    @SerializedName("SLACode")
    val slaCode: String
)