package com.easipos.helpdesk.di.modules

import com.easipos.helpdesk.activities.change_password.navigation.ChangePasswordNavigation
import com.easipos.helpdesk.activities.change_password.navigation.ChangePasswordNavigationImpl
import com.easipos.helpdesk.activities.create_edit_case.navigation.CreateEditCaseNavigation
import com.easipos.helpdesk.activities.create_edit_case.navigation.CreateEditCaseNavigationImpl
import com.easipos.helpdesk.activities.create_edit_case_detail.navigation.CreateEditCaseDetailNavigation
import com.easipos.helpdesk.activities.create_edit_case_detail.navigation.CreateEditCaseDetailNavigationImpl
import com.easipos.helpdesk.activities.login.navigation.LoginNavigation
import com.easipos.helpdesk.activities.login.navigation.LoginNavigationImpl
import com.easipos.helpdesk.activities.main.navigation.MainNavigation
import com.easipos.helpdesk.activities.main.navigation.MainNavigationImpl
import com.easipos.helpdesk.activities.reset_password.navigation.ResetPasswordNavigation
import com.easipos.helpdesk.activities.reset_password.navigation.ResetPasswordNavigationImpl
import com.easipos.helpdesk.activities.splash.navigation.SplashNavigation
import com.easipos.helpdesk.activities.splash.navigation.SplashNavigationImpl
import com.easipos.helpdesk.activities.view_case_details.navigation.ViewCaseDetailsNavigation
import com.easipos.helpdesk.activities.view_case_details.navigation.ViewCaseDetailsNavigationImpl
import io.github.anderscheow.library.di.modules.ActivityBaseModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

class ActivityModule : ActivityBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<SplashNavigation>() with provider { SplashNavigationImpl() }
            bind<LoginNavigation>() with provider { LoginNavigationImpl() }
            bind<ResetPasswordNavigation>() with provider { ResetPasswordNavigationImpl() }
            bind<MainNavigation>() with provider { MainNavigationImpl() }
            bind<ChangePasswordNavigation>() with provider { ChangePasswordNavigationImpl() }
            bind<CreateEditCaseNavigation>() with provider { CreateEditCaseNavigationImpl() }
            bind<CreateEditCaseDetailNavigation>() with provider { CreateEditCaseDetailNavigationImpl() }
            bind<ViewCaseDetailsNavigation>() with provider { ViewCaseDetailsNavigationImpl() }
        }
    }
}
