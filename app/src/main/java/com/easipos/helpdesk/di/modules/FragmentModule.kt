package com.easipos.helpdesk.di.modules

import com.easipos.helpdesk.fragments.account.navigation.AccountNavigation
import com.easipos.helpdesk.fragments.account.navigation.AccountNavigationImpl
import com.easipos.helpdesk.fragments.cases.navigation.CaseNavigation
import com.easipos.helpdesk.fragments.cases.navigation.CaseNavigationImpl
import com.easipos.helpdesk.fragments.dashboard.navigation.DashboardNavigation
import com.easipos.helpdesk.fragments.dashboard.navigation.DashboardNavigationImpl
import io.github.anderscheow.library.di.modules.FragmentBaseModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

class FragmentModule : FragmentBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<CaseNavigation>() with provider { CaseNavigationImpl() }
            bind<DashboardNavigation>() with provider { DashboardNavigationImpl() }
            bind<AccountNavigation>() with provider { AccountNavigationImpl() }
        }
    }
}
