package com.easipos.helpdesk.di.modules

import android.app.Application
import androidx.room.Room
import com.easipos.helpdesk.Easi
import com.easipos.helpdesk.api.misc.AuthInterceptor
import com.easipos.helpdesk.api.misc.HostSelectionInterceptor
import com.easipos.helpdesk.api.misc.TokenAuthenticator
import com.easipos.helpdesk.api.misc.constructOkhttpClient
import com.easipos.helpdesk.api.services.Api
import com.easipos.helpdesk.api.services.GlobalApi
import com.easipos.helpdesk.datasource.DataFactory
import com.easipos.helpdesk.managers.FcmManager
import com.easipos.helpdesk.managers.PushNotificationManager
import com.easipos.helpdesk.repositories.auth.AuthDataRepository
import com.easipos.helpdesk.repositories.auth.AuthRepository
import com.easipos.helpdesk.repositories.cases.CaseDataRepository
import com.easipos.helpdesk.repositories.cases.CaseRepository
import com.easipos.helpdesk.repositories.notification.NotificationDataRepository
import com.easipos.helpdesk.repositories.notification.NotificationRepository
import com.easipos.helpdesk.repositories.precheck.PrecheckDataRepository
import com.easipos.helpdesk.repositories.precheck.PrecheckRepository
import com.easipos.helpdesk.repositories.report.ReportDataRepository
import com.easipos.helpdesk.repositories.report.ReportRepository
import com.easipos.helpdesk.repositories.user.UserDataRepository
import com.easipos.helpdesk.repositories.user.UserRepository
import com.easipos.helpdesk.room.RoomService
import com.easipos.helpdesk.services.FcmService
import com.easipos.helpdesk.services.PushNotificationService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import io.github.anderscheow.library.di.modules.BaseModule
import io.github.anderscheow.library.di.modules.CommonBaseModule
import io.github.anderscheow.validator.Validator
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.eagerSingleton
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CommonModule(private val easi: Easi) : CommonBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        super.provideAdditionalModule(builder)
        builder.apply {
            bind<Easi>() with singleton { instance<Application>() as Easi }
            bind<Validator>() with singleton { Validator.with(easi) }
            bind<PushNotificationManager>() with singleton { PushNotificationManager(PushNotificationService()) }
            bind<FcmManager>() with singleton { FcmManager(FcmService(easi)) }

            bind<DataFactory>() with singleton {
                DataFactory(instance(), instance(), instance(), instance(), instance(), instance())
            }

            bind<PrecheckRepository>() with singleton { PrecheckDataRepository(instance()) }
            bind<AuthRepository>() with singleton { AuthDataRepository(instance()) }
            bind<UserRepository>() with singleton { UserDataRepository(instance()) }
            bind<CaseRepository>() with singleton { CaseDataRepository(instance()) }
            bind<ReportRepository>() with singleton { ReportDataRepository(instance()) }
            bind<NotificationRepository>() with singleton {
                NotificationDataRepository(instance())
            }
        }
    }
}

class ApiModule(private val userAgent: String,
                private val endpoint: String,
                private val clientEndpoint: String,
                private val authorisation: String
) : BaseModule("apiModule") {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<AuthInterceptor>() with singleton { AuthInterceptor(userAgent, authorisation) }
            bind<HostSelectionInterceptor>() with singleton { HostSelectionInterceptor() }
            bind<TokenAuthenticator>() with singleton { TokenAuthenticator(instance()) }
            bind<RxJava2CallAdapterFactory>() with singleton { RxJava2CallAdapterFactory.create() }
            bind<GsonConverterFactory>() with singleton {
                val gson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                    .create()
                GsonConverterFactory.create(gson)
            }
            bind<GlobalApi>() with singleton {
                Retrofit.Builder()
                    .baseUrl(endpoint)
                    .client(
                        constructOkhttpClient(listOf(
                        instance<AuthInterceptor>()
                    ), instance<TokenAuthenticator>())
                    )
                    .addCallAdapterFactory(instance())
                    .addConverterFactory(instance())
                    .build()
                    .create(GlobalApi::class.java)
            }
            bind<Api>() with singleton {
                Retrofit.Builder()
                    .baseUrl(clientEndpoint)
                    .client(
                        constructOkhttpClient(listOf(
                        instance<AuthInterceptor>(), instance<HostSelectionInterceptor>()
                    ), instance<TokenAuthenticator>())
                    )
                    .addCallAdapterFactory(instance())
                    .addConverterFactory(instance())
                    .build()
                    .create(Api::class.java)
            }
        }
    }
}

class DatabaseModule(private val easi: Easi,
                     private val dbName: String
) : BaseModule("databaseModule") {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<RoomService>() with eagerSingleton {
                Room.databaseBuilder(easi.applicationContext,
                    RoomService::class.java, dbName)
                    .fallbackToDestructiveMigration()
                    .build()
            }
        }
    }
}