package com.easipos.helpdesk.models

data class OutletPos(
    val posNo: String,
    val posDesc: String,
    val ipAddress: String
) {

    companion object {
        fun toString(posNo: String, posDesc: String, ipAddress: String): String {
            val string = if (posDesc.isNotBlank()) {
                "$posNo - $posDesc"
            } else {
                posNo
            }

            return if (ipAddress.isNotBlank()) {
                "$string ($ipAddress)"
            } else {
                string
            }
        }
    }

    override fun toString(): String {
        return Companion.toString(posNo, posDesc, ipAddress)
    }
}