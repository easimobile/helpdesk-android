package com.easipos.helpdesk.models

data class ReportSummary(
    val total: Int,
    val open: Int,
    val pending: Int,
    val inProgress: Int,
    val resolved: Int,
    val closed: Int
)