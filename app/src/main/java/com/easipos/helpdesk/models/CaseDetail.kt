package com.easipos.helpdesk.models

import android.os.Parcelable
import com.easipos.helpdesk.constant.CaseStatus
import io.github.anderscheow.library.kotlinExt.formatDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class CaseDetail(
    val caseDetailId: String,
    val notes: String,
    val createdDate: String,
    val createdTime: String,
    val createdBy: String,
    val createdByName: String,
    val status: String,
    val serviceTypeCode: String,
    val serviceTypeDesc: String,
    val serviceReportNo: String,
    val screenshotInUrl: String,
    val isUserNegligence: Boolean,
    val sequenceNo: String?,
    val allowPost: Boolean, // To determine whether is admin
    val editable: Boolean // To determine whether content can edit
) : Parcelable {

    fun formatSequenceNo(): String {
        return "Res $sequenceNo"
    }

    fun formatCreatedDate(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                createdDate
            )
            date?.time.formatDate("dd/MM/yyyy")
        } catch (ex: Exception) {
            createdDate
        }
    }

    fun formatCreatedDateTime(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                createdTime
            )
            "${formatCreatedDate()}\n${date?.time.formatDate("hh:mm a")}"
        } catch (ex: Exception) {
            "${formatCreatedDate()}\n$createdTime"
        }
    }

    fun formatStatus(): String {
        return CaseStatus.parse(status)?.value ?: ""
    }
}