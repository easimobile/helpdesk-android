package com.easipos.helpdesk.models

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.easipos.helpdesk.constant.CasePriority
import com.easipos.helpdesk.constant.CaseStatus
import io.github.anderscheow.library.kotlinExt.formatDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Case(
    val caseId: String,
    val projectName: String,
    val assignedToName: String,
    val priority: Int,
    val createdDate: String,
    val createdTime: String,
    val remarks: String,
    val status: String,
    val editable: Boolean, // To determine whether content can edit
    val isRated: Boolean,
    val numOfRating: Int
) : Parcelable {

    companion object {

        var DIFF_CALLBACK: DiffUtil.ItemCallback<Case> = object : DiffUtil.ItemCallback<Case>() {
            override fun areItemsTheSame(oldItem: Case, newItem: Case): Boolean {
                return oldItem.caseId == newItem.caseId
            }

            override fun areContentsTheSame(oldItem: Case, newItem: Case): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Case

        if (caseId != other.caseId) return false
        if (projectName != other.projectName) return false
        if (assignedToName != other.assignedToName) return false
        if (priority != other.priority) return false
        if (createdDate != other.createdDate) return false
        if (createdTime != other.createdTime) return false
        if (remarks != other.remarks) return false
        if (status != other.status) return false
        if (isRated != other.isRated) return false
        if (numOfRating != other.numOfRating) return false

        return true
    }

    override fun hashCode(): Int {
        return caseId.hashCode()
    }

    fun formatCaseId(): String {
        return "#$caseId"
    }

    fun formatCreatedDate(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                createdDate
            )
            date?.time.formatDate("dd/MM/yyyy")
        } catch (ex: Exception) {
            createdDate
        }
    }

    fun formatCreatedDateTime(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                createdTime
            )
            "${formatCreatedDate()}\n${date?.time.formatDate("hh:mm a")}"
        } catch (ex: Exception) {
            "${formatCreatedDate()}\n$createdTime"
        }
    }

    fun formatPriority(): String {
        return CasePriority.parse(priority)?.value ?: ""
    }

    fun formatStatus(): String {
        return CaseStatus.parse(status)?.value ?: ""
    }

    fun formatAssignToName(): String {
        if (assignedToName.isNotBlank()) {
            return assignedToName
        }
        return "-"
    }
}