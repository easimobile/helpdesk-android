package com.easipos.helpdesk.models

data class UserInfo(
    val userCode: String,
    val name: String,
    val email: String,
    val userProfile: String
)