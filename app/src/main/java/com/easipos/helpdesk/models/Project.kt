package com.easipos.helpdesk.models

data class Project(
    val projectCode: String,
    val projectName: String,
    val outletCount: Int,
    val slaCount: Int
) {

    override fun toString(): String {
        return projectName
    }
}