package com.easipos.helpdesk.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OperationEmployee(
    val employeeCode: String,
    val employeeName: String
) : Parcelable {

    override fun toString(): String {
        return employeeName
    }
}