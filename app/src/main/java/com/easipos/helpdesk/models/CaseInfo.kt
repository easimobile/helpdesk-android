package com.easipos.helpdesk.models

import android.os.Parcelable
import com.easipos.helpdesk.constant.CasePriority
import com.easipos.helpdesk.constant.CaseStatus
import io.github.anderscheow.library.kotlinExt.formatDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class CaseInfo(
    val caseId: String,
    val projectCode: String,
    val projectName: String,
    val createdBy: String,
    val createdByName: String,
    val assignedTo: String,
    val assignedToName: String,
    val outletCode: String,
    val outletName: String,
    val address1: String,
    val address2: String,
    val address3: String,
    val address4: String,
    val address5: String,
    val postalCode: String,
    val posNo: String,
    val posDesc: String,
    val ipAddress: String,
    val priority: Int,
    val createdDate: String,
    val createdTime: String,
    val contactPerson: String,
    val contactNo: String,
    val lastUpdatedDate: String,
    val lastUpdatedTime: String,
    val remarks: String,
    val status: String,
    val caseTypeCode: String,
    val caseTypeDesc: String,
    val caseSubTypeCode1: String?,
    val caseSubTypeDesc1: String?,
    val caseSubTypeCode2: String?,
    val caseSubTypeDesc2: String?,
    val caseSubTypeCode3: String?,
    val caseSubTypeDesc3: String?,
    val screenshotInUrl: String,
    val hasCaseSubType1: Boolean, // refer to caseType
    val hasCaseSubType2: Boolean, // refer to caseSubType1
    val hasCaseSubType3: Boolean, // refer to caseSubType2
    val allowPost: Boolean, // To determine whether is admin
    val editable: Boolean, // To determine whether content can edit
    val originalCaseId: String?,
    val slaCode: String,
    val responseTime: String,
    val resolutionTime: String,
    val pendingDays: Int
) : Parcelable {

    fun formatCaseId(): String {
        return "#$caseId"
    }

    fun formatOriginalCaseId(): String {
        return "#$originalCaseId"
    }

    fun formatCreatedDate(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                createdDate
            )
            date?.time.formatDate("dd/MM/yyyy")
        } catch (ex: Exception) {
            createdDate
        }
    }

    fun formatCreatedDateTime(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                createdTime
            )
            "${formatCreatedDate()}\n${date?.time.formatDate("hh:mm a")}"
        } catch (ex: Exception) {
            "${formatCreatedDate()}\n$createdTime"
        }
    }

    fun formatOutlet(): String {
        return CustomerOutlet.toString(outletCode, outletName, address1, address2, address3,
            address4, address5, postalCode)
    }

    fun formatPos(): String {
        return OutletPos.toString(posNo, posDesc, ipAddress)
    }

    fun formatPriority(): String {
        return CasePriority.parse(priority)?.value ?: ""
    }

    fun formatStatus(): String {
        return CaseStatus.parse(status)?.value ?: ""
    }

    fun formatSla(): String {
        return ProjectSla.toString(responseTime, resolutionTime)
    }

    fun isPending(): Boolean {
        return CaseStatus.parse(status) == CaseStatus.PENDING
    }
}