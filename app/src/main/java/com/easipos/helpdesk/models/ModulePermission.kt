package com.easipos.helpdesk.models

data class ModulePermission(
    val programName: String,
    val allowPost: Boolean
)