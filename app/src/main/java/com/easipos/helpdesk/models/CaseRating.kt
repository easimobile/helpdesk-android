package com.easipos.helpdesk.models

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import io.github.anderscheow.library.kotlinExt.formatDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class CaseRating(
    val caseId: String,
    val rate: Int,
    val remark: String,
    val rateDateTime: String
) : Parcelable {

    companion object {

        var DIFF_CALLBACK: DiffUtil.ItemCallback<CaseRating> = object : DiffUtil.ItemCallback<CaseRating>() {
            override fun areItemsTheSame(oldItem: CaseRating, newItem: CaseRating): Boolean {
                return oldItem.caseId == newItem.caseId
            }

            override fun areContentsTheSame(oldItem: CaseRating, newItem: CaseRating): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CaseRating

        if (caseId != other.caseId) return false
        if (rate != other.rate) return false
        if (remark != other.remark) return false
        if (rateDateTime != other.rateDateTime) return false

        return true
    }

    override fun hashCode(): Int {
        return caseId.hashCode()
    }

    fun formatCaseId(): String {
        return "#$caseId"
    }

    fun formatRateDateTime(): String {
        return try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(
                rateDateTime
            )
            date?.time.formatDate("dd/MM/yyyy")
        } catch (ex: Exception) {
            rateDateTime
        }
    }
}