package com.easipos.helpdesk.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CaseDuplication(
    val caseId: String,
    val duplicationType: DuplicationType
) : Parcelable

@Parcelize
enum class DuplicationType : Parcelable {
    DUPLICATE,  // For Duplicate Case, with Linked Case ID
    COPY        // Purely for Copy Paste Existing Case as New Case
}