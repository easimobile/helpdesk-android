package com.easipos.helpdesk.models

data class ResponseWrapper<T>(
    val code: Int,
    val data: T
)