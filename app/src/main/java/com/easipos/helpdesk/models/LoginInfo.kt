package com.easipos.helpdesk.models

data class LoginInfo(
    val apiKey: String,
    val modules: List<ModulePermission>
)