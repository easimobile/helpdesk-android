package com.easipos.helpdesk.models

class DataSourceListModel<out T>(val itemList: List<T>, val totalOfElements: Long, val hasNext: Boolean)

class DataSourceMapModel<K, out V>(val itemMap: Map<K, V>, val totalOfElements: Long, val hasNext: Boolean)