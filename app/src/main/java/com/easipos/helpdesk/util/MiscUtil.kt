package com.easipos.helpdesk.util

import android.app.Activity
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.easipos.helpdesk.Easi
import com.easipos.helpdesk.R
import com.easipos.helpdesk.adapters.SelectionAdapter
import com.easipos.helpdesk.managers.PushNotificationManager
import com.easipos.helpdesk.managers.PushNotificationManager.Companion.FCM_BODY
import com.easipos.helpdesk.managers.PushNotificationManager.Companion.FCM_TITLE
import com.easipos.helpdesk.models.Selectable
import com.easipos.helpdesk.tools.EqualSpacingItemDecoration
import com.easipos.helpdesk.tools.Preference
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textview.MaterialTextView
import com.tapadoo.alerter.Alerter
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.findColor
import io.github.anderscheow.library.kotlinExt.hideKeyboard
import org.json.JSONObject
import java.util.*

val languages = mapOf("zh" to "中文", "en" to "English")

fun defaultRequestOption(): RequestOptions {
    return RequestOptions()
}

fun ImageView.loadImage(imageUrl: String) {
    Glide.with(this.context)
        .load(imageUrl)
        .apply(defaultRequestOption())
        .transition(DrawableTransitionOptions().crossFade(500))
        .into(this)
}

fun changeLanguage(activity: Activity, language: String) {
    if (Preference.prefLanguageCode != language) {
        Preference.prefLanguageCode = language
        Easi.localeManager.setNewLocale(activity, language)
        activity.recreate()
    }
}

fun showAlerter(easi: Easi, pushNotificationManager: PushNotificationManager, jsonObject: JSONObject) {
    jsonObject.takeIf {
        jsonObject.has(FCM_TITLE) && jsonObject.has(FCM_BODY)
    }.run {
        this?.let { data ->
            easi.currentActivity?.let { activity ->
                Alerter.create(activity)
                    .setTitle(data.getString(FCM_TITLE) ?: "")
                    .setText(data.getString(FCM_BODY) ?: "")
                    .setBackgroundColorInt(activity.findColor(R.color.colorPrimary))
                    .setDuration(5000)
                    .setIconColorFilter(Color.WHITE)
                    .enableSwipeToDismiss()
                    .enableVibration(true)
                    .setOnClickListener(View.OnClickListener {
                        Alerter.hide()

                        pushNotificationManager.openNotification(easi, jsonObject, true)
                    })
                    //.setOnShowListener { Tracker.trackScreen(this@CustomLifecycleActivity, Screen.IN_APP_NOTIFICATION) }
                    .show()
            }
        }
    }
}

fun showBottomSheet(activity: Activity,
                    listener: SelectionAdapter.OnGestureDetectedListener,
                    title: String, data: List<Selectable<Any>>,
                    selectionType: SelectionAdapter.SelectionType = SelectionAdapter.SelectionType.SINGLE): BottomSheetDialog {
    val dialog = BottomSheetDialog(activity, R.style.BottomSheetDialogStyle)

    val displayMetrics = DisplayMetrics()
    activity.windowManager.defaultDisplay.getMetrics(displayMetrics)

    val adapter = SelectionAdapter(activity, selectionType, listener).apply {
        this.items = data.toMutableList()
    }

    val view = activity.layoutInflater.inflate(R.layout.bottom_sheet_selection, null).apply {
        this.findViewById<MaterialTextView>(R.id.text_view_selection_title).text = title
        this.findViewById<RecyclerView>(R.id.recycler_view_selection).apply {
            this.adapter = adapter
            this.layoutManager = LinearLayoutManager(context)
            this.addItemDecoration(EqualSpacingItemDecoration(resources.getDimensionPixelSize(R.dimen.sixteen_dp), EqualSpacingItemDecoration.VERTICAL))
            this.isNestedScrollingEnabled = false
        }
        this.findViewById<AppCompatImageView>(R.id.image_view_close).click {
            dialog.dismiss()
        }
        this.findViewById<TextInputEditText>(R.id.text_input_edit_text_search).addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) = Unit

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let { value ->
                    if (value.isNotBlank()) {
                        adapter.items = data.filter {
                            it.data.toString().toLowerCase(Locale.getDefault()).contains(value.toString().toLowerCase(Locale.getDefault()))
                        }.toMutableList()
                    } else {
                        adapter.items = data.toMutableList()
                    }
                }
            }
        })
    }

    return dialog.apply {
        this.setContentView(view)
        BottomSheetBehavior.from(view.parent as View).let {
            it.peekHeight = (displayMetrics.heightPixels * 0.5).toInt()
            it.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                var isExpanded = false
                var layoutManager = view.findViewById<RecyclerView>(R.id.recycler_view_selection).layoutManager as LinearLayoutManager

                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_EXPANDED -> {
                            isExpanded = true
                        }

                        BottomSheetBehavior.STATE_COLLAPSED -> {
                            isExpanded = false
                        }

                        BottomSheetBehavior.STATE_DRAGGING -> {
                            if (isExpanded && layoutManager.findFirstVisibleItemPosition() != 0) {
                                it.state = BottomSheetBehavior.STATE_EXPANDED
                            }
                        }

                        else -> {
                        }
                    }
                }
            })
        }
        this.setOnDismissListener {
            activity.hideKeyboard()
            listener.onReturnItems(adapter.getSelectedItems())
        }
    }
}