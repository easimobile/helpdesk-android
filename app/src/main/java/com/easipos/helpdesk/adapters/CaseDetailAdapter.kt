package com.easipos.helpdesk.adapters

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.easipos.helpdesk.R
import com.easipos.helpdesk.constant.CaseStatus
import com.easipos.helpdesk.databinding.ViewCaseDetailBinding
import com.easipos.helpdesk.models.CaseDetail
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.setSizeTextTypeToNone
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_case_detail.*

class CaseDetailAdapter(context: Context,
                        private val listener: OnGestureDetectedListener)
    : BaseRecyclerViewAdapter<CaseDetail>(context) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: CaseDetail)

        fun onMarkAsClosed(item: CaseDetail)
    }

    private var viewBinderHelper: ViewBinderHelper? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewCaseDetailBinding>(
            layoutInflater, R.layout.view_case_detail, parent, false)
        return CaseDetailViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CaseDetailViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    fun setCustomItems(items: List<CaseDetail>) {
        viewBinderHelper = ViewBinderHelper().apply {
            this.setOpenOnlyOne(true)
        }
        this.items = items.toMutableList()
    }

    fun saveStates(outState: Bundle?) {
        viewBinderHelper?.saveStates(outState)
    }

    fun restoreStates(inState: Bundle?) {
        viewBinderHelper?.restoreStates(inState)
    }

    inner class CaseDetailViewHolder(private val binding: ViewCaseDetailBinding)
        : BaseViewHolder<CaseDetail>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: CaseDetail) {
            viewBinderHelper?.bind(swipe_reveal_layout, item.caseDetailId)

            layout_content.post {
                layout_swipe_right.post {
                    layout_swipe_right.layoutParams = layout_swipe_right.layoutParams.apply {
                        this.height = layout_content.height
                    }
                }
            }

            swipe_reveal_layout.setLockDrag(item.editable.not() ||
                    item.allowPost.not() ||
                    CaseStatus.parse(item.status) != CaseStatus.RESOLVED
            )
            text_view_status.setSizeTextTypeToNone()

            layout_content.click {
                listener.onSelectItem(item)
            }

            text_view_mark_as_closed.click {
                listener.onMarkAsClosed(item)
            }
        }

        override fun onClick(view: View, item: CaseDetail?) {
        }
    }
}