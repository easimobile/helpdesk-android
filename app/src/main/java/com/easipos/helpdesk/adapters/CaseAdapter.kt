package com.easipos.helpdesk.adapters

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.easipos.helpdesk.R
import com.easipos.helpdesk.constant.CaseStatus
import com.easipos.helpdesk.databinding.ViewCaseBinding
import com.easipos.helpdesk.models.Case
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.setSizeTextTypeToNone
import io.github.anderscheow.library.kotlinExt.visible
import io.github.anderscheow.library.recyclerView.adapters.paging.BasePagedListAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_case.*

class CaseAdapter(private val listener: OnGestureDetectedListener,
                  callback: () -> Unit)
    : BasePagedListAdapter<Case>(Case.DIFF_CALLBACK, callback) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: Case)

        fun onSelectAddResponse(item: Case)

        fun onSelectChangeAssignee(item: Case)

        fun onDuplicateCase(item: Case)

        fun onCopyCase(item: Case)
    }

    private var viewBinderHelper: ViewBinderHelper? = null

    public override fun getBodyLayout(position: Int): Int {
        return R.layout.view_case
    }

    override fun getBodyViewHolder(viewType: Int, binding: ViewDataBinding): RecyclerView.ViewHolder {
        return CaseViewHolder(binding as ViewCaseBinding)
    }

    override fun submitList(pagedList: PagedList<Case>?) {
        viewBinderHelper = ViewBinderHelper().apply {
            this.setOpenOnlyOne(true)
        }
        super.submitList(pagedList)
    }

    fun saveStates(outState: Bundle?) {
        viewBinderHelper?.saveStates(outState)
    }

    fun restoreStates(inState: Bundle?) {
        viewBinderHelper?.restoreStates(inState)
    }

    inner class CaseViewHolder(private val binding: ViewCaseBinding)
        : BaseViewHolder<Case>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: Case) {
            viewBinderHelper?.bind(swipe_reveal_layout, item.caseId)

            CaseStatus.parse(item.status)?.let { status ->
                when (status) {
                    CaseStatus.CLOSED,
                    CaseStatus.CLOSED_WITH_EXCEPTION -> {
                        text_view_add_response.gone()
                        text_view_change_assignee.gone()
                        text_view_duplicate.visible()
                    }

                    else -> {
                        if (item.editable) {
                            text_view_add_response.visible()
                            text_view_change_assignee.visible()
                            text_view_duplicate.gone()
                        } else {
                            swipe_reveal_layout.setLockDrag(true)
                        }
                    }
                }
            }

            text_view_status.setSizeTextTypeToNone()

            layout_content.click {
                swipe_reveal_layout.close(true)
                listener.onSelectItem(item)
            }

            text_view_add_response.click {
                swipe_reveal_layout.close(true)
                listener.onSelectAddResponse(item)
            }

            text_view_change_assignee.click {
                swipe_reveal_layout.close(true)
                listener.onSelectChangeAssignee(item)
            }

            text_view_duplicate.click {
                swipe_reveal_layout.close(true)
                listener.onDuplicateCase(item)
            }

            text_view_copy.click {
                swipe_reveal_layout.close(true)
                listener.onCopyCase(item)
            }
        }

        override fun onClick(view: View, item: Case?) {
        }
    }
}