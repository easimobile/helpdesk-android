package com.easipos.helpdesk.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.helpdesk.R
import com.easipos.helpdesk.databinding.ViewSelectionBinding
import com.easipos.helpdesk.models.Selectable
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_selection.*

class SelectionAdapter(context: Context,
                       private val selectionType: SelectionType,
                       private val listener: OnGestureDetectedListener)
    : BaseRecyclerViewAdapter<Selectable<Any>>(context) {

    enum class SelectionType {
        SINGLE,
        MULTIPLE
    }

    interface OnGestureDetectedListener {
        fun onSelectItem(position: Int, data: Any)

        fun onReturnItems(items: List<Any>)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewSelectionBinding>(
            layoutInflater, R.layout.view_selection, parent, false)
        return SelectionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SelectionViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    fun getSelectedItems(): List<Any> {
        return items.filter { it.isSelected }.map { it.data }
    }

    private fun selectIndex(selectedIndex: Int) {
        items.forEachIndexed { index, selectable ->
            when (selectionType) {
                SelectionType.SINGLE -> {
                    if (selectedIndex == index) {
                        listener.onSelectItem(selectedIndex, selectable.data)
                    }
                    selectable.isSelected = selectedIndex == index
                }

                SelectionType.MULTIPLE -> {
                    if (selectedIndex == index) {
                        selectable.isSelected = selectable.isSelected.not()
                    }
                }
            }
        }
        notifyDataSetChanged()
    }

    inner class SelectionViewHolder(private val binding: ViewSelectionBinding)
        : BaseViewHolder<Selectable<Any>>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: Selectable<Any>) {
            text_view_selection.text = item.data.toString()
            if (item.isSelected) {
                text_view_selection.apply {
                    this.typeface = Typeface.DEFAULT_BOLD
                    this.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_check, 0)
                }
            } else {
                text_view_selection.apply {
                    this.typeface = Typeface.DEFAULT
                    this.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0)
                }
            }
        }

        override fun onClick(view: View, item: Selectable<Any>?) {
            item?.let { _ ->
                selectIndex(adapterPosition)
            }
        }
    }
}