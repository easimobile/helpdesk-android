package com.easipos.helpdesk.adapters

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.easipos.helpdesk.R
import com.easipos.helpdesk.databinding.ViewCaseRatingBinding
import com.easipos.helpdesk.models.CaseRating
import io.github.anderscheow.library.recyclerView.adapters.paging.BasePagedListAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class CaseRatingAdapter(private val listener: OnGestureDetectedListener,
                        callback: () -> Unit)
    : BasePagedListAdapter<CaseRating>(CaseRating.DIFF_CALLBACK, callback) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: CaseRating)
    }

    public override fun getBodyLayout(position: Int): Int {
        return R.layout.view_case_rating
    }

    override fun getBodyViewHolder(viewType: Int, binding: ViewDataBinding): RecyclerView.ViewHolder {
        return CaseRatingViewHolder(binding as ViewCaseRatingBinding)
    }

    inner class CaseRatingViewHolder(private val binding: ViewCaseRatingBinding)
        : BaseViewHolder<CaseRating>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: CaseRating) {
        }

        override fun onClick(view: View, item: CaseRating?) {
            item?.let {
                listener.onSelectItem(item)
            }
        }
    }
}