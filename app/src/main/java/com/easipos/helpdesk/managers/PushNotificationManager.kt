package com.easipos.helpdesk.managers

import android.content.Intent
import com.easipos.helpdesk.Easi
import com.easipos.helpdesk.activities.main.MainActivity
import com.easipos.helpdesk.activities.splash.SplashActivity
import com.easipos.helpdesk.constant.NotificationCategory
import com.easipos.helpdesk.services.PushNotificationService
import com.easipos.helpdesk.util.showAlerter
import com.orhanobut.logger.Logger
import org.json.JSONObject

class PushNotificationManager(val service: PushNotificationService) {

    companion object {
        const val FCM_TITLE = "pn_title"
        const val FCM_BODY = "pn_body"
        const val FCM_CATEGORY = "pn_category"
        const val FCM_ARGUMENT = "pn_arg"
    }

    var payload: Payload? = null
        private set

    fun removePayload() {
        payload = null
    }

    fun receiveNotification(easi: Easi, jsonObject: JSONObject, isActivityVisible: Boolean) {
        if (isActivityVisible) {
            showAlerter(easi, this, jsonObject)
        }
        easi.mainActivity?.let { mainActivity ->
            refreshContentBasedOnCategory(mainActivity, jsonObject)
        }
    }

    fun openNotification(easi: Easi, jsonObject: JSONObject?, isActivityVisible: Boolean) {
        if (jsonObject != null) {
            if (jsonObject.has(FCM_CATEGORY)) {
                payload = Payload(
                    jsonObject[FCM_CATEGORY] as? String,
                    jsonObject[FCM_ARGUMENT] as? String
                )

                if (isActivityVisible) {
                    easi.mainActivity?.processPayload()
                } else {
                    val intent = SplashActivity.newIntent(easi).apply {
                        this.flags =
                            Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK
                    }
                    easi.startActivity(intent)
                }
            }
        }
    }

    /**
     *  Refresh content based on notification category
     */
    private fun refreshContentBasedOnCategory(mainActivity: MainActivity, jsonObject: JSONObject) {
        try {
//            mainActivity.updateNotifications()

            if (jsonObject.has(FCM_CATEGORY)) {
                val notificationType = jsonObject.get(FCM_CATEGORY) as? String

                if (UserManager.token != null && notificationType != null) {
                    NotificationCategory.parse(notificationType)?.let { notificationCategory ->
                        when (notificationCategory) {
                            NotificationCategory.CASE_ASSIGNED_TO,
                            NotificationCategory.CASE_CREATED_BY_CUSTOMER -> {
                                mainActivity.refreshCases()
                            }

                            else -> {}
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
//            Crashlytics.logException(ex)
        }
    }

    private fun finishAllActivitiesExceptMainActivity() {
        try {
            for (activity in Easi.activities) {
                Logger.d(activity)
                if (activity !is MainActivity) {
                    activity.finish()
                }
            }
        } catch (ex: Exception) {
//            Crashlytics.logException(ex)
            ex.printStackTrace()
        }
    }

    class Payload internal constructor(category: String?, arg: String?) {

        var category: String? = category
            private set

        var arg: String? = arg
            private set

        override fun toString(): String {
            return "Payload(category=$category,arg=$arg)"
        }
    }
}