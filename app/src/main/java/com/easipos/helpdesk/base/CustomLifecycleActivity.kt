package com.easipos.helpdesk.base

import android.content.Context
import com.easipos.helpdesk.Easi
import io.github.anderscheow.library.appCompat.activity.LifecycleAppCompatActivity
import io.github.anderscheow.library.viewModel.BaseAndroidViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

abstract class CustomLifecycleActivity<VM : BaseAndroidViewModel<*>>
    : LifecycleAppCompatActivity<VM>(), KodeinAware {

    private val _kodein: Kodein by closestKodein()

    override val kodein = Kodein.lazy {
        extend(_kodein)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(Easi.localeManager.setLocale(base))
    }
}
