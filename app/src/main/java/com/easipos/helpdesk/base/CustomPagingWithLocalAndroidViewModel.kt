package com.easipos.helpdesk.base

import android.app.Application
import com.easipos.helpdesk.Easi
import io.github.anderscheow.library.paging.remoteWithLocal.PagingModel
import io.github.anderscheow.library.viewModel.PagingWithLocalAndroidViewModel
import org.kodein.di.KodeinAware

abstract class CustomPagingWithLocalAndroidViewModel<in Args, Value : PagingModel>(application: Application)
    : PagingWithLocalAndroidViewModel<Args, Value>(application), KodeinAware {

    override val kodein by (application as Easi).kodein
}