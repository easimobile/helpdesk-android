package com.easipos.helpdesk.datasource.cases

import com.easipos.helpdesk.api.requests.cases.*
import com.easipos.helpdesk.mapper.cases.*
import com.easipos.helpdesk.models.*

interface CaseDataStore {

    suspend fun getCases(model: GetCasesRequestModel, caseMapper: CaseMapper): Result<DataSourceListModel<Case>>

    suspend fun getCaseInfo(model: GetCaseInfoRequestModel, caseInfoMapper: CaseInfoMapper): Result<CaseInfo>

    suspend fun getCaseDetails(model: GetCaseDetailsRequestModel, caseDetailMapper: CaseDetailMapper): Result<List<CaseDetail>>

    suspend fun getProjects(projectMapper: ProjectMapper): Result<List<Project>>

    suspend fun getCustomerOutlets(model: GetCustomerOutletsRequestModel, customerOutletMapper: CustomerOutletMapper): Result<List<CustomerOutlet>>

    suspend fun getOutletPosList(model: GetOutletPosListRequestModel, outletPosMapper: OutletPosMapper): Result<List<OutletPos>>

    suspend fun getProjectSla(model: GetProjectSlaRequestModel, projectSlaMapper: ProjectSlaMapper): Result<ProjectSla>

    suspend fun getOperationEmployees(operationEmployeeMapper: OperationEmployeeMapper): Result<List<OperationEmployee>>

    suspend fun getServiceTypes(serviceTypeMapper: ServiceTypeMapper): Result<List<ServiceType>>

    suspend fun getCaseTypes(caseTypeMapper: CaseTypeMapper): Result<List<CaseType>>

    suspend fun getCaseSubTypes1(model: GetCaseSubTypesRequestModel, caseSubTypeMapper: CaseSubTypeMapper): Result<List<CaseSubType>>

    suspend fun getCaseSubTypes2(model: GetCaseSubTypesRequestModel, caseSubTypeMapper: CaseSubTypeMapper): Result<List<CaseSubType>>

    suspend fun getCaseSubTypes3(model: GetCaseSubTypesRequestModel, caseSubTypeMapper: CaseSubTypeMapper): Result<List<CaseSubType>>

    suspend fun createCase(model: CreateCaseRequestModel): Result<String>

    suspend fun createCaseDetail(model: CreateCaseDetailRequestModel): Result<Nothing>

    suspend fun editCase(model: EditCaseRequestModel): Result<Nothing>

    suspend fun editCaseDetail(model: EditCaseDetailRequestModel): Result<Nothing>

    suspend fun changeCaseAssignee(model: ChangeCaseAssigneeRequestModel): Result<Nothing>

    suspend fun changeCaseDetailStatus(model: ChangeCaseDetailStatusRequestModel): Result<Nothing>

    suspend fun getCaseRatings(model: GetCaseRatingsRequestModel, caseRatingMapper: CaseRatingMapper): Result<DataSourceListModel<CaseRating>>
}
