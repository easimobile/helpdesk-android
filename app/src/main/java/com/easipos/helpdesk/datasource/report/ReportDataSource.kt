package com.easipos.helpdesk.datasource.report

import com.easipos.helpdesk.api.misc.parseException
import com.easipos.helpdesk.api.misc.parseResponse
import com.easipos.helpdesk.api.requests.report.GetReportSummaryRequestModel
import com.easipos.helpdesk.api.services.Api
import com.easipos.helpdesk.mapper.report.ReportSummaryMapper
import com.easipos.helpdesk.models.ReportSummary
import com.easipos.helpdesk.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ReportDataSource(private val api: Api) : ReportDataStore {

    override suspend fun getReportSummary(model: GetReportSummaryRequestModel, reportSummaryMapper: ReportSummaryMapper): Result<ReportSummary> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getReportSummary(model.apiKey, model.startDate, model.endDate)
                parseResponse(response) {
                    reportSummaryMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}
