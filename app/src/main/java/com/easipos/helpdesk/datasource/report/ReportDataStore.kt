package com.easipos.helpdesk.datasource.report

import com.easipos.helpdesk.api.requests.report.GetReportSummaryRequestModel
import com.easipos.helpdesk.mapper.report.ReportSummaryMapper
import com.easipos.helpdesk.models.ReportSummary
import com.easipos.helpdesk.models.Result

interface ReportDataStore {

    suspend fun getReportSummary(model: GetReportSummaryRequestModel, reportSummaryMapper: ReportSummaryMapper): Result<ReportSummary>
}
