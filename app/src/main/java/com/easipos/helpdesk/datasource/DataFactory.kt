package com.easipos.helpdesk.datasource

import android.app.Application
import com.easipos.helpdesk.api.services.Api
import com.easipos.helpdesk.api.services.GlobalApi
import com.easipos.helpdesk.datasource.auth.AuthDataSource
import com.easipos.helpdesk.datasource.auth.AuthDataStore
import com.easipos.helpdesk.datasource.cases.CaseDataSource
import com.easipos.helpdesk.datasource.cases.CaseDataStore
import com.easipos.helpdesk.datasource.notification.NotificationDataSource
import com.easipos.helpdesk.datasource.notification.NotificationDataStore
import com.easipos.helpdesk.datasource.precheck.PrecheckDataSource
import com.easipos.helpdesk.datasource.precheck.PrecheckDataStore
import com.easipos.helpdesk.datasource.report.ReportDataSource
import com.easipos.helpdesk.datasource.report.ReportDataStore
import com.easipos.helpdesk.datasource.user.UserDataSource
import com.easipos.helpdesk.datasource.user.UserDataStore
import com.easipos.helpdesk.room.RoomService
import io.github.anderscheow.library.executor.PostExecutionThread
import io.github.anderscheow.library.executor.ThreadExecutor

class DataFactory(private val application: Application,
                  private val globalApi: GlobalApi,
                  private val api: Api,
                  private val roomService: RoomService,
                  private val threadExecutor: ThreadExecutor,
                  private val postExecutionThread: PostExecutionThread
) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(globalApi, api)

    fun createAuthDataSource(): AuthDataStore =
        AuthDataSource(api)

    fun createUserDataSource(): UserDataStore =
        UserDataSource(api)

    fun createCaseDataSource(): CaseDataStore =
        CaseDataSource(api)

    fun createReportDataSource(): ReportDataStore =
        ReportDataSource(api)

    fun createNotificationDataSource(): NotificationDataStore =
        NotificationDataSource(api)
}
